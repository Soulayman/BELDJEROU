class Bagage{
    private int numero;
    private String couleur;
    private double poids;




    public Bagage(int numero, String couleur, double poids){
     this.numero=numero;
     this.couleur=couleur;
     this.poids=poids;   
    }

    public Bagage(){
        
    }

    public void show(){
        System.out.println("Ce bagage a pour matricule "+this.numero+" de couleur "+this.couleur+" avec un poid de "+this.poids);
    }

    public int getNumero(){
        return this.numero;
    }
    public String getCouleur(){
        return this.couleur;
    }
    public double getPoids(){
        return this.poids;
    }
    
    public void setNumero(int num){
        this.numero=num;
    }

    public void setCouleur(String coul){
        this.couleur=coul;
    }
    
    public void setPoids(double poi){
        this.poids=poi;
    }

}