@extends('layouts.app')
@section('content')
<div class="container">
<form method="post" action="{{ route('reservation.store')}}">
@csrf
  <div class="form-group Vietnam font-weight-light">
    <label for="dateD" class="Vietnam">date debut</label>
    <input name="dateD" type="date" class="form-control @error('dateD') is-invalid @enderror acceuil" id="dateD"  >
        @error('dated')
        <div class="alert alert-danger mt-2">
        {{$message}} Vous avez faux Sir 
        </div>
        @enderror
     </div>
  <div class="form-group Vietnam font-weight-light">
    <label for="dateF">date fin</label>
    <input name="dateF" type="date" class="form-control acceuil" id="dateF" >
    
  </div>
  <div class="form-group Vietnam font-weight-light">
  <label for="idPeriode">Select list:</label>
  <select name="idPeriode" class="form-control acceuil" id="idPeriode">
    <option value=1>basse</option>
    <option value=2>moyenne</option>
    <option value=3>haute</option>
  </select>
  </div>
  <button type="submit" class="Viet btn btn-primary">Submit</button>
</form>
</div>
@stop