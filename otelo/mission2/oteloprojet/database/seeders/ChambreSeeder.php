<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Chambre;

class ChambreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i=1; $i<=30 ; $i++){
            $lettrechambre="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $hasard=$lettrechambre[rand(0,25)];
       
            $chambre= Chambre::create([
                'nbCouchage' =>rand(1,4),
                'porte' => $hasard,
                'etage' => rand(1,15),
                'idCategorie' => 1,
                'baignoire' => rand(1,2),
                'prixBase' => rand(45,255)
        ]);

        $chambre->save();   // -> BDD
    }
        //affichage en console
        dd($chambre);
        

    }
}
