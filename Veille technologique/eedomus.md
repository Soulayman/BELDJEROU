# Eedomus c'est quoi ? 

## Eedomus est une box de domotique pour rendre son logement connecté , cela se rapproche de l'intelligence artificiel , mais plus tourner sur l'assistance du locataire , on peut donc contrôler tout les appareils éléctronique de son logement via sa voix 

## extrait de l'article

# Eedomus est une box domotique permettant de rendre un logement connecté avec une liberté quasiment inégalée dans le marché de la domotique. En effet, en une décennie, eedomus a constamment évolué tandis que des centaines de solutions domotiques disparaissent au fil du temps.

# source = https://blog.domadoo.fr/84772-formation-domotique-eedomus-maison-connectee/ 