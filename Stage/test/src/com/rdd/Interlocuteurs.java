package com.rdd;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Interlocuteurs {
	public static void main(String[] args) throws Exception {
		File file = new File("D:\\BTS_SIO\\Stage\\RDD2021\\Excel\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("INTERLOCUTEURS");
		
		int rowNum = 1;
		
		List<InfoInterlocuteurs> retour = new ArrayList<>();
		
		for (int i = 1; i<=sheet.getLastRowNum(); i++)
		{
			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoInterlocuteurs info = new InfoInterlocuteurs();
			
			info.setCodeFourn(ligneCourante.getCell(0).getStringCellValue());
			info.setCodeClient(ligneCourante.getCell(1).getStringCellValue());
			info.setCodeSite(ligneCourante.getCell(2).getStringCellValue());
			info.setTypeInterlo(ligneCourante.getCell(3).getStringCellValue());
			info.setCivilite(ligneCourante.getCell(4).getStringCellValue());
			info.setNom(ligneCourante.getCell(5).getStringCellValue());
			info.setPrenom(ligneCourante.getCell(6).getStringCellValue());
			info.setNTelMob(ligneCourante.getCell(7).getStringCellValue());
			info.setNtelFix(ligneCourante.getCell(8).getStringCellValue());
			info.setEmail(ligneCourante.getCell(9).getStringCellValue());
			info.setRecoitEmail(ligneCourante.getCell(10).getStringCellValue());
			info.setRecoitRapMob(ligneCourante.getCell(11).getStringCellValue());
			info.setRecoitRapQual(ligneCourante.getCell(12).getStringCellValue());
			info.setPersonConf(ligneCourante.getCell(13).getStringCellValue());
			info.setRythme(ligneCourante.getCell(14).getStringCellValue());
			
			retour.add(info);
			
			rowNum++;

		}
		
		
		
		
		
		
		for (InfoInterlocuteurs info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getCodeFourn() + " ; " +info.getCodeClient() + " ; " +info.getCodeSite() 
			+ " ; " +info.getTypeInterlo() + " ; " +info.getCivilite() + " ; " +info.getNom() + " ; " +info.getPrenom() 
			+ " ; " +info.getNTelMob() + " ; " +info.getNtelFix() + " ; " +info.getEmail() + " ; " +info.getRecoitEmail() 
			+ " ; " +info.getRecoitRapMob() + " ; " +info.getRecoitRapQual() + " ; " +info.getPersonConf() 
			+ " ; " +info.getRythme() + (info.isEnErreur() ? info.getMessagesErreur(): "OK"));
		}
	}

}
