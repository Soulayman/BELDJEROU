package eu.hautil;

import java.sql.*;
import java.util.ArrayList;

public class JDBCTable {
        private Connection Conn() throws SQLException { //Ceci est une Fonction
            Connection conn = null;

            try {
                String urlBDD = "jdbc:mysql://sio-hautil.eu:3306/beldjs?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
                String user = "beldjs";
                String pwd = "BUsn05052001&*";
                //Class.forName(Driver);
                System.out.println("Driver is Okay Kid");
                conn = DriverManager.getConnection(urlBDD, user, pwd);
                System.out.println("connexion too kid \n ");
            }
            catch(SQLException e){
               e.printStackTrace();
            }

            finally {
                return conn;
            }
        }

        public void Affichage() throws SQLException {

            Connection conn = this.Conn(); //On appelle la fonction Conn()

            Statement stmt = conn.createStatement();

            String req5 = "SELECT  ID ,Age , Prenom, Orientation FROM JBDC";

            ResultSet result2 = stmt.executeQuery(req5);

            ArrayList<Eleve> listEleve = new ArrayList<Eleve>(); //Creation d'une ArrayList ne contenant que des objets de type Eleve

            while(result2.next()){
                Eleve CEO = new Eleve(result2.getInt(1),result2.getInt(2),result2.getString(3),result2.getString(4));
                listEleve.add(CEO);
            }

            for(Eleve f : listEleve){
                System.out.println(f);
            }

            stmt.close();
            conn.close();
        }

        public void Inserer(Eleve el) throws SQLException{
            Connection conn = this.Conn();

            String req = "INSERT INTO JBDC(age,Prenom,Orientation) VALUES (? , ? , ?)";

            PreparedStatement pdstmt = conn.prepareStatement(req);


            pdstmt.setInt(1, el.getAge());
            pdstmt.setString(2, el.getPrenom());
            pdstmt.setString(3, el.getOrientation());

            pdstmt.executeUpdate();

            System.out.println("\n Hey ! Wake up , la Ligne a été mise à jour");

            pdstmt.close();
            conn.close();

        }

        public void Supprimer(int ki) throws SQLException{

            Connection conn = this.Conn();

            String req = "DELETE FROM JBDC WHERE ID= ?";

            PreparedStatement pdstmt = conn.prepareStatement(req);

            pdstmt.setInt(1, ki);

            pdstmt.executeUpdate();

            System.out.println("\n Hey ! Wake up , la Ligne a été mise à jour");

            pdstmt.close();
            conn.close();

        }
}
