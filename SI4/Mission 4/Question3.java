//Mission4 SI4
//BELDJEROU Soulayman
//23/11/2019
//Question 3

import java.util.Scanner;//importation du Scanner
public class Question3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in); //ajout d'un Scanner
		System.out.println("combien de produit souhaitez vous achetez ? ");//affiche ce que doit faire l'utilisateur
		int a =sc.nextInt(); //laisse l'accès au clavier a l'utilisateur
		double resultat=0;
		
		String[] noms= new String[a]; //on initialise un tableau de chaine de caractère 
		double[] prixht=new double[a];//on initialise un tableau de réels
		
		for(int i=0; i<noms.length;i++){  //on utilise une boucle For 
			System.out.println(" Saisir nom du produit "+ (i+1));
			noms[i]=sc.next();
			sc.nextLine();
			
			System.out.println(" Saisir le prix du produit "+ noms[i]);
			prixht[i]=sc.nextDouble();
			
			resultat= prixht[i] + resultat;//on calcule le total des prix des produits 
		}
		for(int i=0; i<noms.length;i++){ 
			System.out.println(noms[i]+" coute "+prixht[i]);
			
		}
		
		System.out.println("le total de tout vos produit vaut "+ resultat); 
	}	
}