# Compte Rendu Veille Technologique 

## Sujet:
### Le sujet de Veille Technologique sur laquelle j'ai eu l'occasion de travailler est l'Intelligence Artificiel Domotique
### Domotique=consiste à centraliser les commandes de certains appareils de votre home sweet home ; et aussi, de plus en plus, à les programmer

## En quoi consiste ce projet:
### Le but de ce projet était l'information , plus précisément me faire informer sur ce qu'il se passe dans le monde en rapport
### avec la domotique , et ainsi utiliser tout les moyens a ma disposition afin d'enregistrer ces traces qui sont venu à moi

## Ce que j'ai pu apprendre :
### La domotique à pu faire un progrès intéressant ces temps ci , bien que certaines informations ne soit pas vraiment Fiable 
### On peut prendre l'exemple de StreetSmart qui n'a certes pas vraiment de rapport avec les maisons , mais qui reste un sujet 
### intéressant vu qu'elle sera encore plus présente dans nos villes ainsi que nos vies.
### La domotique connaît une notoriété assez haute , notamment grâçe a son Google Home ainsi qu'a Alexa mais aussi d'autres nouvelle
### Assistante vocale. Mais elle ne sont pas les seules vu que nos lumières et nos aspirateurs peuvent aussi nous obéir, comme l'aspirateur
### Roomba qui détecte les horaires auxquels il doit faire le ménage. 
### La domotique a aussi était un acteur important pour certain , tout comme les entreprises ou les particuliers lorsque le virus actuel fit
### sa présence 

## Avantage:
### La domotique est un grand pas pour le future vu que celle ci permettra a l'homme de mieux gérer son temps ainsi qu'avoir une maison 
### totalement à son écoute , en plus d'être un fort avantage pour la sécurité de la maison lorsque son propriétaire est absent 

## Inconvenient: 
### La domotique est certes avantageux mais aussi dangereux pour notre vie privée , car l'on peut être surveillé a n'importe quel moment
### par des personnes inconnu et néfaste , ou encore se retrouver dans une société qui pourrait finir dans la surveillance totale 
### tout comme l'oeuvre George Orwell 1984.
### Elle entraînerai aussi un surpoid vu que les tâches ménagères seront faites par les machines , l'homme dépendra donc de l'intelligence artificiel

## Conclusion: 
### La Domotique est un avantage , tout comme un inconvenient, Mais elle reste toute de même un bon outil pour gagner du temps mais il faut savoir varier 
### la technologie chez soi , et faire sois même les tâches domestiques , Nous ne voulons pas d'un monde similaire à Wall-e si nous dépendons tous de nos
### intelligence artificiels 