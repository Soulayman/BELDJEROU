package eu.hautil;

public class Eleve{
    private int ID ,age;
    private String Prenom,Orientation;

    public Eleve(int age,String Prenom,String Orientation){
        this.age=age;
        this.Prenom=Prenom;
        this.Orientation=Orientation;
    }

    public Eleve(int ID, int age,String Prenom,String Orientation){
        this.ID=ID;
        this.age=age;
        this.Prenom=Prenom;
        this.Orientation=Orientation;
    }

    public String getPrenom(){return this.Prenom;}
    public String getOrientation(){return this.Orientation;}
    public int getAge(){return this.age;}

    public String toString(){
        return "l'élève qui s'appelle " +this.Prenom+ " a " +this.age + " ans et a pour Orientation " +this.Orientation +" Son ID est " +this.ID;
    }


}
