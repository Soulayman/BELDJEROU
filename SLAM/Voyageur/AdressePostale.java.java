
class AdressePostale{
    private String voie;
    private String ville;
    private int codePostal;


    public AdressePostale(String voie, String ville, int codePostale){
        this.voie = voie;
        this.ville = ville;
        this.codePostal = codePostale;
    }  


    public AdressePostale(){

    }

    public void afficher(){
        System.out.println("\nVous habitez à "+ this.voie +" "+ this.ville +" "+ this.codePostal);
    }


    public String getVoie(){
        return this.voie;
    }

    public String getVille(){
        return this.ville;
    }

    public int getcodePostal(){
        return this.codePostal;
    }

    public void setVoie(String Toi){
        this.voie=Toi;
    }

    public void setVille(String Qui){
        this.ville=Qui;
    }

    public void setcodePostal(int Decide){
        this.codePostal=Decide;
    }
}