package eu.hautil;

public class Devise {
    private int quantite;
    private String monnaie;

    public Devise(int somme, String monnaie) {
        this.quantite = somme;
        this.monnaie = monnaie;
    }

    public int getQuantite() {
        return quantite;
    }

    public String getMonnaie() {
        return monnaie;
    }

    public Devise add(Devise m) throws MonnaieDifferenteException {
        if (m.getMonnaie().equals(this.getMonnaie())) {
            int qt = this.getQuantite() + m.getQuantite();
            String mn = this.getMonnaie();
            Devise newD = new Devise(qt, mn);
            return newD;
        } else {
            MonnaieDifferenteException exception = new MonnaieDifferenteException(m, this);
            throw  exception;
        }
    }

    @Override
    public boolean equals(Object obj) {
        System.out.println("Je suis dans la méthode equals");
        boolean res = false;
        if (obj instanceof Devise) {
            // conversion explicite cast

            Devise objDevise = (Devise) obj;
            int quant = objDevise.getQuantite();
            String money = objDevise.getMonnaie();
            if (quant == this.quantite && money.equals(this.getMonnaie())) {
                res = true;
            }
        }

        return res;
    }
}
