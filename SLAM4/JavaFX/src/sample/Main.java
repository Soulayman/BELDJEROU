package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[]args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("le ça va");
        Button btn = new Button();
        btn.setText("Hey ! Comment vas-tu ?");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("JE VAIS BIEN !!");
            }


        });

        StackPane rooky = new StackPane();
        rooky.getChildren().add(btn);
        primaryStage.setScene(new Scene(rooky, 300, 275));
        primaryStage.show();
    }

}
