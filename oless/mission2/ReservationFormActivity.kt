package com.example.oless2

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi
import java.time.LocalDate

class ReservationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation_form)
    }

    data class Reservation(val nomHotel : String, val dateDebut: LocalDate, val dateFin: LocalDate): Parcelable{
        @RequiresApi(Build.VERSION_CODES.O)
        constructor(parcel: Parcel) : this(
            parcel.readString().toString(),
            LocalDate.parse(parcel.readString()),
            LocalDate.parse(parcel.readString())
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(nomHotel)
            parcel.writeString(dateDebut.toString())
            parcel.writeString(dateFin.toString())
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Reservation> {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun createFromParcel(parcel: Parcel): Reservation {
                return Reservation(parcel)
            }

            override fun newArray(size: Int): Array<Reservation?> {
                return arrayOfNulls(size)
            }
        }
    }

}