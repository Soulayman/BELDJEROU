<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Reservation;

class Reservation extends Model
{
    protected $table = "reservation"; 
    protected $fillable = [
       'dateD' ,'dateF','idPeriode','created_at','updated_at'
   ];
}

