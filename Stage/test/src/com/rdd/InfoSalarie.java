package com.rdd;

import java.util.ArrayList;
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 
import java.util.Arrays;
import java.util.List;


public class InfoSalarie {
	
	
	private String matricule;
	
	private String type;
	
	private String civilite;
	
	private String nom;
	
	private String ndjf;
	
	private String prenom;
	
	private String Naissance;

	private boolean enErreur;
	
	private List<String> messagesErreur;
	
	private String[] Cand = {"Candidat","Salari�"} ;

	public String[] Civ = {"Monsieur","Madame"};
	
	public String[] Num = {"0","1","2","3","4","5","6","7","8","9"};
		
	private String lieuNaiss;
	
	private String departement;
	
	private String paysNaiss;
	
	private String paysResid;
	
	private String Adresse;
	
	private String complemAdress;
	
	private String codePost;
	
	private String Ville;
	
	private String TelFix;
	
	private String TM;
	
	private String TMP;
	
	private String EmailPerso;
	
	private String EmailPro;
	
	private String TJI;
	
	private String NPJ;
	
	private String DateValide;
	
	private String LieuDeliv;
	
	private String Nationalite;
	
	private String NSecu;
	
	private String NEnfantcharg;
	
	private String SituationFam;
	
	private String SituationPro;
	
	private String SourceDeConnai;
	
	private String AccepteEnvoi;
	
	private String CasierJud;
	
	private String DateEmi;
	
	private String NonEquipMobi;
	
	private String NePasTransm;
	
	private String TypeDeModul;
	
	private String TypeDeTrav;
	
	private String TravailNuit;
	
	private String Comment;
	
	private String DateAncien;
	
	private String NDPAE;
	
	private String DateProch;
	
	private String DateDer;
	
	private String Depart;
	
	private String Service;
	
	private String Fonction;
	
	private String Ndinsc;
	
	private String DateInsc;
	
	private String SectGeo;
	
	private String ModeTransp;
	
	private String Annexe;
	
	private String Handicap;
	
	private String RoleDirect;
	
	private String RoleInspect;
	
	private String RoleChef;
	
	private String RoleComm;
	
	private String Adherecomplementa;
	
	private String SiMotifRef; 
	
	private String DateFinEngag;
	
	private String ModeRegl;
	
	private String IBAN;
	
	private String BIC;
	
	private String PossedePerm;
	
	private String PossedeVehic;
		
	private String PossedeAutreEmplo;
	
	private String Actif;
	
	private String DateArret;
	
	private String ch = "Le champs ";
	
	private String mp = "est incomplet";
	

	public InfoSalarie()
	{
		this.messagesErreur = new ArrayList<>();
	}
	
	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}
	
	public String getLieuNaiss() {
		return lieuNaiss;
	}
	
	public void setLieuNaiss(String lieuNaiss) {
		this.lieuNaiss = lieuNaiss;
	}
	
	public String getDepartement() {
		return departement;
	}
	
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	
	public String getPaysNaiss() {
		return paysNaiss;
	}

	public void setPaysNaiss(String paysNaiss) {
		this.paysNaiss = paysNaiss;
	}

	public String getPaysResid() {
		return paysResid;
	}

	public void setPaysResid(String paysResid) {
		this.paysResid = paysResid;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public String getComplemAdress() {
		return complemAdress;
	}

	public void setComplemAdress(String complemAdress) {
		this.complemAdress = complemAdress;
	}

	public String getCodePost() {
		return codePost;
	}

	public void setCodePost(String codePost) {
		this.codePost = codePost;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getTelFix() {
		return TelFix;
	}

	public void setTelFix(String telFix) {
		TelFix = telFix;
	}

	public String getTM() {
		return TM;
	}

	public void setTM(String tM) {
		TM = tM;
	}

	public String getTMP() {
		return TMP;
	}

	public void setTMP(String tMP) {
		TMP = tMP;
	}

	public String getEmailPerso() {
		return EmailPerso;
	}

	public void setEmailPerso(String emailPerso) {
		EmailPerso = emailPerso;
	}

	public String getEmailPro() {
		return EmailPro;
	}

	public void setEmailPro(String emailPro) {
		EmailPro = emailPro;
	}

	public String getTJI() {
		return TJI;
	}

	public void setTJI(String tJI) {
		TJI = tJI;
	}

	public String getNPJ() {
		return NPJ;
	}

	public void setNPJ(String nPJ) {
		NPJ = nPJ;
	}

	public String getDateValide() {
		return DateValide;
	}

	public void setDateValide(String dateValide) {
		DateValide = dateValide;
	}

	public String getLieuDeliv() {
		return LieuDeliv;
	}

	public void setLieuDeliv(String lieuDeliv) {
		LieuDeliv = lieuDeliv;
	}

	public String getNationalite() {
		return Nationalite;
	}

	public void setNationalite(String nationalite) {
		Nationalite = nationalite;
	}

	public String getNSecu() {
		return NSecu;
	}

	public void setNSecu(String nSecu) {
		NSecu = nSecu;
	}

	public String getNEnfantcharg() {
		return NEnfantcharg;
	}

	public void setNEnfantcharg(String nEnfantcharg) {
		NEnfantcharg = nEnfantcharg;
	}

	public String getSituationFam() {
		return SituationFam;
	}

	public void setSituationFam(String situationFam) {
		SituationFam = situationFam;
	}

	public String getSituationPro() {
		return SituationPro;
	}

	public void setSituationPro(String situationPro) {
		SituationPro = situationPro;
	}

	public String getSourceDeConnai() {
		return SourceDeConnai;
	}

	public void setSourceDeConnai(String sourceDeConnai) {
		SourceDeConnai = sourceDeConnai;
	}

	public String getAccepteEnvoi() {
		return AccepteEnvoi;
	}

	public void setAccepteEnvoi(String accepteEnvoi) {
		AccepteEnvoi = accepteEnvoi;
	}

	public String getCasierJud() {
		return CasierJud;
	}

	public void setCasierJud(String casierJud) {
		CasierJud = casierJud;
	}

	public String getDateEmi() {
		return DateEmi;
	}

	public void setDateEmi(String dateEmi) {
		DateEmi = dateEmi;
	}

	public String getNonEquipMobi() {
		return NonEquipMobi;
	}

	public void setNonEquipMobi(String nonEquipMobi) {
		NonEquipMobi = nonEquipMobi;
	}

	public String getNePasTransm() {
		return NePasTransm;
	}

	public void setNePasTransm(String nePasTransm) {
		NePasTransm = nePasTransm;
	}

	public String getTypeDeModul() {
		return TypeDeModul;
	}

	public void setTypeDeModul(String typeDeModul) {
		TypeDeModul = typeDeModul;
	}

	public String getTypeDeTrav() {
		return TypeDeTrav;
	}

	public void setTypeDeTrav(String typeDeTrav) {
		TypeDeTrav = typeDeTrav;
	}

	public String getTravailNuit() {
		return TravailNuit;
	}

	public void setTravailNuit(String travailNuit) {
		TravailNuit = travailNuit;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getDateAncien() {
		return DateAncien;
	}

	public void setDateAncien(String dateAncien) {
		DateAncien = dateAncien;
	}

	public String getNDPAE() {
		return NDPAE;
	}

	public void setNDPAE(String nDPAE) {
		NDPAE = nDPAE;
	}

	public String getDateProch() {
		return DateProch;
	}

	public void setDateProch(String dateProch) {
		DateProch = dateProch;
	}

	public String getDateDer() {
		return DateDer;
	}

	public void setDateDer(String dateDer) {
		DateDer = dateDer;
	}

	public String getDepart() {
		return Depart;
	}

	public void setDepart(String depart) {
		Depart = depart;
	}

	public String getService() {
		return Service;
	}

	public void setService(String service) {
		Service = service;
	}

	public String getFonction() {
		return Fonction;
	}

	public void setFonction(String fonction) {
		Fonction = fonction;
	}

	public String getNdinsc() {
		return Ndinsc;
	}

	public void setNdinsc(String ndinsc) {
		Ndinsc = ndinsc;
	}

	public String getDateInsc() {
		return DateInsc;
	}

	public void setDateInsc(String dateInsc) {
		DateInsc = dateInsc;
	}

	public String getSectGeo() {
		return SectGeo;
	}

	public void setSectGeo(String sectGeo) {
		SectGeo = sectGeo;
	}

	public String getModeTransp() {
		return ModeTransp;
	}

	public void setModeTransp(String modeTransp) {
		ModeTransp = modeTransp;
	}

	public String getAnnexe() {
		return Annexe;
	}

	public void setAnnexe(String annexe) {
		Annexe = annexe;
	}

	public String getHandicap() {
		return Handicap;
	}

	public void setHandicap(String handicap) {
		Handicap = handicap;
	}

	public String getRoleDirect() {
		return RoleDirect;
	}

	public void setRoleDirect(String roleDirect) {
		RoleDirect = roleDirect;
	}

	public String getRoleInspect() {
		return RoleInspect;
	}

	public void setRoleInspect(String roleInspect) {
		RoleInspect = roleInspect;
	}

	public String getRoleChef() {
		return RoleChef;
	}

	public void setRoleChef(String roleChef) {
		RoleChef = roleChef;
	}

	public String getRoleComm() {
		return RoleComm;
	}

	public void setRoleComm(String roleComm) {
		RoleComm = roleComm;
	}

	public String getAdherecomplementa() {
		return Adherecomplementa;
	}

	public void setAdherecomplementa(String adherecomplementa) {
		Adherecomplementa = adherecomplementa;
	}

	public String getSiMotifRef() {
		return SiMotifRef;
	}

	public void setSiMotifRef(String siMotifRef) {
		SiMotifRef = siMotifRef;
	}

	public String getDateFinEngag() {
		return DateFinEngag;
	}

	public void setDateFinEngag(String dateFinEngag) {
		DateFinEngag = dateFinEngag;
	}

	public String getModeRegl() {
		return ModeRegl;
	}

	public void setModeRegl(String modeRegl) {
		ModeRegl = modeRegl;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public String getBIC() {
		return BIC;
	}

	public void setBIC(String bIC) {
		BIC = bIC;
	}

	public String getPossedePerm() {
		return PossedePerm;
	}

	public void setPossedePerm(String possedePerm) {
		PossedePerm = possedePerm;
	}

	public String getPossedeVehic() {
		return PossedeVehic;
	}

	public void setPossedeVehic(String possedeVehic) {
		PossedeVehic = possedeVehic;
	}

	public String getPossedeAutreEmplo() {
		return PossedeAutreEmplo;
	}

	public void setPossedeAutreEmplo(String possedeAutreEmplo) {
		PossedeAutreEmplo = possedeAutreEmplo;
	}

	public String getActif() {
		return Actif;
	}

	public void setActif(String actif) {
		Actif = actif;
	}

	public String getDateArret() {
		return DateArret;
	}

	public void setDateArret(String dateArret) {
		DateArret = dateArret;
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCivilit�() {
		return civilite;
	}

	public void setCivilit�(String civilite) {
		this.civilite = civilite;
	}

	public String getNdjf() { 
		return ndjf;
	}

	public void setNdjf(String ndjf) {
		this.ndjf = ndjf;
	}
	
	
	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public boolean isEnErreur() {
		return enErreur;
	}

	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}
	

	
	

	public List<String> getMessagesErreur() {
		return messagesErreur;
	}

	public void setMessagesErreur(List<String> messagesErreur) {
		this.messagesErreur = messagesErreur;
	}

	public String getNaissance() {
		return Naissance;
	}

	public void setNaissance(String naissance) {
		Naissance = naissance;
	}
	
	
	
	public boolean verifierChampsTexte(String champsAVerifier)
	{
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	
	public boolean VerifierBool(String noo)
	{
		if(noo == null || noo == "" || !(noo.equalsIgnoreCase(Num[0]) || noo.equalsIgnoreCase(Num[1])))
			return false;
		return true;
	}
	
	public boolean VerifCand(String Sal)
	{
		if(Sal == null || Sal == "" || !(Sal.equalsIgnoreCase(Cand[0]) || Sal.equalsIgnoreCase(Cand[1])))
			return false;
		return true;
	}
	
	public boolean VerifCivi(String Civi)
	{
		if(Civi == null || Civi == "" || !(Civi.equalsIgnoreCase(Civ[0]) || Civi.equalsIgnoreCase(Civ[1])))
			return false;
		return true;
	}
	
	public boolean mail(String ema)
	{
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
	            "[a-zA-Z0-9_+&*-]+)*@" + 
	            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +  
	            "A-Z]{2,7}$"; 

		Pattern pat = Pattern.compile(emailRegex);
		if (ema == null || ema == "") 
            return false; 
        return pat.matcher(ema).matches();
	}
	
	@SuppressWarnings("deprecation")
	public void verifierCoherence() {
		

		if(!verifierChampsTexte(matricule)){
				enErreur = true;
				messagesErreur.add("Le Champ matricule est incomplet");
		}	


		if(!VerifCand(type)){
			enErreur = true;
			messagesErreur.add("Le Champ Type est invalide ou incomplet");
			
		}
		
		if(!VerifCivi(civilite)){
			enErreur = true;
			messagesErreur.add("Le Champ Civilit� est invalide ou incomplet");
		}
		
		
		if (!verifierChampsTexte(nom))
		{
			enErreur = true;
			messagesErreur.add("Le Champ Nom est incomplet");
		}
		
		
		if (!verifierChampsTexte(prenom)) {
			enErreur = true;
			messagesErreur.add("Le Champ Pr�nom est incomplet");
		}

		if (!verifierChampsTexte(Naissance)) {
			enErreur = true;
			messagesErreur.add("Le Champ Date de Naissance est incomplet");	
		}
		

		if(!verifierChampsTexte(lieuNaiss)){
			enErreur =true;
			messagesErreur.add("Le Champ Lieu de Naissance est incomplet ");
		}
		
		
		if(!verifierChampsTexte(Depart)) {
			enErreur = true;
			messagesErreur.add("Le Champ d�part est incomplet");
		}
		
		if(!verifierChampsTexte(paysNaiss)) {
			enErreur = true;
			messagesErreur.add("Le Champ pays de Naissance est incomplet");
		}
		
		
		if(!verifierChampsTexte(paysResid))
		{
			enErreur = true;
			messagesErreur.add("Le Champ pays de R�sidence est incomplet");
		}
		
		
		if(!verifierChampsTexte(Adresse)) {
			enErreur = true;
			messagesErreur.add("Le Champ adresse est incomplet");
		}
		
		if(!verifierChampsTexte(complemAdress)) {
			enErreur = true;
			messagesErreur.add("Le Champ compl�ment adresse est incomplet");
		}
		
		if(!verifierChampsTexte(codePost)) {
			enErreur = true;
			messagesErreur.add("Le Champ Code Postale est incomplet");
		}
		
		if(!verifierChampsTexte(Ville)) {
			enErreur = true;
			messagesErreur.add("Le Champ Ville est incomplet");
		}
		
		if(!verifierChampsTexte(TelFix)) {
			enErreur = true;
			messagesErreur.add("Le Champ T�l�phone Fixe est incomplet");
		}
		
		if(!verifierChampsTexte(TM)) {
			enErreur = true;
			messagesErreur.add("Le Champ T�l�phone Mobile est incomplet");
			
		
		}
		
		if(!verifierChampsTexte(TMP)) {
			enErreur = true;
			messagesErreur.add("Le Champ t�l�phone mobile");
		}
		
		if(!mail(EmailPerso)) {
			enErreur = true;
			messagesErreur.add("Le Champ Email personnel est incomplet");
		}
		
		if(!mail(EmailPro)) {
			enErreur = true;
			messagesErreur.add("Le Champ Email professionnel est incomplet");
		}
		
		if(!verifierChampsTexte(TJI)) {
			enErreur = true;
			messagesErreur.add("Le Champ Type de Justificatif est incomplet");
		}
		
		if(!verifierChampsTexte(NPJ)) {
			enErreur = true;
			messagesErreur.add("Le Champ Num�ro de pi�ce de Justificatif est incomplet");
		}
		
		if(!verifierChampsTexte(DateValide)) {
			enErreur = true;
			messagesErreur.add("Le Champ date de validit� de la pi�ce d'identit� est incomplet");
		}
		
		if(!verifierChampsTexte(LieuDeliv)) {
			enErreur = true;
			messagesErreur.add(ch+ "Lieu De Livraison " +mp);
		}
		
		if(!verifierChampsTexte(Nationalite)) {
			enErreur = true;
			messagesErreur.add(ch+"Nationalit� "+mp);
		}
		
		if(!verifierChampsTexte(NSecu)) {
			enErreur = true;
			messagesErreur.add(ch+"Num�ro de S�curit� Social "+mp );
		}
		
		if(!verifierChampsTexte(NEnfantcharg)) {
			enErreur = true;
			messagesErreur.add(ch+"Nombre d'enfant en charge "+mp);
		
		}
		
		if(!verifierChampsTexte(SituationFam)) {
			enErreur = true;
			messagesErreur.add(ch+"Situation Familial "+mp);
		}
		
		if(!verifierChampsTexte(SituationPro)){
			enErreur = true;
			messagesErreur.add(ch+"Situation Professionnel "+mp);
		}
		
		if(!verifierChampsTexte(SourceDeConnai)) {
			enErreur = true;
			messagesErreur.add(ch+"Source de Connaissance "+mp);
		}
		
		if(!VerifierBool(AccepteEnvoi)) {
			enErreur = true;
			messagesErreur.add(ch+"Accepte l'envoi d'email "+mp+ " ou invalide");
		}
		
		if(!VerifierBool(CasierJud)) {
			enErreur = true;
			messagesErreur.add(ch+"Casier Judiciaire "+mp);
		}
		
		if(!verifierChampsTexte(DateEmi)) {
			enErreur = true;
			messagesErreur.add(ch+"date d'�mission du casier "+mp+" ou invalide");
		}
		
		if(!verifierChampsTexte(NonEquipMobi)){
			enErreur = true;
			messagesErreur.add(ch+"Non �quip� de MOBICLEAN "+mp);
		}
		
		if(!verifierChampsTexte(NePasTransm)) {
			enErreur = true;
			messagesErreur.add(ch+"Ne pas Transmettre � SILAE "+mp);
		}
		
		if(!verifierChampsTexte(TypeDeModul)) {
			enErreur = true;
			messagesErreur.add(ch+"Type de modulation "+mp);
		}
		
		if(!verifierChampsTexte(TypeDeTrav)) {
			enErreur = true;
			messagesErreur.add(ch+"Type de travaux "+mp);
		}
		
		if(!VerifierBool(TravailNuit)) {
			enErreur = true;
			messagesErreur.add(ch+"Travailleur de nuit "+mp+" ou invalide");
		}
		
		if(!verifierChampsTexte(Comment)) {
			enErreur = true;
			messagesErreur.add(ch+"Commentaire "+mp);
		}
		
		if(!verifierChampsTexte(DateAncien)) {
			enErreur = true;
			messagesErreur.add(ch+"Date d'anciennet� "+mp);
		}
		
		if(!verifierChampsTexte(NDPAE)) {
			enErreur = true;
			messagesErreur.add(ch+"N�DPAE "+mp);
		}
		
		if(!verifierChampsTexte(DateProch)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de la prochaine visite m�dicale "+mp);
		}
		
		if(!verifierChampsTexte(DateDer)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de la derni�re visite m�dicale "+mp);
		}
		
		if(!verifierChampsTexte(departement)) {
			enErreur = true;
			messagesErreur.add(ch+"D�partement "+mp);
		}
		
		if(!verifierChampsTexte(Service)) {
			enErreur = true;
			messagesErreur.add(ch+"Service "+mp);
		}
		
		if(!verifierChampsTexte(Fonction)) {
			enErreur = true;
			messagesErreur.add(ch+"Fonction "+mp);
		}
		
		if(!verifierChampsTexte(Ndinsc)) {
			enErreur = true;
			messagesErreur.add(ch+"N� d'inscription P�le Emploi "+mp);
		}
		
		if(!verifierChampsTexte(DateInsc)) {
			enErreur = true;
			messagesErreur.add(ch+"Date d'inscription P�le Emploi "+mp);
		}
		
		if(!verifierChampsTexte(SectGeo)) {
			enErreur = true;
			messagesErreur.add(ch+"Secteur G�ographique "+mp);
		}
		
		if(!verifierChampsTexte(ModeTransp)) {
			enErreur = true;
			messagesErreur.add(ch+"Mode de transport "+mp);
		}
		
		if(!VerifierBool(Annexe)) {
			enErreur = true;
			messagesErreur.add(ch+"Annexe 7 "+mp+" ou invalide");
		}
		
		if(!VerifierBool(Handicap)) {
			enErreur = true;
			messagesErreur.add(ch+"Handicap "+mp+" ou invalide");
		}
		
		if(!VerifierBool(RoleDirect)) {
			enErreur = true;
			messagesErreur.add(ch+"Role directeur d'exploitation "+mp+" ou invalide");
		}
		
		if(!VerifierBool(RoleInspect)){
			enErreur = true;
			messagesErreur.add(ch+"Role d'inspecteur "+mp+" ou invalide");
		}
		
		if(!VerifierBool(RoleChef)) {
			enErreur = true;
			messagesErreur.add(ch+"Role de chef d'�quipe "+mp+" ou invalide");
			}
		
		if(!VerifierBool(RoleComm)) {
			enErreur = true;
			messagesErreur.add(ch+"Role de commercial "+mp+" ou invalide");
		}
		
		if(!VerifierBool(Adherecomplementa)) {
			enErreur = true;
			messagesErreur.add(ch+"Adh�re � la complementaire sant� de l'entreprise "+mp+" ou invalide");
		}
		
		if(!verifierChampsTexte(SiMotifRef)) {
			enErreur = true;
			messagesErreur.add(ch+"Motif de refus d'adh�rer "+mp);
		}
		
		if(!verifierChampsTexte(DateFinEngag)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de fin d'engagement � la compl�mentaire sant� "+mp);
		}
		
		if(!verifierChampsTexte(ModeRegl)) {
			enErreur = true;
			messagesErreur.add(ch+"Mode de r�glement "+mp);
		}
		
		
		if(!verifierChampsTexte(IBAN)) {
			enErreur = true;
			messagesErreur.add(ch+"IBAN "+mp);
		}
		
		
		if(!verifierChampsTexte(BIC)) {
			enErreur = true;
			messagesErreur.add(ch+"BIC "+mp);
		}
		
		if(!VerifierBool(PossedePerm)) {
			enErreur = true;
			messagesErreur.add(ch+"Poss�de le permis de conduire "+mp);
		}
		
		if(!VerifierBool(PossedeVehic)) {
			enErreur = true;
			messagesErreur.add(ch+"Poss�de un v�hicule "+mp);
		}
		
		if(!VerifierBool(PossedeAutreEmplo)) {
			enErreur = true;
			messagesErreur.add(ch+"Poss�de un autre employeur "+mp);
		}
		
		if(!VerifierBool(Actif)) {
			enErreur = true;
			messagesErreur.add(ch+"Actif "+mp);
		}
		
		if(!verifierChampsTexte(DateArret)) {
			enErreur = true;
			messagesErreur.add(ch+"Date d'arr�t "+mp);
		}
		
		
	}
		
		
	
		
		
		
		
		
		
	
	
	@Override
	public String toString() {
		return "InfoSalarie [matricule=" + matricule + ", type=" + type + ", civilite=" + civilite + ", nom=" + nom
				+ ", ndjf=" + ndjf + ", prenom=" + prenom + ", Naissance=" + Naissance + ", enErreur=" + enErreur
				+ ", Cand=" + Arrays.toString(Cand) + ", Civ="
				+ Arrays.toString(Civ) + ", Num=" + Arrays.toString(Num) + ", lieuNaiss=" + lieuNaiss + ", departement="
				+ departement + ", paysNaiss=" + paysNaiss + ", paysResid=" + paysResid + ", Adresse=" + Adresse
				+ ", complemAdress=" + complemAdress + ", codePost=" + codePost + ", Ville=" + Ville + ", TelFix="
				+ TelFix + ", TM=" + TM + ", TMP=" + TMP + ", EmailPerso=" + EmailPerso + ", EmailPro=" + EmailPro
				+ ", TJI=" + TJI + ", NPJ=" + NPJ + ", DateValide=" + DateValide + ", LieuDeliv=" + LieuDeliv
				+ ", Nationalite=" + Nationalite + ", NSecu=" + NSecu + ", NEnfantcharg=" + NEnfantcharg
				+ ", SituationFam=" + SituationFam + ", SituationPro=" + SituationPro + ", SourceDeConnai="
				+ SourceDeConnai + ", AccepteEnvoi=" + AccepteEnvoi + ", CasierJud=" + CasierJud + ", DateEmi="
				+ DateEmi + ", NonEquipMobi=" + NonEquipMobi + ", NePasTransm=" + NePasTransm + ", TypeDeModul="
				+ TypeDeModul + ", TypeDeTrav=" + TypeDeTrav + ", TravailNuit=" + TravailNuit + ", Comment=" + Comment
				+ ", DateAncien=" + DateAncien + ", NDPAE=" + NDPAE + ", DateProch=" + DateProch + ", DateDer="
				+ DateDer + ", Depart=" + Depart + ", Service=" + Service + ", Fonction=" + Fonction + ", Ndinsc="
				+ Ndinsc + ", DateInsc=" + DateInsc + ", SectGeo=" + SectGeo + ", ModeTransp=" + ModeTransp
				+ ", Annexe=" + Annexe + ", Handicap=" + Handicap + ", RoleDirect=" + RoleDirect + ", RoleInspect="
				+ RoleInspect + ", RoleChef=" + RoleChef + ", RoleComm=" + RoleComm + ", Adherecomplementa="
				+ Adherecomplementa + ", SiMotifRef=" + SiMotifRef + ", DateFinEngag=" + DateFinEngag + ", ModeRegl="
				+ ModeRegl + ", IBAN=" + IBAN + ", BIC=" + BIC + ", PossedePerm=" + PossedePerm + ", PossedeVehic="
				+ PossedeVehic + ", PossedeAutreEmplo=" + PossedeAutreEmplo + ", Actif=" + Actif + ", DateArret="
				+ DateArret + ", getCivilite()=" + getCivilite() + ", getLieuNaiss()=" + getLieuNaiss()
				+ ", getDepartement()=" + getDepartement() + ", getPaysNaiss()=" + getPaysNaiss() + ", getPaysResid()="
				+ getPaysResid() + ", getAdresse()=" + getAdresse() + ", getComplemAdress()=" + getComplemAdress()
				+ ", getCodePost()=" + getCodePost() + ", getVille()=" + getVille() + ", getTelFix()=" + getTelFix()
				+ ", getTM()=" + getTM() + ", getTMP()=" + getTMP() + ", getEmailPerso()=" + getEmailPerso()
				+ ", getEmailPro()=" + getEmailPro() + ", getTJI()=" + getTJI() + ", getNPJ()=" + getNPJ()
				+ ", getDateValide()=" + getDateValide() + ", getLieuDeliv()=" + getLieuDeliv() + ", getNationalite()="
				+ getNationalite() + ", getNSecu()=" + getNSecu() + ", getNEnfantcharg()=" + getNEnfantcharg()
				+ ", getSituationFam()=" + getSituationFam() + ", getSituationPro()=" + getSituationPro()
				+ ", getSourceDeConnai()=" + getSourceDeConnai() + ", getAccepteEnvoi()=" + getAccepteEnvoi()
				+ ", getCasierJud()=" + getCasierJud() + ", getDateEmi()=" + getDateEmi() + ", getNonEquipMobi()="
				+ getNonEquipMobi() + ", getNePasTransm()=" + getNePasTransm() + ", getTypeDeModul()="
				+ getTypeDeModul() + ", getTypeDeTrav()=" + getTypeDeTrav() + ", getTravailNuit()=" + getTravailNuit()
				+ ", getComment()=" + getComment() + ", getDateAncien()=" + getDateAncien() + ", getNDPAE()="
				+ getNDPAE() + ", getDateProch()=" + getDateProch() + ", getDateDer()=" + getDateDer()
				+ ", getDepart()=" + getDepart() + ", getService()=" + getService() + ", getFonction()=" + getFonction()
				+ ", getNdinsc()=" + getNdinsc() + ", getDateInsc()=" + getDateInsc() + ", getSectGeo()=" + getSectGeo()
				+ ", getModeTransp()=" + getModeTransp() + ", getAnnexe()=" + getAnnexe() + ", getHandicap()="
				+ getHandicap() + ", getRoleDirect()=" + getRoleDirect() + ", getRoleInspect()=" + getRoleInspect()
				+ ", getRoleChef()=" + getRoleChef() + ", getRoleComm()=" + getRoleComm() + ", getAdherecomplementa()="
				+ getAdherecomplementa() + ", getSiMotifRef()=" + getSiMotifRef() + ", getDateFinEngag()="
				+ getDateFinEngag() + ", getModeRegl()=" + getModeRegl() + ", getIBAN()=" + getIBAN() + ", getBIC()="
				+ getBIC() + ", getPossedePerm()=" + getPossedePerm() + ", getPossedeVehic()=" + getPossedeVehic()
				+ ", getPossedeAutreEmplo()=" + getPossedeAutreEmplo() + ", getActif()=" + getActif()
				+ ", getDateArret()=" + getDateArret() + ", getType()=" + getType() + ", getCivilit�()=" + getCivilit�()
				+ ", getNdjf()=" + getNdjf() + ", getMatricule()=" + getMatricule() + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", isEnErreur()=" + isEnErreur() + ", getMessagesErreur()="
				+ getMessagesErreur() + ", getNaissance()=" + getNaissance() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	

}
