package com.example.oless

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.list_ville.*

class VilleActivity : AppCompatActivity() {
    var villes=arrayOf<String>("Erceville",
        "Ercheu",
        "Erdeven",
        "Ergersheim",
        "Ergny",
        "Ergue-Gaberic",
        "Erize-Saint-Dizier",
        "Ermenonville",
        "Ermont",
        "Ernee",
        "Ernemont-sur-Buchy",
        "Ernestviller",
        "Ernolsheim-Bruche",
        "Erome",
        "Eroudeville",
        "Erquinghem-Lys",
        "Erquinvillers",
        "Erquy",
        "Erre",
        "Errouville",
        "Erstein",
        "Ervauville",
        "Esbarres",
        "Esbly",
        "Escalquens",
        "Escames",
        "Escassefort",
        "Escaudain",
        "Escaudoeuvres",
        "Escautpont",
        "Escazeaux",
        "Eschau",
        "Eschbach-au-Val",
        "Eschentzwiller",
        "Esches",
        "Esclainvillers",
        "Escolives-Sainte-Camille",
        "Escombres-et-le-Chesnois",
        "Escondeaux",
        "Escorneboeuf",
        "Escou",
        "Escout",
        "Escoutoux",
        "Escurolles",
        "Esery",
        "Eslettes",
        "Esmery-Hallon",
        "Esnandes",
        "Esnouveaux",
        "Espagnac",
        "Espalais",
        "Espalion",
        "Espaly-Saint-Marcel",
        "Esparron-de-Verdon",
        "Espedaillac",
        "Espelette",
        "Espeluche",
        "Espezel",
        "Espiet",
        "Espinasses",
        "Espira-de-Conflent",
        "Espirat",
        "Espondeilhan",
        "Esquay-Notre-Dame",
        "Esquay-sur-Seulles",
        "Esquelbecq",
        "Esquerchin",
        "Esquerdes");
    val adapter = VilleAdapter(villes)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_ville )
        villes_recycler_view!!.layoutManager=LinearLayoutManager(this)
        villes_recycler_view!!.adapter=adapter
    }
}