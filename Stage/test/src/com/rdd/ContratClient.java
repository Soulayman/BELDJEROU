package com.rdd;

import java.io.File;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ContratClient {
	public static void main(String[] args) throws Exception{
		File file = new File("D:\\BTS_SIO\\\\Stage\\RDD2021\\Excel\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("CONTRATS CLIENTS");
		
		int rowNum = 1;
		
		List<InfoContratCli> retour = new ArrayList<>();
		
		for (int i = 1; i<=sheet.getLastRowNum(); i++)
		{
			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoContratCli info = new InfoContratCli();
			
			info.setCodeClient(ligneCourante.getCell(0).getStringCellValue());
			info.setCodeSite(ligneCourante.getCell(1).getStringCellValue());
			info.setNContrat(ligneCourante.getCell(2).getStringCellValue());
			info.setDateDebut(ligneCourante.getCell(3).getStringCellValue());
			info.setDateFin(ligneCourante.getCell(4).getStringCellValue());
			info.setTaciteRecon(ligneCourante.getCell(5).getStringCellValue());
			info.setDatePrea(ligneCourante.getCell(6).getStringCellValue());
			info.setMotifContrat(ligneCourante.getCell(7).getStringCellValue());
			info.setCategoContrat(ligneCourante.getCell(8).getStringCellValue());
			info.setRefContrat(ligneCourante.getCell(9).getStringCellValue());
			info.setNDevis(ligneCourante.getCell(10).getStringCellValue());
			info.setCompteAnaly(ligneCourante.getCell(11).getStringCellValue());
			info.setComment(ligneCourante.getCell(12).getStringCellValue());
			info.setMateriel(ligneCourante.getCell(13).getStringCellValue());
			info.setProduits(ligneCourante.getCell(14).getStringCellValue());
			info.setFacturerPassa(ligneCourante.getCell(15).getStringCellValue());
			info.setContratprinci(ligneCourante.getCell(16).getStringCellValue());
			info.setModeRegl(ligneCourante.getCell(17).getStringCellValue());
			info.setFacturerSeul(ligneCourante.getCell(18).getStringCellValue());
			info.setFacturerAvec(ligneCourante.getCell(19).getStringCellValue());
			info.setFacturerSiege(ligneCourante.getCell(20).getStringCellValue(	));
			info.setEcheance(ligneCourante.getCell(21).getStringCellValue());
			info.setValeurX(ligneCourante.getCell(22).getStringCellValue());
			info.setReference(ligneCourante.getCell(23).getStringCellValue());
			info.setDureePrea(ligneCourante.getCell(24).getStringCellValue());
			info.setAdresseFact(ligneCourante.getCell(25).getStringCellValue());
			info.setAdresseDiff(ligneCourante.getCell(26).getStringCellValue());
			info.setAdresseDiff2(ligneCourante.getCell(27).getStringCellValue());
			info.setAdresseDiff3(ligneCourante.getCell(28).getStringCellValue());
			info.setAdresseDiff4(ligneCourante.getCell(29).getStringCellValue());
			info.setAdresseDiff5(ligneCourante.getCell(30).getStringCellValue());
			info.setAdresseDiff6(ligneCourante.getCell(31).getStringCellValue());
			info.setAdresseDiff7(ligneCourante.getCell(32).getStringCellValue());
			info.setNMarche(ligneCourante.getCell(33).getStringCellValue());
			info.setNEngagement(ligneCourante.getCell(34).getStringCellValue());
			
			retour.add(info);
			rowNum++;
		}
		
		for (InfoContratCli info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getCodeClient()+ " ; " +info.getCodeSite()+ " ; " +info.getNContrat()
			+ " ; " +info.getDateDebut()+ " ; " +info.getDateFin()+ " ; " +info.getTaciteRecon()
			+ " ; " +info.getDatePrea()+ " ; " +info.getMotifContrat()+ " ; " +info.getCategoContrat()
			+ " ; " +info.getRefContrat()+ " ; " +info.getNDevis()+ " ; " +info.getCompteAnaly()
			+ " ; " +info.getComment()+ " ; " +info.getMateriel()+ " ; " +info.getProduits()
			+ " ; " +info.getFacturerPassa()+ " ; " +info.getContratprinci()
			+ " ; " +info.getModeRegl()+ " ; " +info.getFacturerSeul()+ " ; " +info.getFacturerAvec()
			+ " ; " +info.getFacturerSiege()+ " ; " +info.getEcheance()+ " ; " +info.getValeurX()
			+ " ; " +info.getReference()+ " ; " +info.getDureePrea()+ " ; " +info.getAdresseDiff()
			+ " ; " +info.getAdresseDiff2()+ " ; " +info.getAdresseDiff3()+ " ; " +info.getAdresseDiff4()
			+ " ; " +info.getAdresseDiff5()+ " ; " +info.getAdresseDiff6()+ " ; " +info.getAdresseDiff7()
			+ " ; " +info.getNMarche()+ " ; " +info.getNEngagement()+ (info.isEnErreur() ? info.getMessagesErreur(): "OK"));
		}
	}
}
