import java.util.ArrayList;

public class Etudiant{
    private String nom;
    private String prenom;

    public Etudiant(String nom, String prenom){
        this.nom=nom;
        this.prenom=prenom;
    }

    public Etudiant(){

    }

    public String getNom(){
        return nom;
    }
    public void setNom(String nom){
        this.nom=nom;
    }

    public String getPrenom(){
        return prenom;
    }

    public void setPrenom(String prenom){
        this.prenom = prenom;
    }

    public static void main(String args[]){
        ArrayList<Etudiant> tatamar=new ArrayList();
        for(int i=0;i<15;i++){
            tatamar.add(new Etudiant());
        }
        tatamar.get(0).setNom("Wesh");
        Etudiant monEtudiant= tatamar.get(0);
        monEtudiant.setNom("Wesh"); 
        //get(0)->objet Etudiant case 0 de tatamar
        tatamar.get(0).setPrenom("triso");
        tatamar.remove(3);
    }


}
