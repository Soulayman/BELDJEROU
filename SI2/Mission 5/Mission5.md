#Mission 5
##Etape 1

###Quelles est la commande utilisé ? 
la commande utiliser pour voir l'adresse ip de la STA est ip a

###Combien d'interface réseau sont identifié ?
il y'a donc 2 interface réseau , "lo" et "eth0"
adresse local : 127.0.0.1/8
adresse ethernet : 10.0.27.40/8

###Quelle interface est utilisée ?
l'interface réseau utilisé est l'interface ethernet donc : 10.0.27.40/8

###Qui a défini cette adresse IP ?
c'est le serveur DHCP

##Etape 2

###Quelle est l'ip du collègue ?

l'ip du collègue esr 10.0.29.92/8

###Quelle est la commande utilisé 

la commande utilisé est "ping"

###Que se passe-t-il ?

des packets infini sont envoyé a la machine "10.0.29.92" du collègue puis on les reçois

###Quel protocole est utilisé pour pinger une machine 

le protocole utilisé est "Internet Control Message Protocol" ICMP se situe au même niveau que le protocole IP

##Etape 3 

###Quelle est la commande utilisé ?

la commande utilisé est ip r 

###Qui a définit la route 

c'est l'administateur réseau qui définit la route 

###Pourquoi est elle définit 

pour pouvoir sortir du réseau 

##Etape 4 

###Quelle est l'adresse choisie ?

l'adresse choisie est : 172.186.0.35/22

###Quelle est la commande utilisée ? 

ip a add 172.186.0.35/22 dev eth0

###Retenter la communication avec le même voisin (@IP de l'étape2). Proposer une explication du comportement 

 il ne se passe rien , la machine communique toujours avec la machine du voisin grace a la switch avec les adresses MAC

##Etape 5 

###Quelle est la commande utilisée ?

ip a del 172.186.0.35/22 dev eth0

###Que se passe-t-il ? Quelle est l'adresse IP de ma STA ?

l'adresse 172.186.0.35/22 se supprime  , l'adresse 10.0.27.40/8 reste donc la meme 

##Etape 8 

###Quelle est la commande utilisé ?

la commande utilisé est ip route add default via 10.0.0.2

###Quelles conséquences produit ce changement 

la machine ne pas acceder a d'autres machine sur d'autres réseau 

###Redéfinir la route par défaur identifiée à l'étape 3

la commande sera ip route add default via 10.0.0.1 , pour la remettre par defaut 





