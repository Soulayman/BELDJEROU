/*Algo : Question 3
	variables : prixttc réel , prixht réel, quantite réel , tva réel , resultat réel , resultatt réel , necessite réel ; nom chaine de caractère , mdp chaine de caractère , t entier
	Debut
		pour t de 0 à 3 faire
			afficher "le mdp est est correct"
			Saisir mdp
			Si mdp <- Padawan alors
				Fin pour
			Fin si 
			Si b==3 alors
				afficher "les 3 tentives ont été echouer"
			Fin si 
		Fin pour
			
		afficher "saisir nom"
		saisir nom
		afficher "saisir prixht"
		saisir prixht
		afficher "saisir quantite"
		saisir quantite 
		afficher "saisir necessite"
		saisir necessite
		Si necessite = 1 alors
			tva <- 5
		Sinon 
			tva <- 20
		Fin si
		prixttc <- prixht +(prixht*tva)/100
		resultat <- prixttc*quantite;
				
		Si quantite<100 alors
			prixttc<-prixht+(prixht*prixttc)/100;
			resultat<-prixttc*quantite;
		Sinon 
			prixttc<-prixht+(prixht*prixttc)/100;
			resultatt<-prixttc*quantite;
			resultat<-resultatt-((resultatt*10)/100);
		Fin si 
				
		afficher "nom + resultat"
		Sinon 
			afficher "mdp incorrect
		Fin si
	Fin	

*/

import java.util.Scanner;

public class Question3{
	public static void main (String[] args){
		
		double prixttc;
		double prixht;
		double quantite;
		double tva;
		double resultat;
		double resultatt;
		int necessite;
		String nom;
		String mdp;
		int t;//inialisation d'une variable
		
		Scanner a4 = new Scanner(System.in);
		
		System.out.println("saisir votre mot de passe ");
		
		for(t=0;t<3;t++){
			mdp=a4.nextLine();
			
			if (mdp.equals("Padawan")){							
				System.out.println("Votre mot de passe est correct");
				System.out.println("saisir nom du biens:");
				nom=a4.nextLine();
				System.out.println("saisir le prix hors taxe du bien:");
				prixht=a4.nextDouble();
				System.out.println("saisir la quantite de bien:");
				quantite=a4.nextDouble();
				System.out.println("si votre produit est de premiere necessite taper 1 si il ne l'est pas repondre 2");
				necessite=a4.nextInt();
				
				if(necessite==1) {
					tva=5;
				}else{
					tva=20;
				}
				
				prixttc=prixht+(prixht*tva)/100;
				resultat=prixttc*quantite;
				
				
				if(quantite<100) {
					prixttc=prixht+(prixht*prixttc)/100;
					resultat=prixttc*quantite;
				}else{
					prixttc=prixht+(prixht*prixttc)/100;
					resultatt=prixttc*quantite;
					resultat=resultatt-((resultatt*10)/100);
				}
				
				
				System.out.println(" le prix final de "+ nom + " est de " + resultat);
			break;//force la sortie de la boucle 
			}else{ 
			System.out.println("Mot de passe incorrect , recommencez");
			}
		}
	}
}