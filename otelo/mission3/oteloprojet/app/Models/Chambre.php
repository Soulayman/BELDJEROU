<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


Class Chambre extends Model
{
     protected $table = "chambre"; 
     protected $fillable = [
        'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase','updated_at','created_at'
    ];
}
