<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Chambre;

Class Chambre extends Model
{
     protected $table = "chambre"; 
     protected $fillable = [
        'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase' 
    ];
}