## 04/09/2020

# dans l'un des article de Actu IA , on nous parle d'intelligence artificiel développer en dehors de la domotique , c'est à dire que l'intelligence artificiel sera diversifier en ville et avoir une "ville intelligente" d'ou le nom du projet "Street Smart"

# voici un extrait de l'article :

## "Le rapport souligne que de nombreux citadins sont mécontents de l’aménagement actuel de leur ville et seraient prêts à s’installer dans une ville davantage engagée dans un programme de transition numérique. 40% des citadins interrogés à travers le monde invoquent de multiples raisons qui pourraient les décider à quitter leur ville dans un avenir proche, parmi lesquelles une « frustration digitale »."

# source de l'article : https://www.actuia.com/actualite/ville-intelligente-la-demande-de-smart-cities-lie-au-developpement-durable-et-a-la-transition-numerique-augmente/