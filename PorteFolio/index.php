<?php session_start();?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Connexion a porte Folio</title>
        <link rel="stylesheet" href="style.css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    </head>
    <body class="text-center connect">


        <div class="text-center">
            <img src="https://www.iconfinder.com/data/icons/finance-209/32/Credit_wallet-512.png" class="rounded connect " alt="..." style="width: 80px; height: 80px; margin-bottom: -250px;">
        </div>

        <div class="h3 mb-3 font-weight-normal " style="margin-top: 190px; padding: 30px">Veuillez vous connectez Soulayman</div>
        
            <form method="post" id="frmConnection" action="verification.php">
                <input type="text" name="pseudo" class="form-control shadow p-3 mb-5 bg-white rounded" placeholder="Pseudonyme" style="width: 250px; margin: auto; text-align: center;"required />
                <br>
                <input type="password" name="modp" class="form-control shadow p-3 mb-5 bg-white rounded"  placeholder="Mot De Passe" style="width: 250px; margin: auto; text-align: center;" required />
                <br>
                <?php

                    $er= '<p class="Er">Vos identifiant sont incorrect</p>';
                     if(isset($_SESSION['erreur']) && $_SESSION['erreur'] ){
                    //echo "erreur";
                        echo $er;
                    }
                     $_SESSION['erreur']=false;
                    ?>
                <br>
                <input type="submit" name="cmd" id="cmd" value=" Connexion " class="btn btn-primary" style="width: 120px; margin: auto; height:40px"/>
                
            </form>

    <div class="invité h5">
        <a class="breadcumb-item" href="Bonsoir1.php">se connecter en tant qu'invité</a>
    </div>
        
        
    </body>
</html>