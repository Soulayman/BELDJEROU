package com.rdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoClient {
	
	
	private boolean enErreur;
	private List<String> messagesErreur;
	public String[] Civ = {"Monsieur","Madame"};
	private String[] Num = {"0","1"};
	private String ch = "Le champs ";
	private String mp = " est incomplet";
	private String bo = " est incomplet ou invalide";
	private String CodeClient;
	private String TypeClient;
	private String StatutClient;
	private String RaisonSo;
	private String Libelle;
	private String NSiteSiege;
	private String NSiret;
	private String TVA;
	private String CodeAPE;
	private String Categorie;
	private String SecteurGeo;
	private String DatePreContact;
	private String RDV;
	private String Comptable;
	private String Auxiliare;
	private String ClientRegu;	//boolean
	private String Exigence;
	private String Comment;
	private String Civilite;
	private String Nom;
	private String Prenom;
	private String Ndjf;
	private String DateNaiss;
	private String LieuNaiss;
	private String SituationFam;
	private String NTelConj;
	private String EmailConj;
	private String ClientInterv;	//boolean
	private String ClesFourn;	//boolean
	private String NombreCles;
	private String CodeCles;
	private String DescCles;
	private String PresAnim;	//boolean
	private String TypeAnim;
	private String NomAnim;
	private String AccesPossAnim;
	private String PresAlarm;	//boolean
	private String Emplacement;
	private String ModeActiv;
	private String ModeDesact;
	private String Beneficiaire;	//boolean
	private String NumCAF;
	private String NbEnfants;
	private String ZoneSco;
	private String NMSA;
	private String AccesCles;
	private String ItiniTransp;
	private String Pays;
	private String Adresse;
	private String ComplemAdres;
	private String CodePost;
	private String Ville;
	private String NTelFix;
	private String NTelMob;
	private String NtelBur;
	private String NFax;
	private String Email;
	private String AcceptMail;	//boolean
	private String Actif;	//boolean
	private String DateArret;
	
	
	
	public boolean isEnErreur() {
		return enErreur;
	}
	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}
	public List<String> getMessagesErreur() {
		return messagesErreur;
	}
	public void setMessagesErreur(List<String> messagesErreur) {
		this.messagesErreur = messagesErreur;
	}
	public String getCodeClient() {
		return CodeClient;
	}
	public void setCodeClient(String codeClient) {
		CodeClient = codeClient;
	}
	public String getTypeClient() {
		return TypeClient;
	}
	public void setTypeClient(String typeClient) {
		TypeClient = typeClient;
	}
	public String getStatutClient() {
		return StatutClient;
	}
	public void setStatutClient(String statutClient) {
		StatutClient = statutClient;
	}
	
	
	public String getRaisonSo() {
		return RaisonSo;
	}
	public void setRaisonSo(String raisonSo) {
		RaisonSo = raisonSo;
	}
	public String getLibelle() {
		return Libelle;
	}
	public void setLibelle(String libelle) {
		Libelle = libelle;
	}
	public String getNSiteSiege() {
		return NSiteSiege;
	}
	public void setNSiteSiege(String nSiteSiege) {
		NSiteSiege = nSiteSiege;
	}
	public String getNSiret() {
		return NSiret;
	}
	public void setNSiret(String nSiret) {
		NSiret = nSiret;
	}
	public String getTVA() {
		return TVA;
	}
	public void setTVA(String tVA) {
		TVA = tVA;
	}
	public String getCodeAPE() {
		return CodeAPE;
	}
	public void setCodeAPE(String codeAPE) {
		CodeAPE = codeAPE;
	}
	public String getCategorie() {
		return Categorie;
	}
	public void setCategorie(String categorie) {
		Categorie = categorie;
	}
	public String getSecteurGeo() {
		return SecteurGeo;
	}
	public void setSecteurGeo(String secteurGeo) {
		SecteurGeo = secteurGeo;
	}
	public String getDatePreContact() {
		return DatePreContact;
	}
	public void setDatePreContact(String datePreContact) {
		DatePreContact = datePreContact;
	}
	public String getRDV() {
		return RDV;
	}
	public void setRDV(String rDV) {
		RDV = rDV;
	}
	public String getComptable() {
		return Comptable;
	}
	public void setComptable(String comptable) {
		Comptable = comptable;
	}
	public String getAuxiliare() {
		return Auxiliare;
	}
	public void setAuxiliare(String auxiliare) {
		Auxiliare = auxiliare;
	}
	public String getClientRegu() {
		return ClientRegu;
	}
	public void setClientRegu(String clientRegu) {
		ClientRegu = clientRegu;
	}
	public String getExigence() {
		return Exigence;
	}
	public void setExigence(String exigence) {
		Exigence = exigence;
	}
	public String getComment() {
		return Comment;
	}
	public void setComment(String comment) {
		Comment = comment;
	}
	public String getCivilite() {
		return Civilite;
	}
	public void setCivilite(String civilite) {
		Civilite = civilite;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public String getNdjf() {
		return Ndjf;
	}
	public void setNdjf(String ndjf) {
		Ndjf = ndjf;
	}
	public String getDateNaiss() {
		return DateNaiss;
	}
	public void setDateNaiss(String dateNaiss) {
		DateNaiss = dateNaiss;
	}
	public String getLieuNaiss() {
		return LieuNaiss;
	}
	public void setLieuNaiss(String lieuNaiss) {
		LieuNaiss = lieuNaiss;
	}
	public String getSituationFam() {
		return SituationFam;
	}
	public void setSituationFam(String situationFam) {
		SituationFam = situationFam;
	}
	public String getNTelConj() {
		return NTelConj;
	}
	public void setNTelConj(String nTelConj) {
		NTelConj = nTelConj;
	}
	public String getEmailConj() {
		return EmailConj;
	}
	public void setEmailConj(String emailConj) {
		EmailConj = emailConj;
	}
	public String getClientInterv() {
		return ClientInterv;
	}
	public void setClientInterv(String clientInterv) {
		ClientInterv = clientInterv;
	}
	public String getClesFourn() {
		return ClesFourn;
	}
	public void setClesFourn(String clesFourn) {
		ClesFourn = clesFourn;
	}
	public String getNombreCles() {
		return NombreCles;
	}
	public void setNombreCles(String nombreCles) {
		NombreCles = nombreCles;
	}
	public String getCodeCles() {
		return CodeCles;
	}
	public void setCodeCles(String codeCles) {
		CodeCles = codeCles;
	}
	public String getDescCles() {
		return DescCles;
	}
	public void setDescCles(String descCles) {
		DescCles = descCles;
	}
	public String getPresAnim() {
		return PresAnim;
	}
	public void setPresAnim(String presAnim) {
		PresAnim = presAnim;
	}
	public String getTypeAnim() {
		return TypeAnim;
	}
	public void setTypeAnim(String typeAnim) {
		TypeAnim = typeAnim;
	}
	public String getNomAnim() {
		return NomAnim;
	}
	public void setNomAnim(String nomAnim) {
		NomAnim = nomAnim;
	}
	public String getAccesPossAnim() {
		return AccesPossAnim;
	}
	public void setAccesPossAnim(String accesPossAnim) {
		AccesPossAnim = accesPossAnim;
	}
	public String getPresAlarm() {
		return PresAlarm;
	}
	public void setPresAlarm(String presAlarm) {
		PresAlarm = presAlarm;
	}
	public String getEmplacement() {
		return Emplacement;
	}
	public void setEmplacement(String emplacement) {
		Emplacement = emplacement;
	}
	public String getModeActiv() {
		return ModeActiv;
	}
	public void setModeActiv(String modeActiv) {
		ModeActiv = modeActiv;
	}
	public String getModeDesact() {
		return ModeDesact;
	}
	public void setModeDesact(String modeDesact) {
		ModeDesact = modeDesact;
	}
	public String getBeneficiaire() {
		return Beneficiaire;
	}
	public void setBeneficiaire(String beneficiaire) {
		Beneficiaire = beneficiaire;
	}
	public String getNumCAF() {
		return NumCAF;
	}
	public void setNumCAF(String numCAF) {
		NumCAF = numCAF;
	}
	public String getNbEnfants() {
		return NbEnfants;
	}
	public void setNbEnfants(String nbEnfants) {
		NbEnfants = nbEnfants;
	}
	public String getZoneSco() {
		return ZoneSco;
	}
	public void setZoneSco(String zoneSco) {
		ZoneSco = zoneSco;
	}
	public String getNMSA() {
		return NMSA;
	}
	public void setNMSA(String nMSA) {
		NMSA = nMSA;
	}
	public String getAccesCles() {
		return AccesCles;
	}
	public void setAccesCles(String accesCles) {
		AccesCles = accesCles;
	}
	public String getItiniTransp() {
		return ItiniTransp;
	}
	public void setItiniTransp(String itiniTransp) {
		ItiniTransp = itiniTransp;
	}
	public String getPays() {
		return Pays;
	}
	public void setPays(String pays) {
		Pays = pays;
	}
	public String getAdresse() {
		return Adresse;
	}
	public void setAdresse(String adresse) {
		Adresse = adresse;
	}
	public String getComplemAdres() {
		return ComplemAdres;
	}
	public void setComplemAdres(String complemAdres) {
		ComplemAdres = complemAdres;
	}
	public String getCodePost() {
		return CodePost;
	}
	public void setCodePost(String codePost) {
		CodePost = codePost;
	}
	public String getVille() {
		return Ville;
	}
	public void setVille(String ville) {
		Ville = ville;
	}
	public String getNTelFix() {
		return NTelFix;
	}
	public void setNTelFix(String nTelFix) {
		NTelFix = nTelFix;
	}
	public String getNTelMob() {
		return NTelMob;
	}
	public void setNTelMob(String nTelMob) {
		NTelMob = nTelMob;
	}
	public String getNtelBur() {
		return NtelBur;
	}
	public void setNtelBur(String ntelBur) {
		NtelBur = ntelBur;
	}
	public String getNFax() {
		return NFax;
	}
	public void setNFax(String nFax) {
		NFax = nFax;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getAcceptMail() {
		return AcceptMail;
	}
	public void setAcceptMail(String acceptMail) {
		AcceptMail = acceptMail;
	}
	public String getActif() {
		return Actif;
	}
	public void setActif(String actif) {
		Actif = actif;
	}
	public String getDateArret() {
		return DateArret;
	}
	public void setDateArret(String dateArret) {
		DateArret = dateArret;
	}	
	
	public InfoClient(){
		this.messagesErreur = new ArrayList<>();
	}
	
	public boolean verifierChampsTexte(String champsAVerifier)
	{
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	public boolean VerifierBool(String noo)
	{
		if(noo == null || noo == "" || !(noo.equalsIgnoreCase(Num[0]) || noo.equalsIgnoreCase(Num[1])))
			return false;
		return true;
	}
	
	public void verifierCoherence() {
		if(!verifierChampsTexte(CodeClient)){
			enErreur = true;
			messagesErreur.add(ch+"Code Client"+mp);
		}
		
		if(!verifierChampsTexte(TypeClient)){
			enErreur = true;
			messagesErreur.add(ch+"Type Client"+mp);
		}
		
		if(!verifierChampsTexte(StatutClient)){
			enErreur = true;
			messagesErreur.add(ch+"Statut Client"+mp);
		}
		
		if(!verifierChampsTexte(RaisonSo)){
			enErreur = true;
			messagesErreur.add(ch+"Raison Sociale"+mp);
		}
		
		if(!verifierChampsTexte(Libelle)){
			enErreur = true;
			messagesErreur.add(ch+"Libell�"+mp);
		}
		
		if(!verifierChampsTexte(NSiteSiege)){
			enErreur = true;
			messagesErreur.add(ch+"N� du site si�ge�"+mp);
		}
		
		if(!verifierChampsTexte(NSiret)){
			enErreur = true;
			messagesErreur.add(ch+"N� Siret+"+mp);
		}
		
		if(!verifierChampsTexte(TVA)) {
			enErreur = true;
			messagesErreur.add(ch+"TVA"+mp);
		}
	
		if(!verifierChampsTexte(CodeAPE)) {
			enErreur = true;
			messagesErreur.add(ch+"Code APE"+mp);
		}
		
		if(!verifierChampsTexte(Categorie)) {
			enErreur = true;
			messagesErreur.add(ch+"Categorie"+mp);
	
		}
		
		if(!verifierChampsTexte(SecteurGeo)) {
			enErreur = true;
			messagesErreur.add(ch+"Secteur Geographique"+mp);
		
		}
		
		if(!verifierChampsTexte(DatePreContact)){
			enErreur = true;
			messagesErreur.add(ch+"Date de premier contrat "+mp);
		}
		
		if(!verifierChampsTexte(RDV)){
			enErreur = true;
			messagesErreur.add(ch+"Rendez-vous"+mp);
		}
			
		if(!verifierChampsTexte(Comptable)) {
			enErreur = true;
			messagesErreur.add(ch+"Comptable"+mp);
		}
		
		
		if(!verifierChampsTexte(Auxiliare)) {
			enErreur = true; 
			messagesErreur.add(ch+"Auxiliaire"+mp); 
		}
		
		if(!VerifierBool(ClientRegu)) {
			enErreur = true; 
			messagesErreur.add(ch+"Client Region"+bo);
		}
		
		if(!verifierChampsTexte(Exigence)) {
			enErreur = true; 
			messagesErreur.add(ch+"Exigence"+mp);
		}

		if(!verifierChampsTexte(Comment)) {
			enErreur = true; 
			messagesErreur.add(ch+"Commentaire"+mp);
		}
		
		if(!verifierChampsTexte(Civilite)) {
			enErreur = true; 
			messagesErreur.add(ch+"Civilit�"+mp);
		}
		
		if(!verifierChampsTexte(Nom)) {
			enErreur = true; 
			messagesErreur.add(ch+"Nom"+mp);
		}
		
		if(!verifierChampsTexte(Prenom)) {
			enErreur = true; 
			messagesErreur.add(ch+"Prenom"+mp);
		}
		
		if(!verifierChampsTexte(Ndjf)) {
			enErreur = true; 
			messagesErreur.add(ch+"Nom de jeune fille"+mp);
		}
		
		if(!verifierChampsTexte(DateNaiss)) {
			enErreur = true; 
			messagesErreur.add(ch+"Date de naissance"+mp);
		}
		
		if(!verifierChampsTexte(LieuNaiss)) {
			enErreur = true; 
			messagesErreur.add(ch+"Lieu de naissance"+mp);
		}

		if(!verifierChampsTexte(SituationFam)) {
			enErreur = true; 
			messagesErreur.add(ch+"Situation familiale"+mp);
		}
		
		if(!verifierChampsTexte(NTelConj)) {
			enErreur = true; 
			messagesErreur.add(ch+"N� T�l�phone Conjoint"+mp);
		}
		
		if(!verifierChampsTexte(EmailConj)) {
			enErreur = true; 
			messagesErreur.add(ch+"Email Conjoint"+mp);
		}
		
		if(!VerifierBool(ClientInterv)) {
			enErreur = true; 
			messagesErreur.add(ch+"Client pr�sent pendant Intervention"+bo);
		}
		
		if(!VerifierBool(ClesFourn)) {
			enErreur = true; 
			messagesErreur.add(ch+"Cl�s fournis"+bo);
		}
		
		if(!verifierChampsTexte(NombreCles)) {
			enErreur = true; 
			messagesErreur.add(ch+"Nombre de cl�s"+mp);
		}
		
		if(!verifierChampsTexte(CodeCles)) {
			enErreur = true; 
			messagesErreur.add(ch+"Code cl�s"+mp);
		}
		
		if(!verifierChampsTexte(DescCles)) {
			enErreur = true; 
			messagesErreur.add(ch+"Description des cl�s"+mp);
		}
		
		if(!VerifierBool(PresAnim)) {
			enErreur = true; 
			messagesErreur.add(ch+"Pr�sence animaux"+bo);
		}
		
		if(!verifierChampsTexte(TypeAnim)) {
			enErreur = true; 
			messagesErreur.add(ch+"Type animal"+mp);
		}
		
		if(!verifierChampsTexte(NomAnim)) {
			enErreur = true; 
			messagesErreur.add(ch+"Nom animal"+mp);
		}
		
		if(!verifierChampsTexte(AccesPossAnim)) {
			enErreur = true; 
			messagesErreur.add(ch+"Acc�s possible animal"+mp);
		}
		
		if(!VerifierBool(PresAlarm)) {
			enErreur = true; 
			messagesErreur.add(ch+"Pr�sence alarme"+bo);
		}
		
		if(!verifierChampsTexte(Emplacement)) {
			enErreur = true; 
			messagesErreur.add(ch+"Emplacement"+mp);
		}
		
		if(!verifierChampsTexte(ModeActiv)) {
			enErreur = true; 
			messagesErreur.add(ch+"Mode activation"+mp);
		}
		
		if(!verifierChampsTexte(ModeDesact)) {
			enErreur = true; 
			messagesErreur.add(ch+"D�sactivation"+mp);
		}
		
		if(!VerifierBool(Beneficiaire)) {
			enErreur = true; 
			messagesErreur.add(ch+"B�n�ficiaire CAF"+bo);
		}
		
		if(!verifierChampsTexte(NumCAF)) {
			enErreur = true; 
			messagesErreur.add(ch+"Num�ro CAF"+mp);
		}
		
		if(!verifierChampsTexte(NbEnfants)) {
			enErreur = true; 
			messagesErreur.add(ch+"nbre d'enfants"+mp);
		}
		
		if(!verifierChampsTexte(ZoneSco)) {
			enErreur = true; 
			messagesErreur.add(ch+"Zone Scolaire"+mp);
		}
		
		if(!verifierChampsTexte(NMSA)) {
			enErreur = true; 
			messagesErreur.add(ch+"Num�ro MSA"+mp);
		}
		
		if(!verifierChampsTexte(AccesCles)) {
			enErreur = true; 
			messagesErreur.add(ch+"Acc�s cl�s"+mp);
		}
		
		if(!verifierChampsTexte(ItiniTransp)) {
			enErreur = true; 
			messagesErreur.add(ch+"Itin�raire de transport"+mp);
		}
		
		if(!verifierChampsTexte(Pays)) {
			enErreur = true; 
			messagesErreur.add(ch+"Pays"+mp);
		}
		
		if(!verifierChampsTexte(Adresse)) {
			enErreur = true; 
			messagesErreur.add(ch+"Adresse"+mp);
		}
		
		if(!verifierChampsTexte(ComplemAdres)) {
			enErreur = true; 
			messagesErreur.add(ch+"Compl�ment d'adresse"+mp);
		}
		
		if(!verifierChampsTexte(CodePost)) {
			enErreur = true; 
			messagesErreur.add(ch+"Code postal"+mp);
		}
		
		if(!verifierChampsTexte(Ville)) {
			enErreur = true; 
			messagesErreur.add(ch+"Ville"+mp);
		}
		
		if(!verifierChampsTexte(NTelFix)) {
			enErreur = true; 
			messagesErreur.add(ch+"N� de t�l�phone fixe"+mp);
		}
		
		if(!verifierChampsTexte(NTelMob)) {
			enErreur = true; 
			messagesErreur.add(ch+"N� de t�l�phone Mobile"+mp);
		}
		
		if(!verifierChampsTexte(NtelBur)) {
			enErreur = true; 
			messagesErreur.add(ch+"N� de t�l�phone du bureau"+mp);
		}
		
		if(!verifierChampsTexte(NFax)) {
			enErreur = true; 
			messagesErreur.add(ch+"N� de fax"+mp);
		}
		
		if(!verifierChampsTexte(Email)) {
			enErreur = true; 
			messagesErreur.add(ch+"Email"+mp);
		}
		
		if(!VerifierBool(AcceptMail)) {
			enErreur = true; 
			messagesErreur.add(ch+"Accepte les mails"+bo);
		}
		
		if(!VerifierBool(Actif)) {
			enErreur = true; 
			messagesErreur.add(ch+"Actif"+bo);
		}
		
		if(!verifierChampsTexte(DateArret)) {
			enErreur = true; 
			messagesErreur.add(ch+"Date d'arr�t"+mp);
		}
		
	}
	@Override
	public String toString() {
		return "InfoClient [enErreur=" + enErreur + ", messagesErreur=" + messagesErreur + ", Civ="
				+ Arrays.toString(Civ) + ", Num=" + Arrays.toString(Num) + ", ch=" + ch + ", mp=" + mp + ", bo=" + bo
				+ ", CodeClient=" + CodeClient + ", TypeClient=" + TypeClient + ", StatutClient=" + StatutClient
				+ ", Libelle=" + Libelle + ", NSiteSiege=" + NSiteSiege + ", NSiret=" + NSiret + ", TVA=" + TVA
				+ ", CodeAPE=" + CodeAPE + ", Categorie=" + Categorie + ", SecteurGeo=" + SecteurGeo
				+ ", DatePreContact=" + DatePreContact + ", RDV=" + RDV + ", Comptable=" + Comptable + ", Auxiliare="
				+ Auxiliare + ", ClientRegu=" + ClientRegu + ", Exigence=" + Exigence + ", Comment=" + Comment
				+ ", Civilite=" + Civilite + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Ndjf=" + Ndjf + ", DateNaiss="
				+ DateNaiss + ", LieuNaiss=" + LieuNaiss + ", SituationFam=" + SituationFam + ", NTelConj=" + NTelConj
				+ ", EmailConj=" + EmailConj + ", ClientInterv=" + ClientInterv + ", ClesFourn=" + ClesFourn
				+ ", NombreCles=" + NombreCles + ", CodeCles=" + CodeCles + ", DescCles=" + DescCles + ", PresAnim="
				+ PresAnim + ", TypeAnim=" + TypeAnim + ", NomAnim=" + NomAnim + ", AccesPossAnim=" + AccesPossAnim
				+ ", PresAlarm=" + PresAlarm + ", Emplacement=" + Emplacement + ", ModeActiv=" + ModeActiv
				+ ", ModeDesact=" + ModeDesact + ", Beneficiaire=" + Beneficiaire + ", NumCAF=" + NumCAF
				+ ", NbEnfants=" + NbEnfants + ", ZoneSco=" + ZoneSco + ", NMSA=" + NMSA + ", AccesCles=" + AccesCles
				+ ", ItiniTransp=" + ItiniTransp + ", Pays=" + Pays + ", Adresse=" + Adresse + ", ComplemAdres="
				+ ComplemAdres + ", CodePost=" + CodePost + ", Ville=" + Ville + ", NTelFix=" + NTelFix + ", NTelMob="
				+ NTelMob + ", NtelBur=" + NtelBur + ", NFax=" + NFax + ", Email=" + Email + ", AcceptMail="
				+ AcceptMail + ", Actif=" + Actif + ", DateArret=" + DateArret + "]";
	}
	
	
}
