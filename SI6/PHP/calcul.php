<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>calcul.php</title>
    </head>
    <body>
    <?php
        $a = 2;                           //a=2
        $a = $a - 1;                      //a=1
        $a++;                             //a=a+1= 2
        $b = 8;                           //b=8
        $b += 2;                          //b=b+2= 10
        $c = $a + $b * $b;                //c=a+b*b <=> 2+10*10 = 102          
        $c+=4 ;                           //c=c+4 = 106 
        $d = $a * $b + $b;                //d= a*b+b <=> 2*10+10 = 30
        $e = $a * ($b + $b);              //e = a*(b+b) <=> 2*20 = 40
        $e-- ;                            //e=e-1= 39
        $f = $a * $b / $a;                //f=a*b/a <=> 2*10/2 = 10
        $f++ ;                            //f=f+1 = 11  
        $g = $b / $a * $a;                //g=b/a*a=10/2*2 = 10      
        $g-=2;                            //g=g-2 <=> 10-2 = 8

        echo "a = $a <br>";
        echo "b = $b <br>";
        echo "c = $c <br>";
        echo "d = $d <br>";
        echo "e = $e <br>";
        echo "f = $f <br>";
        echo "g = $g <br>";
    ?>
    </body>
</html>