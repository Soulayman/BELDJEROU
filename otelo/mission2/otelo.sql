-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 19 nov. 2020 à 13:46
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `otelo`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'standard'),
(2, 'confort'),
(3, 'premium'),
(4, 'luxe');

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

DROP TABLE IF EXISTS `chambre`;
CREATE TABLE IF NOT EXISTS `chambre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nbCouchage` int(11) NOT NULL,
  `porte` varchar(5) NOT NULL,
  `etage` varchar(5) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `baignoire` tinyint(1) NOT NULL,
  `prixBase` double NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`id`, `nbCouchage`, `porte`, `etage`, `idCategorie`, `baignoire`, `prixBase`, `updated_at`, `created_at`) VALUES
(52, 2, 'C', '10', 1, 0, 50, '2020-11-05', '2020-11-05'),
(53, 2, 'C', '10', 1, 0, 50, '2020-11-05', '2020-11-05'),
(54, 2, 'C', '10', 1, 0, 50, '2020-11-05', '2020-11-05'),
(59, 2, 'C', '10', 1, 0, 50, '2020-11-19', '2020-11-19'),
(60, 2, 'C', '10', 1, 0, 50, '2020-11-19', '2020-11-19'),
(61, 2, 'C', '10', 1, 0, 50, '2020-11-19', '2020-11-19'),
(62, 4, 'J', '11', 1, 1, 50, '2020-11-19', '2020-11-19'),
(63, 2, 'H', '6', 1, 1, 116, '2020-11-19', '2020-11-19'),
(64, 2, 'U', '10', 1, 1, 142, '2020-11-19', '2020-11-19'),
(65, 1, 'E', '4', 1, 2, 136, '2020-11-19', '2020-11-19'),
(66, 2, 'O', '3', 1, 2, 231, '2020-11-19', '2020-11-19'),
(67, 4, 'C', '9', 1, 2, 227, '2020-11-19', '2020-11-19'),
(68, 1, 'R', '9', 1, 2, 90, '2020-11-19', '2020-11-19'),
(69, 1, 'R', '14', 1, 1, 174, '2020-11-19', '2020-11-19'),
(70, 1, 'X', '11', 1, 1, 239, '2020-11-19', '2020-11-19'),
(71, 4, 'G', '14', 1, 2, 150, '2020-11-19', '2020-11-19'),
(72, 3, 'P', '4', 1, 2, 85, '2020-11-19', '2020-11-19'),
(73, 4, 'I', '4', 1, 2, 228, '2020-11-19', '2020-11-19'),
(74, 3, 'L', '7', 1, 2, 80, '2020-11-19', '2020-11-19'),
(75, 2, 'P', '13', 1, 1, 109, '2020-11-19', '2020-11-19'),
(76, 1, 'O', '15', 1, 1, 118, '2020-11-19', '2020-11-19'),
(77, 4, 'T', '2', 1, 1, 101, '2020-11-19', '2020-11-19'),
(78, 1, 'S', '14', 1, 1, 227, '2020-11-19', '2020-11-19'),
(79, 1, 'M', '8', 1, 1, 59, '2020-11-19', '2020-11-19'),
(80, 1, 'X', '10', 1, 1, 52, '2020-11-19', '2020-11-19'),
(81, 2, 'D', '15', 1, 2, 223, '2020-11-19', '2020-11-19'),
(82, 4, 'T', '9', 1, 1, 88, '2020-11-19', '2020-11-19'),
(83, 1, 'X', '5', 1, 1, 56, '2020-11-19', '2020-11-19'),
(84, 1, 'Z', '11', 1, 1, 246, '2020-11-19', '2020-11-19'),
(85, 1, 'Z', '6', 1, 2, 51, '2020-11-19', '2020-11-19'),
(86, 3, 'R', '1', 1, 2, 164, '2020-11-19', '2020-11-19'),
(87, 3, 'L', '13', 1, 2, 238, '2020-11-19', '2020-11-19'),
(88, 2, 'Y', '2', 1, 2, 76, '2020-11-19', '2020-11-19'),
(89, 3, 'X', '5', 1, 1, 122, '2020-11-19', '2020-11-19'),
(90, 4, 'R', '4', 1, 1, 242, '2020-11-19', '2020-11-19'),
(91, 4, 'P', '4', 1, 2, 172, '2020-11-19', '2020-11-19');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_reservation`
--

DROP TABLE IF EXISTS `ligne_reservation`;
CREATE TABLE IF NOT EXISTS `ligne_reservation` (
  `idchambre` int(11) NOT NULL,
  `idreservation` int(11) NOT NULL,
  KEY `idchambre` (`idchambre`),
  KEY `idreservation` (`idreservation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2014_10_12_000000_create_users_table', 1),
(22, '2014_10_12_100000_create_password_resets_table', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2020_11_05_155843_create_reservation_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

DROP TABLE IF EXISTS `periode`;
CREATE TABLE IF NOT EXISTS `periode` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `periode`
--

INSERT INTO `periode` (`id`, `libelle`, `coefficient`) VALUES
(1, 'basse', 0.7),
(2, 'moyenne', 1),
(3, 'haute', 2);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dateD` date NOT NULL,
  `dateF` date NOT NULL,
  `idPeriode` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD CONSTRAINT `chambre_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categorie` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
