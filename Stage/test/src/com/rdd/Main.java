package com.rdd;

import java.io.File;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		

		File file = new File("D:\\BTS_SIO\\Stage\\RDD2021\\Excel\\test2.xlsx");

		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("SALARIES");
		
		int rowNum = 1;
		
		List<InfoSalarie> retour = new ArrayList<>();
		
		for (int i = 1; i<=sheet.getLastRowNum(); i++)
		{
			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoSalarie info = new InfoSalarie();
			info.setMatricule(ligneCourante.getCell(0).getStringCellValue());	
			info.setType(ligneCourante.getCell(1).getStringCellValue());
			info.setCivilité(ligneCourante.getCell(2).getStringCellValue());
			info.setNom(ligneCourante.getCell(3).getStringCellValue());
			info.setNdjf(ligneCourante.getCell(4).getStringCellValue());
			info.setPrenom(ligneCourante.getCell(5).getStringCellValue());
			info.setNaissance(ligneCourante.getCell(6).getStringCellValue());
			info.setLieuNaiss(ligneCourante.getCell(7).getStringCellValue());
			info.setDepart(ligneCourante.getCell(8).getStringCellValue());
			info.setPaysNaiss(ligneCourante.getCell(9).getStringCellValue());
			info.setPaysResid(ligneCourante.getCell(10).getStringCellValue());
			info.setAdresse(ligneCourante.getCell(11).getStringCellValue());
			info.setComplemAdress(ligneCourante.getCell(12).getStringCellValue());
			info.setCodePost(ligneCourante.getCell(13).getStringCellValue());
			info.setVille(ligneCourante.getCell(14).getStringCellValue());
			info.setTelFix(ligneCourante.getCell(15).getStringCellValue());
			info.setTM(ligneCourante.getCell(16).getStringCellValue());
			info.setTMP(ligneCourante.getCell(17).getStringCellValue());
			info.setEmailPerso(ligneCourante.getCell(18).getStringCellValue(	));
			info.setEmailPro(ligneCourante.getCell(19).getStringCellValue());
			info.setTJI(ligneCourante.getCell(20).getStringCellValue());
			info.setNPJ(ligneCourante.getCell(21).getStringCellValue());
			info.setDateValide(ligneCourante.getCell(22).getStringCellValue());
			info.setLieuDeliv(ligneCourante.getCell(23).getStringCellValue());
			info.setNationalite(ligneCourante.getCell(24).getStringCellValue());
			info.setNSecu(ligneCourante.getCell(25).getStringCellValue());
			info.setNEnfantcharg(ligneCourante.getCell(26).getStringCellValue());
			info.setSituationFam(ligneCourante.getCell(27).getStringCellValue());
			info.setSituationPro(ligneCourante.getCell(28).getStringCellValue());
			info.setSourceDeConnai(ligneCourante.getCell(29).getStringCellValue());
			info.setAccepteEnvoi(ligneCourante.getCell(30).getStringCellValue());
			info.setCasierJud(ligneCourante.getCell(31).getStringCellValue());
			info.setDateEmi(ligneCourante.getCell(32).getStringCellValue());
			info.setNonEquipMobi(ligneCourante.getCell(33).getStringCellValue());
			info.setNePasTransm(ligneCourante.getCell(34).getStringCellValue());
			info.setTypeDeModul(ligneCourante.getCell(35).getStringCellValue());
			info.setTypeDeTrav(ligneCourante.getCell(36).getStringCellValue());
			info.setTravailNuit(ligneCourante.getCell(37).getStringCellValue());
			info.setComment(ligneCourante.getCell(38).getStringCellValue());
			info.setDateAncien(ligneCourante.getCell(39).getStringCellValue());
			info.setNDPAE(ligneCourante.getCell(40).getStringCellValue());
			info.setDateProch(ligneCourante.getCell(41).getStringCellValue());
			info.setDateDer(ligneCourante.getCell(42).getStringCellValue());
			info.setDepartement(ligneCourante.getCell(43).getStringCellValue());
			info.setService(ligneCourante.getCell(44).getStringCellValue());
			info.setFonction(ligneCourante.getCell(45).getStringCellValue());
			info.setNdinsc(ligneCourante.getCell(46).getStringCellValue());
			info.setDateInsc(ligneCourante.getCell(47).getStringCellValue());
			info.setSectGeo(ligneCourante.getCell(48).getStringCellValue());
			info.setModeTransp(ligneCourante.getCell(49).getStringCellValue());
			info.setAnnexe(ligneCourante.getCell(50).getStringCellValue());
			info.setHandicap(ligneCourante.getCell(51).getStringCellValue());
			info.setRoleDirect(ligneCourante.getCell(52).getStringCellValue());
			info.setRoleInspect(ligneCourante.getCell(53).getStringCellValue());
			info.setRoleChef(ligneCourante.getCell(54).getStringCellValue());
			info.setRoleComm(ligneCourante.getCell(55).getStringCellValue());
			info.setAdherecomplementa(ligneCourante.getCell(56).getStringCellValue());
			info.setSiMotifRef(ligneCourante.getCell(57).getStringCellValue());
			info.setDateFinEngag(ligneCourante.getCell(58).getStringCellValue());
			info.setModeRegl(ligneCourante.getCell(59).getStringCellValue());
			info.setIBAN(ligneCourante.getCell(60).getStringCellValue());
			info.setBIC(ligneCourante.getCell(61).getStringCellValue());
			info.setPossedePerm(ligneCourante.getCell(62).getStringCellValue());
			info.setPossedeVehic(ligneCourante.getCell(63).getStringCellValue());
			info.setPossedeAutreEmplo(ligneCourante.getCell(64).getStringCellValue());
			info.setActif(ligneCourante.getCell(65).getStringCellValue());
			info.setDateArret(ligneCourante.getCell(66).getStringCellValue());
			
			
			
			
			retour.add(info);
			
			rowNum++;
		
			
			
		}
		
		/*int noOfColumns = sheet.getRow(0).getLastCellNum();
		String cellValue = sheet.getRow(noOfColumns).getCell(noOfColumns).getStringCellValue();
		
		Iterator rowIterator = sheet.rowIterator();
		
		
		if(rowIterator.hasNext()) {
			Row headerRow = (Row) rowIterator.next();
			noOfColumns = headerRow.get();
			
			
			System.out.println("number of cells = " + noOfColumns + cellValue);
		}*/
		
		
		
		
		for (InfoSalarie info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getMatricule() + " ; " +info.getType() + " ; " + info.getCivilite() + " ; " + info.getNom()+ " ; " +info.getNdjf()+ " ; " +info.getLieuNaiss()+ " ; " +info.getDepartement()
			+ " ; " +info.getPaysNaiss()+ " ; " +info.getPaysResid()+ " ; " + info.getAdresse()+ " ; " +info.getComplemAdress()
			+ " ; " +info.getCodePost()+ " ; " +info.getVille()+ " ; " +info.getTelFix()+ " ; " +info.getTM()+ " ; " +info.getTMP()+ " ; " +info.getEmailPerso()+ " ; " +info.getEmailPro()+ " ; " +info.getTJI()+ " ; " +info.getNPJ()
			+ " ; " +info.getDateValide()+ " ; " +info.getLieuDeliv()+ " ; " +info.getNationalite()+ " ; " +info.getNSecu()
			+ " ; " +info.getNEnfantcharg()+ " ; " +info.getSituationFam()+ " ; " +info.getSituationPro()+ " ; " +info.getSourceDeConnai()
			+ " ; " +info.getAccepteEnvoi()+ " ; " +info.getCasierJud()+ " ; " +info.getDateEmi()+ " ; " +info.getNonEquipMobi()
			+ " ; " +info.getNePasTransm()+ " ; " +info.getTypeDeModul()+ " ; " +info.getTypeDeTrav()+ " ; " +info.getTravailNuit()				
			+ " ; " +info.getComment()+ " ; " +info.getDateAncien()+ " ; " +info.getNDPAE()+ " ; " +info.getDateProch()+ " ; " +info.getDepart()
			+ " ; " +info.getService()+ " ; " +info.getFonction()+ " ; " +info.getNdinsc()+ " ; " +info.getDateInsc()+ " ; " +info.getSectGeo()
			+ " ; " +info.getModeTransp()+ " ; " +info.getAnnexe()+ " ; " +info.getHandicap()+ " ; " +info.getRoleDirect()+ " ; " +info.getRoleInspect()
			+ " ; " +info.getRoleChef()+ " ; " +info.getRoleComm()+ " ; " +info.getAdherecomplementa()+ " ; " +info.getSiMotifRef()+ " ; " +info.getDateFinEngag()
			+ " ; " +info.getModeRegl()+ " ; " +info.getIBAN()+ " ; " +info.getBIC()+ " ; " +info.getPossedePerm()+ " ; " +info.getPossedeVehic()
			+ " ; " +info.getPossedeAutreEmplo()+ " ; " +info.getActif()+ " ; " +info.getDateArret() +  (info.isEnErreur() ? info.getMessagesErreur(): "OK"));
		}
		
	}

}
