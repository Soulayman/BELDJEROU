package com.example.oless

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_reservation_form.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class ReservationFormActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation_form)


        button_form.setOnClickListener {


            val reservation = Reservation(
                nom_view.text.toString(),
                LocalDate.parse(dateDebut_view.text),
                LocalDate.parse(dateFin_view.text)
            )
            val intent = Intent(this, ReservationConfirmActivity::class.java)
            intent.putExtra("reservation", reservation)


            val fragment = ConfirmReservationFragment()
            fragment.listener = object : ConfirmReservationFragment.ConfirmDeleteListener {
                override fun onDialogPositiveClick() {
                    Log.i("FormActivity", "onDialogPositiveClick()")
                    startActivity(intent)
                }

                override fun onDialogNegativeClick() {
                    Log.i("FormActivity", "onDialogNegativeClick()")
                }
            }
            fragment.show(supportFragmentManager, "confirmzaza")

        }

            var cal = Calendar.getInstance()
            val dateSetListener = object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                       dayOfMonth: Int) {
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, monthOfYear)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE)
                    dateDebut_view.setText(sdf.format(cal.time))

                }
            }
            dateDebut_view.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    DatePickerDialog(this@ReservationFormActivity,
                        dateSetListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show()
                }
            })

            var cal2 = Calendar.getInstance()
            val dateSetListener2 = object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                       dayOfMonth: Int) {
                    cal2.set(Calendar.YEAR, year)
                    cal2.set(Calendar.MONTH, monthOfYear)
                    cal2.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    val sdf2 = SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE)
                    dateFin_view.setText(sdf2.format(cal2.time))
                }
            }
            dateFin_view.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    DatePickerDialog(this@ReservationFormActivity,
                        dateSetListener2,
                        // set DatePickerDialog to point to today's date when it loads up
                        cal2.get(Calendar.YEAR),
                        cal2.get(Calendar.MONTH),
                        cal2.get(Calendar.DAY_OF_MONTH)).show()
                }
            })


        }
    }