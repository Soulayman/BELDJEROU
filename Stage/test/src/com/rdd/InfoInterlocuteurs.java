package com.rdd;

import java.util.List;
import java.util.regex.Matcher; 
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoInterlocuteurs {
	private List<String> messagesErreur;
	
	private String CodeFourn;
	
	private String CodeClient;
	
	private String CodeSite;
	
	private String TypeInterlo;
	
	private String Civilite;
	
	private String Nom;
	
	private String Prenom;
	
	private String NTelMob;
	
	private String NtelFix;
	
	private String Email;
	
	private String RecoitEmail;
	
	private String RecoitRapMob;
	
	private String RecoitRapQual;
	
	private String PersonConf;
	
	private String Rythme;
	
	private String ch = "Le champs ";
	
	private String mp = " est incomplet";
	
	private boolean enErreur;
	
	public InfoInterlocuteurs()
	{
		this.messagesErreur = new ArrayList<>();
	}

	public List<String> getMessagesErreur() {
		return messagesErreur;
	}

	public void setMessagesErreur(List<String> messagesErreur) {
		this.messagesErreur = messagesErreur;
	}
	
	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}
	
	public boolean isEnErreur() {
		return enErreur;
	}



	public String getCodeFourn() {
		return CodeFourn;
	}

	public void setCodeFourn(String codeFourn) {
		CodeFourn = codeFourn;
	}

	public String getCodeClient() {
		return CodeClient;
	}

	public void setCodeClient(String codeClient) {
		CodeClient = codeClient;
	}

	public String getCodeSite() {
		return CodeSite;
	}

	public void setCodeSite(String codeSite) {
		CodeSite = codeSite;
	}

	public String getTypeInterlo() {
		return TypeInterlo;
	}

	public void setTypeInterlo(String typeInterlo) {
		TypeInterlo = typeInterlo;
	}

	public String getCivilite() {
		return Civilite;
	}

	public void setCivilite(String civilite) {
		Civilite = civilite;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getNTelMob() {
		return NTelMob;
	}

	public void setNTelMob(String nTelMob) {
		NTelMob = nTelMob;
	}

	public String getNtelFix() {
		return NtelFix;
	}

	public void setNtelFix(String ntelFix) {
		NtelFix = ntelFix;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getRecoitEmail() {
		return RecoitEmail;
	}

	public void setRecoitEmail(String recoitEmail) {
		RecoitEmail = recoitEmail;
	}

	public String getRecoitRapMob() {
		return RecoitRapMob;
	}

	public void setRecoitRapMob(String recoitRapMob) {
		RecoitRapMob = recoitRapMob;
	}

	public String getRecoitRapQual() {
		return RecoitRapQual;
	}

	public void setRecoitRapQual(String recoitRapQual) {
		RecoitRapQual = recoitRapQual;
	}

	public String getPersonConf() {
		return PersonConf;
	}

	public void setPersonConf(String personConf) {
		PersonConf = personConf;
	}

	public String getRythme() {
		return Rythme;
	}

	public void setRythme(String rythme) {
		Rythme = rythme;
	}

	public String getCh() {
		return ch;
	}

	public void setCh(String ch) {
		this.ch = ch;
	}

	public String getMp() {
		return mp;
	}

	public void setMp(String mp) {
		this.mp = mp;
	}




	
	public boolean verifierChampsTexte(String champsAVerifier)
	{
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	
	
	public void verifierCoherence() {
		if(!verifierChampsTexte(CodeFourn)){
			enErreur = true;
			messagesErreur.add(ch+"Code fournisseur"+mp);
		}
		
		if(!verifierChampsTexte(CodeClient)){
			enErreur = true;
			messagesErreur.add(ch+"Code Client"+mp);
		}
		
		if(!verifierChampsTexte(CodeSite)){
			enErreur = true;
			messagesErreur.add(ch+"Code Site"+mp);
		}
		
		if(!verifierChampsTexte(TypeInterlo)){
			enErreur = true;
			messagesErreur.add(ch+"Type Interlocuteur"+mp);
		}
		
		if(!verifierChampsTexte(Civilite)){
			enErreur = true;
			messagesErreur.add(ch+"Civilit�"+mp);
		}

		if(!verifierChampsTexte(Nom)){
			enErreur = true;
			messagesErreur.add(ch+"Nom"+mp);
		}
		
		
		if(!verifierChampsTexte(Prenom)){
			enErreur = true;
			messagesErreur.add(ch+"Pr�nom"+mp);
		}
		
		
		if(!verifierChampsTexte(NTelMob)){
			enErreur = true;
			messagesErreur.add(ch+"N� de T�l�phone mobile"+mp);
		}
		
		
		if(!verifierChampsTexte(NtelFix)){
			enErreur = true;
			messagesErreur.add(ch+"N� de T�l�phone Fixe"+mp);
		}
		
		if(!verifierChampsTexte(Email)){
			enErreur = true;
			messagesErreur.add(ch+"Email"+mp);
		}
		
		if(!verifierChampsTexte(RecoitEmail)){
			enErreur = true;
			messagesErreur.add(ch+"re�oit les emails de factures"+mp);
		}
		
		if(!verifierChampsTexte(RecoitRapMob)){
			enErreur = true;
			messagesErreur.add(ch+"re�oit les rapports Mobiclean"+mp);
		}
		
		if(!verifierChampsTexte(RecoitRapQual)){
			enErreur = true;
			messagesErreur.add(ch+"re�oit les rapports Qualimobi"+mp);
		}
		
		if(!verifierChampsTexte(PersonConf)){
			enErreur = true;
			messagesErreur.add(ch+"Personne de confiance"+mp);
		}
		
		if(!verifierChampsTexte(Rythme)){
			enErreur = true;
			messagesErreur.add(ch+"Rythme passage coordination pro"+mp);
		}
	}

	@Override
	public String toString() {
		return "InfoInterlocuteurs [messagesErreur=" + messagesErreur + ", CodeFourn=" + CodeFourn + ", CodeClient="
				+ CodeClient + ", CodeSite=" + CodeSite + ", TypeInterlo=" + TypeInterlo + ", Civilite=" + Civilite
				+ ", Nom=" + Nom + ", Prenom=" + Prenom + ", NTelMob=" + NTelMob + ", NtelFix=" + NtelFix + ", Email="
				+ Email + ", RecoitEmail=" + RecoitEmail + ", RecoitRapMob=" + RecoitRapMob + ", RecoitRapQual="
				+ RecoitRapQual + ", PersonConf=" + PersonConf + ", Rythme=" + Rythme + ", ch=" + ch + ", mp=" + mp
				+ ", enErreur=" + enErreur + "]";
	}
	
	
	
	
}
