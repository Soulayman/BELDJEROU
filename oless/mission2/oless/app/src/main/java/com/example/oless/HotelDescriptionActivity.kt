package com.example.oless

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_hotel_description.*

class HotelDescriptionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_description)
        setContentView(R.layout.activity_hotel_description)
        //val action = intent.action  //on ne s en sert pas ici
        //val isHotel = intent.hasCategory("Hotel") //on ne s en sert pas ici
        val name = intent.getStringExtra("Underground Hôtel")
        activity_hotel_description_textview.setText(name)
    }
}