package com.example.oless

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_reservation_form.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_hotel_description.setOnClickListener{
            println("youhou")
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            startActivity(intent)
        }
        activity_hotel_description.setOnClickListener{
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            intent.action=Intent.ACTION_VIEW
            intent.addCategory("Hotel")
            intent.putExtra("Underground Hôtel","l'Underground Hôtel est un batîment spécial , ici vous ne trouverez pas un hôtel classique lamda mais souterrain ")
            startActivity(intent)
        }
        activity_reservation_form.setOnClickListener{
            println("youhou")
            val intent = Intent(this, ReservationFormActivity::class.java)
            intent.action=Intent.ACTION_VIEW
            startActivity(intent)
        }
    }
}

