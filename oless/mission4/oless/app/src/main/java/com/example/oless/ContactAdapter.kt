package com.example.oless

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class ContactAdapter(val contacts : ArrayList<Contact>, /*new*/ val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        /*new*/
        val cardView = itemView.findViewById(R.id.card_view) as CardView
        val nom = itemView.findViewById(R.id.item_nom) as TextView
        val numero = itemView.findViewById(R.id.item_numero) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.item_contact, parent, false)
        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]
        holder.nom.text = contact.nom
        holder.numero.text = contact.numero
        /*new*/
        holder.cardView.tag = position
        holder.cardView.setOnClickListener(itemClickListener)
    }
}