//fichier:Question4.java
//

import java.util.Scanner;

public class Question4{
	public static void main (String[] args){
		
		double prixttc;
		double prixht;
		double quantite;
		double tva=20;
		double resultat;
		String nom;
		
		Scanner a4 = new Scanner(System.in);
		
		System.out.println("saisir nom du biens:");
		nom=a4.nextLine();
		System.out.println("saisir le prix hors taxe du bien:");
		prixht=a4.nextDouble();
		System.out.println("saisir la quantite de bien:");
		quantite=a4.nextDouble();
		
		prixttc=prixht+(prixht*tva)/100;
		resultat=prixttc*quantite;
		
		System.out.println(" resultat "+nom +" est de: "+ resultat);
	}
}