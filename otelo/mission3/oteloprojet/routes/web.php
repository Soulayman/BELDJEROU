<?php

use Illuminate\Support\Facades\Route;
use App\Models\Chambre;
use Illuminate\Support\Facades\Schema;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/accueil','App\Http\Controllers\PremierController@home'); 

Route::get('/chambre','App\Http\Controllers\ChambreController@store');

Route::get('/newreservation','App\Http\Controllers\ReservationController@create');

Route::post('/storereservation','App\Http\Controllers\ReservationController@store')->name('reservation.store');
