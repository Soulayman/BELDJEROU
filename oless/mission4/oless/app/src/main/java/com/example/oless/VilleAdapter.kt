package com.example.oless

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class VilleAdapter(val villes: Array<String>) : RecyclerView.Adapter<VilleAdapter.ViewHolder>() {

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val nom=itemView.findViewById(R.id.item_nom) as TextView
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.item_ville,parent,false)
        return ViewHolder(viewItem)
    }
    override fun getItemCount(): Int {
        return villes.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ville= villes[position]
        holder.nom.text=ville
    }
}