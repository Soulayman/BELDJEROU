package com.rdd;

import java.util.ArrayList;

import java.util.regex.Matcher; 
import java.util.regex.Pattern; 
import java.util.Arrays;
import java.util.List;


public class InfoContratSal {
	
	private List<String> messagesErreur;
	
	private String matriSal;
	
	private String TypeContr;
	
	private String DateDebContr;
	
	private String DateFinContr;
	
	private String MotifDebEmplo;
	
	private String HoraireDeDebuEmplo;
	
	private String DureePeriodeEssai;
	
	private String TypeDePeriodeEssai;
	
	private String DateFinPeriode;
	
	private String ClassificaMetier;
	
	private String ClassificaMetierSAP;
	
	private String NaturEmplo;
	
	private String TauxHorai;
	
	private String SMIC; //Boolean
	
	private String NePasTransm;
	
	private String nbHeureHebdo;
	
	private String nbHeureMensu;
	
	private String nbJourTrav;
	
	private String Lundi; //Boolean
	
	private String Mardi; //Boolean
	
	private String Mercredi; //Boolean
	
	private String Jeudi; //Boolean
	
	private String Vendredi;
	
	private String Samedi; //Boolean 
	
	private String Dimanche; //Boolean
	
	private String ContratSign; //Boolean
	
	private String ch = "Le champs ";
	
	private String mp = " est incomplet";
	
	private String bo = " est incomplet ou invalide";
	
	private String[] Num = {"0","1"};
	
	private boolean enErreur;
	

	
	public InfoContratSal()
	{
		this.messagesErreur = new ArrayList<>();
	}
	
	public List<String> getMessagesErreur() {
		return messagesErreur;
	}
	
	public boolean isEnErreur() {
		return enErreur;
	}

	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}
	

	
	public String getMatriSal() {
		return matriSal;
	}

	public void setMatriSal(String matriSal) {
		this.matriSal = matriSal;
	}

	public String getTypeContr() {
		return TypeContr;
	}

	public void setTypeContr(String typeContr) {
		TypeContr = typeContr;
	}

	public String getDateDebContr() {
		return DateDebContr;
	}

	public void setDateDebContr(String dateDebContr) {
		DateDebContr = dateDebContr;
	}

	public String getDateFinContr() {
		return DateFinContr;
	}

	public void setDateFinContr(String dateFinContr) {
		DateFinContr = dateFinContr;
	}

	public String getMotifDebEmplo() {
		return MotifDebEmplo;
	}

	public void setMotifDebEmplo(String motifDebEmplo) {
		MotifDebEmplo = motifDebEmplo;
	}

	public String getHoraireDeDebuEmplo() {
		return HoraireDeDebuEmplo;
	}

	public void setHoraireDeDebuEmplo(String horaireDeDebuEmplo) {
		HoraireDeDebuEmplo = horaireDeDebuEmplo;
	}

	public String getDureePeriodeEssai() {
		return DureePeriodeEssai;
	}

	public void setDureePeriodeEssai(String dureePeriodeEssai) {
		DureePeriodeEssai = dureePeriodeEssai;
	}

	public String getTypeDePeriodeEssai() {
		return TypeDePeriodeEssai;
	}

	public void setTypeDePeriodeEssai(String typeDePeriodeEssai) {
		TypeDePeriodeEssai = typeDePeriodeEssai;
	}

	public String getDateFinPeriode() {
		return DateFinPeriode;
	}

	public void setDateFinPeriode(String dateFinPeriode) {
		DateFinPeriode = dateFinPeriode;
	}

	public String getClassificaMetier() {
		return ClassificaMetier;
	}

	public void setClassificaMetier(String classificaMetier) {
		ClassificaMetier = classificaMetier;
	}

	public String getClassificaMetierSAP() {
		return ClassificaMetierSAP;
	}

	public void setClassificaMetierSAP(String classificaMetierSAP) {
		ClassificaMetierSAP = classificaMetierSAP;
	}

	public String getNaturEmplo() {
		return NaturEmplo;
	}

	public void setNaturEmplo(String naturEmplo) {
		NaturEmplo = naturEmplo;
	}

	public String getTauxHorai() {
		return TauxHorai;
	}

	public void setTauxHorai(String tauxHorai) {
		TauxHorai = tauxHorai;
	}

	public String getSMIC() {
		return SMIC;
	}

	public void setSMIC(String sMIC) {
		SMIC = sMIC;
	}

	public String getNePasTransm() {
		return NePasTransm;
	}

	public void setNePasTransm(String nePasTransm) {
		NePasTransm = nePasTransm;
	}

	public String getNbHeureHebdo() {
		return nbHeureHebdo;
	}

	public void setNbHeureHebdo(String nbHeureHebdo) {
		this.nbHeureHebdo = nbHeureHebdo;
	}

	public String getNbHeureMensu() {
		return nbHeureMensu;
	}

	public void setNbHeureMensu(String nbHeureMensu) {
		this.nbHeureMensu = nbHeureMensu;
	}

	public String getNbJourTrav() {
		return nbJourTrav;
	}

	public void setNbJourTrav(String nbJourTrav) {
		this.nbJourTrav = nbJourTrav;
	}

	public String getLundi() {
		return Lundi;
	}

	public void setLundi(String lundi) {
		Lundi = lundi;
	}

	public String getMardi() {
		return Mardi;
	}

	public void setMardi(String mardi) {
		Mardi = mardi;
	}

	public String getMercredi() {
		return Mercredi;
	}

	public void setMercredi(String mercredi) {
		Mercredi = mercredi;
	}

	public String getJeudi() {
		return Jeudi;
	}

	public void setJeudi(String jeudi) {
		Jeudi = jeudi;
	}
	
	public String getVendredi() {
		return Vendredi;
	}

	public void setVendredi(String vendredi) {
		Vendredi = vendredi;
	}

	public String getSamedi() {
		return Samedi;
	}

	public void setSamedi(String samedi) {
		Samedi = samedi;
	}

	public String getDimanche() {
		return Dimanche;
	}

	public void setDimanche(String dimanche) {
		Dimanche = dimanche;
	}

	public String getContratSign() {
		return ContratSign;
	}

	public void setContratSign(String contratSign) {
		ContratSign = contratSign;
	}
	
	public boolean verifierChampsTexte(String champsAVerifier)
	{
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	public boolean boolea(String boo) {
		if(boo == null || boo == "" || !(boo.equalsIgnoreCase(Num[0]) || boo.equalsIgnoreCase(Num[1])))
			return false;
		
		return true;
		
	}
	
	public void verifierCoherence() {
	
		if(!verifierChampsTexte(matriSal)){
			enErreur = true;
			messagesErreur.add(ch+"Matricule Salari�"+mp);
		
		}
		
		if(!verifierChampsTexte(TypeContr)) {
			enErreur = true;
			messagesErreur.add(ch+"Type de contrat"+mp);
		}
		
		if(!verifierChampsTexte(DateDebContr)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de d�but du contrat"+mp);
		}
		
		if(!verifierChampsTexte(DateFinContr)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de fin du contrat"+mp);
		}
		
		if(!verifierChampsTexte(MotifDebEmplo)) {
			enErreur = true;
			messagesErreur.add(ch+"Motif de d�but de l'emploi"+mp);
		}
		
		if(!verifierChampsTexte(HoraireDeDebuEmplo)) {
			enErreur = true;
			messagesErreur.add(ch+"Horaire de d�but d'emploi"+mp);
		}
		
		if(!verifierChampsTexte(DureePeriodeEssai)) {
			enErreur = true;
			messagesErreur.add(ch+"Dur�e p�riode d'essai"+mp);
		}
		
		if(!verifierChampsTexte(TypeDePeriodeEssai)) {
			enErreur = true;
			messagesErreur.add(ch+"Type de periode d'essai"+mp);
		}
		
		if(!verifierChampsTexte(DateFinPeriode)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de fin de p�riode"+mp);
		}
		
		if(!verifierChampsTexte(ClassificaMetier)) {
			enErreur = true;
			messagesErreur.add(ch+"Classification M�tier"+mp);
		}
		
		if(!verifierChampsTexte(ClassificaMetierSAP)) {
			enErreur = true;
			messagesErreur.add(ch+"Classification M�tier SAP"+mp);
		}
		
		if(!verifierChampsTexte(NaturEmplo)) {
			enErreur = true;
			messagesErreur.add(ch+"Nature de l'emploi"+mp);
			
		}
		
		if(!verifierChampsTexte(TauxHorai)) {
			enErreur = true;
			messagesErreur.add(ch+"Taux horaire"+mp);
		}
		
		if(!boolea(SMIC)) {
			enErreur = true;
			messagesErreur.add(ch+"SMIC"+bo);
		}
		
		if(!verifierChampsTexte(NePasTransm)) {
			enErreur = true;
			messagesErreur.add(ch+"Ne pas transmettre le taux"+mp);
		}
		
		if(!verifierChampsTexte(nbHeureHebdo)) {
			enErreur = true;
			messagesErreur.add(ch+"nombre d'heure hebdomadaires"+mp);
		}
		
		if(!verifierChampsTexte(nbHeureMensu)) {
			enErreur = true;
			messagesErreur.add(ch+"nombre d'heure mensuelles"+mp);
		}
		
		if(!verifierChampsTexte(nbJourTrav)) {
			enErreur = true;
			messagesErreur.add(ch+"nombre de jours travaill�s"+mp);
		}
		
		if(!boolea(Lundi)) {
			enErreur = true;
			messagesErreur.add(ch+"Lundi"+bo);
		}
		
		if(!boolea(Mardi)) {
			enErreur = true;
			messagesErreur.add(ch+"Mardi"+bo);
		}
		
		if(!boolea(Mercredi)) {
			enErreur = true;
			messagesErreur.add(ch+"Mercredi"+bo);
		}
		
		if(!boolea(Jeudi)) {
			enErreur = true;
			messagesErreur.add(ch+"Jeudi"+bo);
		}
		
		if(!boolea(Vendredi)) {
			enErreur = true;
			messagesErreur.add(ch+"Vendredi"+bo);
		}
		
		if(!boolea(Samedi)) {
			enErreur = true;
			messagesErreur.add(ch+"Samedi"+bo);
		}
		
		if(!boolea(Dimanche)) {
			enErreur = true;
			messagesErreur.add(ch+"Dimanche"+bo);
		}
		
		if(!verifierChampsTexte(ContratSign)) {
			enErreur = true;
			messagesErreur.add(ch+"ContratSign"+mp);
		}
			
		
	}
	
	
	
	
	
	@Override
	public String toString() {
		return "InfoContratSal [messagesErreur=" + messagesErreur + ", matriSal=" + matriSal + ", TypeContr="
				+ TypeContr + ", DateDebContr=" + DateDebContr + ", DateFinContr=" + DateFinContr + ", MotifDebEmplo="
				+ MotifDebEmplo + ", HoraireDeDebuEmplo=" + HoraireDeDebuEmplo + ", DureePeriodeEssai="
				+ DureePeriodeEssai + ", TypeDePeriodeEssai=" + TypeDePeriodeEssai + ", DateFinPeriode="
				+ DateFinPeriode + ", ClassificaMetier=" + ClassificaMetier + ", ClassificaMetierSAP="
				+ ClassificaMetierSAP + ", NaturEmplo=" + NaturEmplo + ", TauxHorai=" + TauxHorai + ", SMIC=" + SMIC
				+ ", NePasTransm=" + NePasTransm + ", nbHeureHebdo=" + nbHeureHebdo + ", nbHeureMensu=" + nbHeureMensu
				+ ", nbJourTrav=" + nbJourTrav + ", Lundi=" + Lundi + ", Mardi=" + Mardi + ", Mercredi=" + Mercredi
				+ ", Jeudi=" + Jeudi + ", Vendredi=" + Vendredi + ", Samedi=" + Samedi + ", Dimanche=" + Dimanche
				+ ", ContratSign=" + ContratSign + ", enErreur=" + enErreur + "]";
	}

	
	

}
