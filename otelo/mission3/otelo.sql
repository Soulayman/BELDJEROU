-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 15 oct. 2020 à 15:09
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `otelo`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'standard'),
(2, 'confort'),
(3, 'premium'),
(4, 'luxe');

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

DROP TABLE IF EXISTS `chambre`;
CREATE TABLE IF NOT EXISTS `chambre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nbCouchage` int(11) NOT NULL,
  `porte` varchar(5) NOT NULL,
  `etage` varchar(5) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `baignoire` tinyint(1) NOT NULL,
  `prixBase` double NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`id`, `nbCouchage`, `porte`, `etage`, `idCategorie`, `baignoire`, `prixBase`, `updated_at`, `created_at`) VALUES
(1, 2, 'C', '10', 1, 0, 50, '2020-09-17 12:57:28', '2020-09-17 12:57:28'),
(4, 2, 'B', '10', 1, 0, 50, '2020-09-17 14:11:09', '2020-09-17 14:11:09'),
(5, 3, 'T', '5', 1, 2, 56, '2020-09-24 11:56:54', '2020-09-24 11:56:54'),
(6, 3, 'O', '10', 1, 1, 64, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(7, 4, 'N', '12', 1, 2, 165, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(8, 1, 'U', '15', 1, 2, 76, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(9, 1, 'R', '12', 1, 1, 60, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(10, 2, 'O', '3', 1, 1, 128, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(11, 3, 'B', '8', 1, 1, 136, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(12, 2, 'P', '14', 1, 2, 146, '2020-09-24 11:56:55', '2020-09-24 11:56:55'),
(13, 3, 'G', '10', 1, 2, 223, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(14, 3, 'S', '9', 1, 2, 128, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(15, 1, 'M', '3', 1, 2, 47, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(16, 4, 'K', '10', 1, 2, 92, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(17, 1, 'H', '7', 1, 2, 76, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(18, 2, 'H', '3', 1, 1, 170, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(19, 4, 'K', '4', 1, 2, 131, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(20, 2, 'I', '2', 1, 1, 67, '2020-09-24 11:56:56', '2020-09-24 11:56:56'),
(21, 3, 'J', '5', 1, 1, 107, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(22, 3, 'R', '15', 1, 1, 47, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(23, 1, 'F', '1', 1, 1, 114, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(24, 3, 'H', '11', 1, 1, 212, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(25, 3, 'O', '6', 1, 1, 103, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(26, 2, 'H', '11', 1, 1, 251, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(27, 2, 'O', '6', 1, 2, 75, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(28, 4, 'E', '12', 1, 1, 173, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(29, 3, 'B', '14', 1, 2, 73, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(30, 4, 'S', '11', 1, 1, 160, '2020-09-24 11:56:57', '2020-09-24 11:56:57'),
(31, 4, 'K', '10', 1, 2, 158, '2020-09-24 11:56:58', '2020-09-24 11:56:58'),
(32, 2, 'W', '7', 1, 2, 210, '2020-09-24 11:56:58', '2020-09-24 11:56:58'),
(33, 4, 'M', '6', 1, 2, 57, '2020-09-24 11:56:58', '2020-09-24 11:56:58'),
(34, 3, 'F', '2', 1, 2, 148, '2020-09-24 11:56:58', '2020-09-24 11:56:58'),
(35, 2, 'C', '10', 1, 0, 50, '2020-09-24 12:58:30', '2020-09-24 12:58:30'),
(36, 2, 'C', '10', 1, 0, 50, '2020-09-24 12:58:31', '2020-09-24 12:58:31'),
(37, 2, 'C', '10', 1, 0, 50, '2020-09-24 12:58:41', '2020-09-24 12:58:40'),
(38, 2, 'C', '10', 1, 0, 50, '2020-09-24 13:07:00', '2020-09-24 13:07:00'),
(39, 2, 'C', '10', 1, 0, 50, '2020-10-01 11:40:17', '2020-10-01 11:40:16'),
(40, 2, 'C', '10', 1, 0, 50, '2020-10-01 11:47:33', '2020-10-01 11:47:33'),
(41, 2, 'C', '10', 1, 0, 50, '2020-10-01 11:51:22', '2020-10-01 11:51:22'),
(42, 2, 'C', '10', 1, 0, 50, '2020-10-01 12:04:13', '2020-10-01 12:04:13'),
(43, 2, 'C', '10', 1, 0, 50, '2020-10-08 11:41:33', '2020-10-08 11:41:33'),
(44, 2, 'C', '10', 1, 0, 50, '2020-10-08 11:41:58', '2020-10-08 11:41:57');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_reservation`
--

DROP TABLE IF EXISTS `ligne_reservation`;
CREATE TABLE IF NOT EXISTS `ligne_reservation` (
  `idchambre` int(11) NOT NULL,
  `idreservation` int(11) NOT NULL,
  KEY `idchambre` (`idchambre`),
  KEY `idreservation` (`idreservation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2014_10_12_000000_create_users_table', 1),
(16, '2014_10_12_100000_create_password_resets_table', 1),
(17, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

DROP TABLE IF EXISTS `periode`;
CREATE TABLE IF NOT EXISTS `periode` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `periode`
--

INSERT INTO `periode` (`id`, `libelle`, `coefficient`) VALUES
(1, 'basse', 0.7),
(2, 'moyenne', 1),
(3, 'haute', 2);

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dateD` date NOT NULL,
  `dateF` date NOT NULL,
  `idPeriode` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
