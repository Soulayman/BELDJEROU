//Mission4 SI4
//BELDJEROU Soulayman
//23/11/2019
//Question 4
import java.util.Scanner;//importation du Scanner
public class Question4BONUS{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in); //ajout d'un Scanner
		System.out.println("combien de produit souhaitez vous achetez ? ");//affiche ce que doit faire l'utilisateur
		int a =sc.nextInt(); //laisse l'accès au clavier a l'utilisateur
		double resultat=0;
		int plus=0;
		int moins=0;
		double moyenne=0;
		
		String[] noms= new String[a]; //on initialise un tableau de chaine de caractère 
		double[] prixht=new double[a];//on initialise un tableau de réels
		
		for(int i=0; i<noms.length;i++){  //on utilise une boucle For 
			System.out.println(" Saisir nom du produit "+ (i+1));
			noms[i]=sc.next();
			sc.nextLine();//permet de ne pas faire quitter le programme si l'utilisateur se trompe en mettant un nombre
		
		
			System.out.println(" Saisir le prix du produit "+ noms[i]);
			prixht[i]=sc.nextDouble();
			
			
			if(i==0){//la valeur de départ est 0 
				 plus=i;
				 moins=i;
			}else{
				if(prixht[i]>prixht[plus]){//cela donnera le prixht du produit le plus haut
					plus=i;
				}
				if(prixht[i]<prixht[moins]){//cela donnera le prix du produit le plus bas 
					moins=i;
				}
			}
			resultat= prixht[i] + resultat;//on calcule le total des prix des produits 
		}
		for(int i=0; i<noms.length;i++){ 
			System.out.println(noms[i]+" coute "+ prixht[i]);
		}
		System.out.println("le total de tout vos produit vaut "+ resultat); 
		moyenne=resultat/noms.length;//cela servira a calculer la moyenne 
		System.out.println(" le prix les plus cher est " + prixht[plus] + " le prix le plus faible est " + prixht[moins]);//on affiche le prix le plus haut et le plus bas 
		System.out.println("la moyenne de vos produit est " + moyenne);//on affiche la moyenne 
	}	
}