package com.example.oless

import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_hotel_description.*

class HotelDescriptionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_description)
        setContentView(R.layout.activity_hotel_description)
        //val action = intent.action  //on ne s en sert pas ici
        //val isHotel = intent.hasCategory("Hotel") //on ne s en sert pas ici
        val name = intent.getStringExtra("Underground Hôtel")
        activity_hotel_description_textview.setText(name)
    }
}

class ConfirmReservationFragment: DialogFragment(){
    interface ConfirmDeleteListener{
        fun onDialogPositiveClick()
        fun onDialogNegativeClick()
    }
    var listener:ConfirmDeleteListener?=null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var builder = AlertDialog.Builder(activity)
        builder.setMessage("Confirmer la reservation")
            .setPositiveButton("oui", object: DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    Log.i(TAG,"confirm")
                    listener?.onDialogPositiveClick()
                }})
            .setNegativeButton("non",DialogInterface.OnClickListener{dialog,id ->
                Log.i(TAG,"annul")
                listener?.onDialogNegativeClick()
            })
        return builder.create()
    }
}

