package com.rdd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class Client {
	public static void main(String[]args) throws Exception{
		File file = new File("D:\\BTS_SIO\\Stage\\RDD2021\\Excel\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("CLIENT");
		
		int rowNum = 1;
		
		List<InfoClient> retour = new ArrayList<>();
		
		for (int i = 1; i<=sheet.getLastRowNum(); i++)
		{
			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoClient info = new InfoClient();
			
			info.setCodeClient(ligneCourante.getCell(0).getStringCellValue());
			info.setTypeClient(ligneCourante.getCell(1).getStringCellValue());
			info.setStatutClient(ligneCourante.getCell(2).getStringCellValue());
			info.setRaisonSo(ligneCourante.getCell(3).getStringCellValue());
			info.setLibelle(ligneCourante.getCell(4).getStringCellValue());
			info.setNSiteSiege(ligneCourante.getCell(5).getStringCellValue());
			info.setNSiret(ligneCourante.getCell(6).getStringCellValue());
			info.setTVA(ligneCourante.getCell(7).getStringCellValue());
			info.setCodeAPE(ligneCourante.getCell(8).getStringCellValue());
			info.setCategorie(ligneCourante.getCell(9).getStringCellValue());
			info.setSecteurGeo(ligneCourante.getCell(10).getStringCellValue());
			info.setDatePreContact(ligneCourante.getCell(11).getStringCellValue());
			info.setRDV(ligneCourante.getCell(12).getStringCellValue());
			info.setComptable(ligneCourante.getCell(13).getStringCellValue());
			info.setAuxiliare(ligneCourante.getCell(14).getStringCellValue());
			info.setClientRegu(ligneCourante.getCell(15).getStringCellValue());
			info.setExigence(ligneCourante.getCell(16).getStringCellValue());
			info.setComment(ligneCourante.getCell(17).getStringCellValue());
			info.setCivilite(ligneCourante.getCell(18).getStringCellValue());
			info.setNom(ligneCourante.getCell(19).getStringCellValue());
			info.setPrenom(ligneCourante.getCell(20).getStringCellValue(	));
			info.setNdjf(ligneCourante.getCell(21).getStringCellValue());
			info.setDateNaiss(ligneCourante.getCell(22).getStringCellValue());
			info.setLieuNaiss(ligneCourante.getCell(23).getStringCellValue());
			info.setSituationFam(ligneCourante.getCell(24).getStringCellValue());
			info.setNTelConj(ligneCourante.getCell(25).getStringCellValue());
			info.setEmailConj(ligneCourante.getCell(26).getStringCellValue());
			info.setClientInterv(ligneCourante.getCell(27).getStringCellValue());
			info.setClesFourn(ligneCourante.getCell(28).getStringCellValue());
			info.setNombreCles(ligneCourante.getCell(29).getStringCellValue());
			info.setCodeCles(ligneCourante.getCell(30).getStringCellValue());
			info.setDescCles(ligneCourante.getCell(31).getStringCellValue());
			info.setPresAnim(ligneCourante.getCell(32).getStringCellValue());
			info.setTypeAnim(ligneCourante.getCell(33).getStringCellValue());
			info.setNomAnim(ligneCourante.getCell(34).getStringCellValue());
			info.setAccesPossAnim(ligneCourante.getCell(35).getStringCellValue());
			info.setPresAlarm(ligneCourante.getCell(36).getStringCellValue());
			info.setEmplacement(ligneCourante.getCell(37).getStringCellValue());
			info.setModeActiv(ligneCourante.getCell(38).getStringCellValue());
			info.setModeDesact(ligneCourante.getCell(39).getStringCellValue());
			info.setBeneficiaire(ligneCourante.getCell(40).getStringCellValue());
			info.setNumCAF(ligneCourante.getCell(41).getStringCellValue());
			info.setNbEnfants(ligneCourante.getCell(42).getStringCellValue());
			info.setZoneSco(ligneCourante.getCell(43).getStringCellValue());
			info.setNMSA(ligneCourante.getCell(44).getStringCellValue());
			info.setAccesCles(ligneCourante.getCell(45).getStringCellValue());
			info.setItiniTransp(ligneCourante.getCell(46).getStringCellValue());
			info.setPays(ligneCourante.getCell(47).getStringCellValue());
			info.setAdresse(ligneCourante.getCell(48).getStringCellValue());
			info.setComplemAdres(ligneCourante.getCell(49).getStringCellValue());
			info.setCodePost(ligneCourante.getCell(50).getStringCellValue());
			info.setVille(ligneCourante.getCell(51).getStringCellValue());
			info.setNTelFix(ligneCourante.getCell(52).getStringCellValue());
			info.setNTelMob(ligneCourante.getCell(53).getStringCellValue());
			info.setNtelBur(ligneCourante.getCell(54).getStringCellValue());
			info.setNFax(ligneCourante.getCell(55).getStringCellValue());
			info.setEmail(ligneCourante.getCell(56).getStringCellValue());
			info.setAcceptMail(ligneCourante.getCell(57).getStringCellValue());
			info.setActif(ligneCourante.getCell(58).getStringCellValue());
			info.setDateArret(ligneCourante.getCell(59).getStringCellValue());
			
			
			
			
			
			retour.add(info);
			rowNum++;
		}
		
		for (InfoClient info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getCodeClient()+ " ; " +info.getTypeClient()+ " ; " +info.getStatutClient()
			+ " ; " +info.getRaisonSo()+ " ; " +info.getLibelle()+ " ; " +info.getNSiteSiege()+ " ; " +info.getNSiret()
			+ " ; " +info.getTVA()+ " ; " +info.getCodeAPE()+ " ; " +info.getCategorie()+ " ; " +info.getSecteurGeo()+ " ; " +info.getDatePreContact()
			+ " ; " +info.getRDV()+ " ; " +info.getComptable()+ " ; " +info.getAuxiliare()+ " ; " +info.getClientRegu()
			+ " ; " +info.getExigence()+ " ; " +info.getComment()+ " ; " +info.getCivilite()+ " ; " +info.getNom()+ " ; " +info.getPrenom()
			+ " ; " +info.getNdjf()+ " ; " +info.getDateNaiss()+ " ; " +info.getLieuNaiss()+ " ; " +info.getSituationFam()
			+ " ; " +info.getNTelConj()+ " ; " +info.getEmailConj()+ " ; " +info.getClientInterv()+ " ; " +info.getClesFourn()
			+ " ; " +info.getNombreCles()+ " ; " +info.getCodeCles()+ " ; " +info.getDescCles()+ " ; " +info.getPresAnim()
			+ " ; " +info.getTypeAnim()+ " ; " +info.getNomAnim()+ " ; " +info.getAccesPossAnim()+ " ; " +info.getPresAlarm()
			+ " ; " +info.getEmplacement()+ " ; " +info.getModeActiv()+ " ; " +info.getModeDesact()+ " ; " +info.getBeneficiaire()
			+ " ; " +info.getNumCAF()+ " ; " +info.getNbEnfants()+ " ; " +info.getZoneSco()+ " ; " +info.getNMSA()
			+ " ; " +info.getAccesCles()+ " ; " +info.getItiniTransp()+ " ; " +info.getPays()+ " ; " +info.getAdresse()
			+ " ; " +info.getComplemAdres()+ " ; " +info.getCodePost()+ " ; " +info.getVille()+ " ; " +info.getNTelFix()
			+ " ; " +info.getNTelMob()+ " ; " +info.getNtelBur()+ " ; " +info.getNFax()
			+ " ; " +info.getEmail()+ " ; " +info.getAcceptMail()+ " ; " +info.getActif()
			+ " ; " +info.getDateArret() + (info.isEnErreur() ? info.getMessagesErreur(): "OK"));
		}
	}
}
