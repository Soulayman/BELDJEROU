package com.rdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoSite {
	
	private boolean enErreur;
	private List<String> messagesErreur;
	public String[] Civ = {"Monsieur","Madame"};
	private String[] Num = {"0","1"};
	private String ch = "Le champs ";
	private String mp = " est incomplet";
	private String bo = " est incomplet ou invalide";
	private String CodeCli;
	private String CodeSite;
	private String LibelSite;
	private String MatriResp;
	private String MatriInsp;
	private String MatriComm;
	private String MatriChef;
	private String CategoSite;
	private String SectGeo;
	private String Facturable; //boolean
	private String interdiPlan;
	private String Comment;
	private String Pole;
	private String Activite;
	private String Section;
	private String Regroup1;
	private String Regroup2;
	private String Regroup3;
	private String DirectReg;
	private String DirectOpe;
	private String Tel;
	private String AccesClef;
	private String ItiniTransp;
	private String Pays;
	private String Adresse;
	private String ComplemAdre;
	private String CodePost;
	private String Ville;
	private String TelFix;
	private String TelMob;
	private String TelFax;
	private String Email;
	private String AcceptMail;
	private String CodeAlarm;
	private String SurfaceTota;
	private String SurfaceVitre;
	private String PointEau;
	private String Electr;
	private String LocalMen;
	private String CompteCompta;
	private String CompteAnal;
	private String ModeReg;
	private String EcheFact;
	private String ValeurX;
	private String GroupeFact; 
	private String Actif; //Boolean
	private String DateArret;
	private String publicfrag;
	private String GIR;
	private String NRemplace;
	private String MesuProtec;
	private String NSecu;
	private String Mutuelle;
	private String CaisRetrai;
	private String TypeLog;
	private String Access;
	private String EtatGener;
	private String HabVie;
	private String ObjeAtt;
	private String Nature;
	private String ConsigneCli; 
	private String ConsigneInt;
	private String Materiel;
	
	
	
	public boolean isEnErreur() {
		return enErreur;
	}

	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}

	public List<String> getMessagesErreur() {
		return messagesErreur;
	}

	public void setMessagesErreur(List<String> messagesErreur) {
		this.messagesErreur = messagesErreur;
	}

	public String[] getCiv() {
		return Civ;
	}

	public void setCiv(String[] civ) {
		Civ = civ;
	}

	public String[] getNum() {
		return Num;
	}

	public void setNum(String[] num) {
		Num = num;
	}

	public String getCh() {
		return ch;
	}

	public void setCh(String ch) {
		this.ch = ch;
	}

	public String getMp() {
		return mp;
	}

	public void setMp(String mp) {
		this.mp = mp;
	}

	public String getBo() {
		return bo;
	}

	public void setBo(String bo) {
		this.bo = bo;
	}

	public String getCodeCli() {
		return CodeCli;
	}

	public void setCodeCli(String codeCli) {
		CodeCli = codeCli;
	}

	public String getCodeSite() {
		return CodeSite;
	}

	public void setCodeSite(String codeSite) {
		CodeSite = codeSite;
	}

	public String getLibelSite() {
		return LibelSite;
	}

	public void setLibelSite(String libelSite) {
		LibelSite = libelSite;
	}

	public String getMatriResp() {
		return MatriResp;
	}

	public void setMatriResp(String matriResp) {
		MatriResp = matriResp;
	}

	public String getMatriInsp() {
		return MatriInsp;
	}

	public void setMatriInsp(String matriInsp) {
		MatriInsp = matriInsp;
	}

	public String getMatriComm() {
		return MatriComm;
	}

	public void setMatriComm(String matriComm) {
		MatriComm = matriComm;
	}

	public String getMatriChef() {
		return MatriChef;
	}

	public void setMatriChef(String matriChef) {
		MatriChef = matriChef;
	}

	public String getCategoSite() {
		return CategoSite;
	}

	public void setCategoSite(String categoSite) {
		CategoSite = categoSite;
	}

	public String getSectGeo() {
		return SectGeo;
	}

	public void setSectGeo(String sectGeo) {
		SectGeo = sectGeo;
	}

	public String getFacturable() {
		return Facturable;
	}

	public void setFacturable(String facturable) {
		Facturable = facturable;
	}

	public String getInterdiPlan() {
		return interdiPlan;
	}

	public void setInterdiPlan(String interdiPlan) {
		this.interdiPlan = interdiPlan;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getPole() {
		return Pole;
	}

	public void setPole(String pole) {
		Pole = pole;
	}

	public String getActivite() {
		return Activite;
	}

	public void setActivite(String activite) {
		Activite = activite;
	}

	public String getSection() {
		return Section;
	}

	public void setSection(String section) {
		Section = section;
	}

	public String getRegroup1() {
		return Regroup1;
	}

	public void setRegroup1(String regroup1) {
		Regroup1 = regroup1;
	}

	public String getRegroup2() {
		return Regroup2;
	}

	public void setRegroup2(String regroup2) {
		Regroup2 = regroup2;
	}

	public String getRegroup3() {
		return Regroup3;
	}

	public void setRegroup3(String regroup3) {
		Regroup3 = regroup3;
	}

	public String getDirectReg() {
		return DirectReg;
	}

	public void setDirectReg(String directReg) {
		DirectReg = directReg;
	}

	public String getDirectOpe() {
		return DirectOpe;
	}

	public void setDirectOpe(String directOpe) {
		DirectOpe = directOpe;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getAccesClef() {
		return AccesClef;
	}

	public void setAccesClef(String accesClef) {
		AccesClef = accesClef;
	}

	public String getItiniTransp() {
		return ItiniTransp;
	}

	public void setItiniTransp(String itiniTransp) {
		ItiniTransp = itiniTransp;
	}

	public String getPays() {
		return Pays;
	}

	public void setPays(String pays) {
		Pays = pays;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public String getComplemAdre() {
		return ComplemAdre;
	}

	public void setComplemAdre(String complemAdre) {
		ComplemAdre = complemAdre;
	}

	public String getCodePost() {
		return CodePost;
	}

	public void setCodePost(String codePost) {
		CodePost = codePost;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getTelFix() {
		return TelFix;
	}

	public void setTelFix(String telFix) {
		TelFix = telFix;
	}

	public String getTelMob() {
		return TelMob;
	}

	public void setTelMob(String telMob) {
		TelMob = telMob;
	}

	public String getTelFax() {
		return TelFax;
	}

	public void setTelFax(String telFax) {
		TelFax = telFax;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getAcceptMail() {
		return AcceptMail;
	}

	public void setAcceptMail(String acceptMail) {
		AcceptMail = acceptMail;
	}

	public String getCodeAlarm() {
		return CodeAlarm;
	}

	public void setCodeAlarm(String codeAlarm) {
		CodeAlarm = codeAlarm;
	}

	public String getSurfaceTota() {
		return SurfaceTota;
	}

	public void setSurfaceTota(String surfaceTota) {
		SurfaceTota = surfaceTota;
	}

	public String getSurfaceVitre() {
		return SurfaceVitre;
	}

	public void setSurfaceVitre(String surfaceVitre) {
		SurfaceVitre = surfaceVitre;
	}

	public String getPointEau() {
		return PointEau;
	}

	public void setPointEau(String pointEau) {
		PointEau = pointEau;
	}

	public String getElectr() {
		return Electr;
	}

	public void setElectr(String electr) {
		Electr = electr;
	}

	public String getLocalMen() {
		return LocalMen;
	}

	public void setLocalMen(String localMen) {
		LocalMen = localMen;
	}

	public String getCompteCompta() {
		return CompteCompta;
	}

	public void setCompteCompta(String compteCompta) {
		CompteCompta = compteCompta;
	}

	public String getCompteAnal() {
		return CompteAnal;
	}

	public void setCompteAnal(String compteAnal) {
		CompteAnal = compteAnal;
	}

	public String getModeReg() {
		return ModeReg;
	}

	public void setModeReg(String modeReg) {
		ModeReg = modeReg;
	}

	public String getEcheFact() {
		return EcheFact;
	}

	public void setEcheFact(String echeFact) {
		EcheFact = echeFact;
	}

	public String getValeurX() {
		return ValeurX;
	}

	public void setValeurX(String valeurX) {
		ValeurX = valeurX;
	}

	public String getGroupeFact() {
		return GroupeFact;
	}

	public void setGroupeFact(String groupeFact) {
		GroupeFact = groupeFact;
	}

	public String getActif() {
		return Actif;
	}

	public void setActif(String actif) {
		Actif = actif;
	}

	public String getDateArret() {
		return DateArret;
	}

	public void setDateArret(String dateArret) {
		DateArret = dateArret;
	}

	public String getPublicfrag() {
		return publicfrag;
	}

	public void setPublicfrag(String publicfrag) {
		this.publicfrag = publicfrag;
	}

	public String getGIR() {
		return GIR;
	}

	public void setGIR(String gIR) {
		GIR = gIR;
	}

	public String getNRemplace() {
		return NRemplace;
	}

	public void setNRemplace(String nRemplace) {
		NRemplace = nRemplace;
	}

	public String getMesuProtec() {
		return MesuProtec;
	}

	public void setMesuProtec(String mesuProtec) {
		MesuProtec = mesuProtec;
	}

	public String getNSecu() {
		return NSecu;
	}

	public void setNSecu(String nSecu) {
		NSecu = nSecu;
	}

	public String getMutuelle() {
		return Mutuelle;
	}

	public void setMutuelle(String mutuelle) {
		Mutuelle = mutuelle;
	}

	public String getCaisRetrai() {
		return CaisRetrai;
	}

	public void setCaisRetrai(String caisRetrai) {
		CaisRetrai = caisRetrai;
	}

	public String getTypeLog() {
		return TypeLog;
	}

	public void setTypeLog(String typeLog) {
		TypeLog = typeLog;
	}

	public String getAccess() {
		return Access;
	}

	public void setAccess(String access) {
		Access = access;
	}

	public String getEtatGener() {
		return EtatGener;
	}

	public void setEtatGener(String etatGener) {
		EtatGener = etatGener;
	}

	public String getHabVie() {
		return HabVie;
	}

	public void setHabVie(String habVie) {
		HabVie = habVie;
	}

	public String getObjeAtt() {
		return ObjeAtt;
	}

	public void setObjeAtt(String objeAtt) {
		ObjeAtt = objeAtt;
	}

	public String getNature() {
		return Nature;
	}

	public void setNature(String nature) {
		Nature = nature;
	}

	public String getConsigneCli() {
		return ConsigneCli;
	}

	public void setConsigneCli(String consigneCli) {
		ConsigneCli = consigneCli;
	}

	public String getConsigneInt() {
		return ConsigneInt;
	}

	public void setConsigneInt(String consigneInt) {
		ConsigneInt = consigneInt;
	}

	public String getMateriel() {
		return Materiel;
	}

	public void setMateriel(String materiel) {
		Materiel = materiel;
	}

	public InfoSite(){
		this.messagesErreur = new ArrayList<>();
	}
	
	public boolean verifierChampsTexte(String champsAVerifier)
	{
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	public boolean VerifierBool(String noo)
	{
		if(noo == null || noo == "" || !(noo.equalsIgnoreCase(Num[0]) || noo.equalsIgnoreCase(Num[1])))
			return false;
		return true;
	}
	
	public void verifierCoherence() {
		if(!verifierChampsTexte(CodeCli)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Client"+mp);
		}
		
		if(!verifierChampsTexte(CodeSite)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Site"+mp);
		}
		
		if(!verifierChampsTexte(LibelSite)) {
			enErreur = true;
			messagesErreur.add(ch+"Libell� de Site"+mp);
		}
		
		if(!verifierChampsTexte(MatriResp)) {
			enErreur = true;
			messagesErreur.add(ch+"Matricule du Responsable (directeur d'exploitation)"+mp);
		}
		
		if(!verifierChampsTexte(MatriInsp)) {
			enErreur = true;
			messagesErreur.add(ch+"Matricule de l'inspecteur (charg� d'affaires)"+mp);
		}
		
		if(!verifierChampsTexte(MatriComm)) {
			enErreur = true;
			messagesErreur.add(ch+"Matricule du commercial"+mp);
		}
		
		if(!verifierChampsTexte(MatriChef)) {
			enErreur = true;
			messagesErreur.add(ch+"Matricule du chef d'�quipe"+mp);
		}
		
		if(!verifierChampsTexte(CategoSite)) {
			enErreur = true;
			messagesErreur.add(ch+"Cat�gorie site"+mp);
		}
		
		if(!verifierChampsTexte(SectGeo)) {
			enErreur = true;
			messagesErreur.add(ch+"Secteur G�ographique du site"+mp);
		}
		
		if(!verifierChampsTexte(Facturable)) {
			enErreur = true;
			messagesErreur.add(ch+"Facturable"+mp);
		}
		
		if(!verifierChampsTexte(interdiPlan)) {
			enErreur = true;
			messagesErreur.add(ch+"interdire la planification les jours f�ri�s "+mp);
		}
		
		if(!verifierChampsTexte(Comment)) {
			enErreur = true;
			messagesErreur.add(ch+"Commentaire"+mp);
		}
		
		if(!verifierChampsTexte(Pole)) {
			enErreur = true;
			messagesErreur.add(ch+"P�le"+mp);
		}
		
		if(!verifierChampsTexte(Activite)) {
			enErreur = true;
			messagesErreur.add(ch+"Activit�"+mp);
		}
		
		if(!verifierChampsTexte(Section)) {
			enErreur = true;
			messagesErreur.add(ch+"Section"+mp);
		}
		
		if(!verifierChampsTexte(Regroup1)) {
			enErreur = true;
			messagesErreur.add(ch+"1er Regroupement "+mp);
		}
		
		if(!verifierChampsTexte(Regroup2)) {
			enErreur = true;
			messagesErreur.add(ch+"2nd Regroupement"+mp);
		}
		
		if(!verifierChampsTexte(Regroup3)) {
			enErreur = true;
			messagesErreur.add(ch+"3�me regroupement"+mp);
		}
		
		if(!verifierChampsTexte(DirectReg)) {
			enErreur = true;
			messagesErreur.add(ch+"Directeur r�gional"+mp);
		}
		
		if(!verifierChampsTexte(DirectOpe)) {
			enErreur = true;
			messagesErreur.add(ch+"Directeur op�rationnel"+mp);
		}
		
		if(!verifierChampsTexte(Tel)) {
			enErreur = true;
			messagesErreur.add(ch+"T�l�phone"+mp);
		}
		
		if(!verifierChampsTexte(AccesClef)) {
			enErreur = true;
			messagesErreur.add(ch+"Acc�s Clefs"+mp);
		}
		
		if(!verifierChampsTexte(ItiniTransp)) {
			enErreur = true;
			messagesErreur.add(ch+"Itin�raire Transport"+mp);
		}
		
		if(!verifierChampsTexte(Pays)) {
			enErreur = true;
			messagesErreur.add(ch+"Pays"+mp);
		}
		
		if(!verifierChampsTexte(Adresse)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse"+mp);
		}
		
		if(!verifierChampsTexte(ComplemAdre)) {
			enErreur = true;
			messagesErreur.add(ch+"Compl�mentaire d'adresse"+mp);
		}
		
		if(!verifierChampsTexte(CodePost)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Postal"+mp);
		}
		
		if(!verifierChampsTexte(Ville)) {
			enErreur = true;
			messagesErreur.add(ch+"Ville"+mp);
		}
		
		if(!verifierChampsTexte(TelFix)) {
			enErreur = true;
			messagesErreur.add(ch+"N� de t�l�phone fixe"+mp);
		}
		
		if(!verifierChampsTexte(TelMob)) {
			enErreur = true;
			messagesErreur.add(ch+"N� de t�l�phone mobile"+mp);
		}
		
		if(!verifierChampsTexte(TelFax)) {
			enErreur = true;
			messagesErreur.add(ch+"telephoneFax"+mp);
		}
		
		if(!verifierChampsTexte(Email)) {
			enErreur = true;
			messagesErreur.add(ch+"Email"+mp);
		}
		
		if(!verifierChampsTexte(AcceptMail)) {
			enErreur = true;
			messagesErreur.add(ch+"Accepte email"+mp);
		}
		
		if(!verifierChampsTexte(CodeAlarm)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Alarme"+mp);
		}
		
		if(!verifierChampsTexte(SurfaceTota)) {
			enErreur = true;
			messagesErreur.add(ch+"Surface totale"+mp);
		}
		
		if(!verifierChampsTexte(SurfaceVitre)) {
			enErreur = true;
			messagesErreur.add(ch+"surface vitres"+mp);
		}
		
		if(!verifierChampsTexte(PointEau)) {
			enErreur = true;
			messagesErreur.add(ch+"Point d'eau"+mp);
		}
		
		if(!verifierChampsTexte(Electr)) {
			enErreur = true;
			messagesErreur.add(ch+"Electricit�"+mp);
		}
		
		if(!verifierChampsTexte(LocalMen)) {
			enErreur = true;
			messagesErreur.add(ch+"Local M�nage"+mp);
		}
		
		if(!verifierChampsTexte(CompteCompta)) {
			enErreur = true;
			messagesErreur.add(ch+"Compte comptable"+mp);
		}
		
		if(!verifierChampsTexte(CompteAnal)) {
			enErreur = true;
			messagesErreur.add(ch+"compte analytique"+mp);
		}
		
		if(!verifierChampsTexte(ModeReg)) {
			enErreur = true;
			messagesErreur.add(ch+"Mode de r�glement"+mp);
		}
		
		if(!verifierChampsTexte(EcheFact)) {
			enErreur = true;
			messagesErreur.add(ch+"�ch�ance de facture"+mp);
		}
		
		if(!verifierChampsTexte(ValeurX)) {
			enErreur = true;
			messagesErreur.add(ch+"Valeur du X"+mp);
		}
		
		if(!verifierChampsTexte(GroupeFact)) {
			enErreur = true;
			messagesErreur.add(ch+"groupe de facturation"+mp);
		}
		
		if(!verifierChampsTexte(Actif)) {
			enErreur = true;
			messagesErreur.add(ch+"Actif"+mp);
		}
		
		if(!verifierChampsTexte(DateArret)) {
			enErreur = true;
			messagesErreur.add(ch+"Date d'arr�t"+mp);
		}
		
		if(!verifierChampsTexte(publicfrag)) {
			enErreur = true;
			messagesErreur.add(ch+"Public fragile"+mp);
		}
		
		if(!verifierChampsTexte(GIR)) {
			enErreur = true;
			messagesErreur.add(ch+"GIR"+mp);
		}
		
		if(!verifierChampsTexte(NRemplace)) {
			enErreur = true;
			messagesErreur.add(ch+"Niveau de remplacement"+mp);
		}
		
		if(!verifierChampsTexte(MesuProtec)) {
			enErreur = true;
			messagesErreur.add(ch+"Mesure de protection"+mp);
		}
		
		if(!verifierChampsTexte(NSecu)) {
			enErreur = true;
			messagesErreur.add(ch+"N� Secu"+mp);
		}
		
		if(!verifierChampsTexte(Mutuelle)) {
			enErreur = true;
			messagesErreur.add(ch+"Mutuelle"+mp);
		}
		
		if(!verifierChampsTexte(CaisRetrai)) {
			enErreur = true;
			messagesErreur.add(ch+"Caisse de retraite"+mp);
		}
		
		if(!verifierChampsTexte(TypeLog)) {
			enErreur = true;
			messagesErreur.add(ch+"Type du logement"+mp);
		}
		
		if(!verifierChampsTexte(Access)) {
			enErreur = true;
			messagesErreur.add(ch+"Accessibilit�"+mp);
		}
		
		if(!verifierChampsTexte(EtatGener)) {
			enErreur = true;
			messagesErreur.add(ch+"Etat g�n�ral du logement"+mp);
		}
		
		if(!verifierChampsTexte(HabVie)) {
			enErreur = true;
			messagesErreur.add(ch+"Habitude de vie"+mp);
		}
		
		if(!verifierChampsTexte(ObjeAtt)) {
			enErreur = true;
			messagesErreur.add(ch+"Objectif � atteindre"+mp);
		}
		
		if(!verifierChampsTexte(Nature)) {
			enErreur = true;
			messagesErreur.add(ch+"Nature des t�ches et indications"+mp);
		}
		
		if(!verifierChampsTexte(ConsigneCli)) {
			enErreur = true;
			messagesErreur.add(ch+"Consignes clients"+mp);
		}
		
		if(!verifierChampsTexte(ConsigneInt)) {
			enErreur = true;
			messagesErreur.add(ch+"Consignes intervenants"+mp);
		}
		
		if(!verifierChampsTexte(Materiel)) {
			enErreur = true;
			messagesErreur.add(ch+"Mat�riel � disposition"+mp);
		}
		
		
	}

	@Override
	public String toString() {
		return "InfoSite [enErreur=" + enErreur + ", messagesErreur=" + messagesErreur + ", Civ=" + Arrays.toString(Civ)
				+ ", Num=" + Arrays.toString(Num) + ", ch=" + ch + ", mp=" + mp + ", bo=" + bo + ", CodeCli=" + CodeCli
				+ ", CodeSite=" + CodeSite + ", LibelSite=" + LibelSite + ", MatriResp=" + MatriResp + ", MatriInsp="
				+ MatriInsp + ", MatriComm=" + MatriComm + ", MatriChef=" + MatriChef + ", CategoSite=" + CategoSite
				+ ", SectGeo=" + SectGeo + ", Facturable=" + Facturable + ", interdiPlan=" + interdiPlan + ", Comment="
				+ Comment + ", Pole=" + Pole + ", Activite=" + Activite + ", Section=" + Section + ", Regroup1="
				+ Regroup1 + ", Regroup2=" + Regroup2 + ", Regroup3=" + Regroup3 + ", DirectReg=" + DirectReg
				+ ", DirectOpe=" + DirectOpe + ", Tel=" + Tel + ", AccesClef=" + AccesClef + ", ItiniTransp="
				+ ItiniTransp + ", Pays=" + Pays + ", Adresse=" + Adresse + ", ComplemAdre=" + ComplemAdre
				+ ", CodePost=" + CodePost + ", Ville=" + Ville + ", TelFix=" + TelFix + ", TelMob=" + TelMob
				+ ", TelFax=" + TelFax + ", Email=" + Email + ", AcceptMail=" + AcceptMail + ", CodeAlarm=" + CodeAlarm
				+ ", SurfaceTota=" + SurfaceTota + ", SurfaceVitre=" + SurfaceVitre + ", PointEau=" + PointEau
				+ ", Electr=" + Electr + ", LocalMen=" + LocalMen + ", CompteCompta=" + CompteCompta + ", CompteAnal="
				+ CompteAnal + ", ModeReg=" + ModeReg + ", EcheFact=" + EcheFact + ", ValeurX=" + ValeurX
				+ ", GroupeFact=" + GroupeFact + ", Actif=" + Actif + ", DateArret=" + DateArret + ", publicfrag="
				+ publicfrag + ", GIR=" + GIR + ", NRemplace=" + NRemplace + ", MesuProtec=" + MesuProtec + ", NSecu="
				+ NSecu + ", Mutuelle=" + Mutuelle + ", CaisRetrai=" + CaisRetrai + ", TypeLog=" + TypeLog + ", Access="
				+ Access + ", EtatGener=" + EtatGener + ", HabVie=" + HabVie + ", ObjeAtt=" + ObjeAtt + ", Nature="
				+ Nature + ", ConsigneCli=" + ConsigneCli + ", ConsigneInt=" + ConsigneInt + ", Materiel=" + Materiel
				+ "]";
	}
	
	
}
