<?php


namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Reservation;


class ReservationController extends Controller{
    public function create(){
        return view('createReservation');
    }

    public function __construct(){
        $this->middleware('auth');
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'dateD' => 'required|date|after:tomorrow',
            'dateF' => 'required|date|after:dateD',
            'idPeriode'=> 'required|between:1,3'
      ]);

        $reservation=new Reservation();
           
            $dateD=$request->input('dateD');
            $dateF=$request->input('dateF');
            $idPeriode=$request->input('idPeriode');
            $reservation->dateD=$dateD;
            $reservation->dateF=$dateF;
            $reservation->idPeriode=$idPeriode;
            $reservation->save();
            return redirect()->back();
            $reservation= Reservation::create([
                'dateD' =>$dateD,
                'dateF' => $dateF,
                'idPeriode' => $idPeriode
 
            ]);

    }
}
?>