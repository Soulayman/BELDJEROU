package com.rdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoContratCli {
	private boolean enErreur;
	private List<String> messagesErreur;
	public String[] Civ = {"Monsieur","Madame"};
	private String[] Num = {"0","1"};
	private String ch = "Le champs ";
	private String mp = " est incomplet";
	private String bo = " est incomplet ou invalide";
	private String CodeClient;
	private String CodeSite;
	private String NContrat;
	private String DateDebut;
	private String DateFin;
	private String TaciteRecon; //boolean
	private String DatePrea;
	private String MotifContrat;
	private String CategoContrat;
	private String RefContrat;
	private String NDevis;
	private String CompteAnaly;
	private String Comment;
	private String Materiel;
	private String Produits;
	private String FacturerPassa;
	private String Contratprinci;
	private String ModeRegl;
	private String FacturerSeul;
	private String FacturerAvec;
	private String FacturerSiege;
	private String Echeance;
	private String ValeurX;
	private String Reference;
	private String DureePrea;
	private String AdresseFact;
	private String AdresseDiff;
	private String AdresseDiff2;
	private String AdresseDiff3;
	private String AdresseDiff4;
	private String AdresseDiff5;
	private String AdresseDiff6;
	private String AdresseDiff7;
	private String NMarche;
	private String NEngagement;
	
	
	
	
	public boolean isEnErreur() {
		return enErreur;
	}

	public void setEnErreur(boolean enErreur) {
		this.enErreur = enErreur;
	}

	public List<String> getMessagesErreur() {
		return messagesErreur;
	}

	public void setMessagesErreur(List<String> messagesErreur) {
		this.messagesErreur = messagesErreur;
	}

	public String getCodeClient() {
		return CodeClient;
	}

	public void setCodeClient(String codeClient) {
		CodeClient = codeClient;
	}

	public String getCodeSite() {
		return CodeSite;
	}

	public void setCodeSite(String codeSite) {
		CodeSite = codeSite;
	}

	public String getNContrat() {
		return NContrat;
	}

	public void setNContrat(String nContrat) {
		NContrat = nContrat;
	}

	public String getDateDebut() {
		return DateDebut;
	}

	public void setDateDebut(String dateDebut) {
		DateDebut = dateDebut;
	}

	public String getDateFin() {
		return DateFin;
	}

	public void setDateFin(String dateFin) {
		DateFin = dateFin;
	}

	public String getTaciteRecon() {
		return TaciteRecon;
	}

	public void setTaciteRecon(String taciteRecon) {
		TaciteRecon = taciteRecon;
	}

	public String getDatePrea() {
		return DatePrea;
	}

	public void setDatePrea(String datePrea) {
		DatePrea = datePrea;
	}

	public String getMotifContrat() {
		return MotifContrat;
	}

	public void setMotifContrat(String motifContrat) {
		MotifContrat = motifContrat;
	}

	public String getCategoContrat() {
		return CategoContrat;
	}

	public void setCategoContrat(String categoContrat) {
		CategoContrat = categoContrat;
	}

	public String getRefContrat() {
		return RefContrat;
	}

	public void setRefContrat(String refContrat) {
		RefContrat = refContrat;
	}

	public String getNDevis() {
		return NDevis;
	}

	public void setNDevis(String nDevis) {
		NDevis = nDevis;
	}

	public String getCompteAnaly() {
		return CompteAnaly;
	}

	public void setCompteAnaly(String compteAnaly) {
		CompteAnaly = compteAnaly;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getMateriel() {
		return Materiel;
	}

	public void setMateriel(String materiel) {
		Materiel = materiel;
	}

	public String getProduits() {
		return Produits;
	}

	public void setProduits(String produits) {
		Produits = produits;
	}

	public String getFacturerPassa() {
		return FacturerPassa;
	}

	public void setFacturerPassa(String facturerPassa) {
		FacturerPassa = facturerPassa;
	}

	public String getContratprinci() {
		return Contratprinci;
	}

	public void setContratprinci(String contratprinci) {
		Contratprinci = contratprinci;
	}

	public String getModeRegl() {
		return ModeRegl;
	}

	public void setModeRegl(String modeRegl) {
		ModeRegl = modeRegl;
	}

	public String getFacturerSeul() {
		return FacturerSeul;
	}

	public void setFacturerSeul(String facturerSeul) {
		FacturerSeul = facturerSeul;
	}

	public String getFacturerAvec() {
		return FacturerAvec;
	}

	public void setFacturerAvec(String facturerAvec) {
		FacturerAvec = facturerAvec;
	}

	public String getFacturerSiege() {
		return FacturerSiege;
	}

	public void setFacturerSiege(String facturerSiege) {
		FacturerSiege = facturerSiege;
	}

	public String getEcheance() {
		return Echeance;
	}

	public void setEcheance(String echeance) {
		Echeance = echeance;
	}

	public String getValeurX() {
		return ValeurX;
	}

	public void setValeurX(String valeurX) {
		ValeurX = valeurX;
	}

	public String getReference() {
		return Reference;
	}

	public void setReference(String reference) {
		Reference = reference;
	}

	public String getDureePrea() {
		return DureePrea;
	}

	public void setDureePrea(String dureePrea) {
		DureePrea = dureePrea;
	}

	public String getAdresseFact() {
		return AdresseFact;
	}

	public void setAdresseFact(String adresseFact) {
		AdresseFact = adresseFact;
	}

	public String getAdresseDiff() {
		return AdresseDiff;
	}

	public void setAdresseDiff(String adresseDiff) {
		AdresseDiff = adresseDiff;
	}

	public String getAdresseDiff2() {
		return AdresseDiff2;
	}

	public void setAdresseDiff2(String adresseDiff2) {
		AdresseDiff2 = adresseDiff2;
	}

	public String getAdresseDiff3() {
		return AdresseDiff3;
	}

	public void setAdresseDiff3(String adresseDiff3) {
		AdresseDiff3 = adresseDiff3;
	}

	public String getAdresseDiff4() {
		return AdresseDiff4;
	}

	public void setAdresseDiff4(String adresseDiff4) {
		AdresseDiff4 = adresseDiff4;
	}

	public String getAdresseDiff5() {
		return AdresseDiff5;
	}

	public void setAdresseDiff5(String adresseDiff5) {
		AdresseDiff5 = adresseDiff5;
	}

	public String getAdresseDiff6() {
		return AdresseDiff6;
	}

	public void setAdresseDiff6(String adresseDiff6) {
		AdresseDiff6 = adresseDiff6;
	}
	
	
	public String getAdresseDiff7() {
		return AdresseDiff7;
	}

	public void setAdresseDiff7(String adresseDiff7) {
		AdresseDiff7 = adresseDiff7;
	}

	public String getNMarche() {
		return NMarche;
	}

	public void setNMarche(String nMarche) {
		NMarche = nMarche;
	}

	public String getNEngagement() {
		return NEngagement;
	}

	public void setNEngagement(String nEngagement) {
		NEngagement = nEngagement;
	}

	public InfoContratCli(){
		this.messagesErreur = new ArrayList<>();
	}
	
	public boolean verifierChampsTexte(String champsAVerifier)
	{ 
		if (champsAVerifier == null || champsAVerifier == "") 
			return false;
		
		return true;
	}
	
	public boolean VerifierBool(String noo)
	{
		if(noo == null || noo == "" || !(noo.equalsIgnoreCase(Num[0]) || noo.equalsIgnoreCase(Num[1])))
			return false;
		return true;
	}
	
	public void verifierCoherence() {
		
		if(!verifierChampsTexte(CodeClient)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Client"+mp);
		}
		
		if(!verifierChampsTexte(CodeSite)) {
			enErreur = true;
			messagesErreur.add(ch+"Code Site"+mp);
		}
		
		if(!verifierChampsTexte(NContrat)) {
			enErreur = true;
			messagesErreur.add(ch+"N� de contrat"+mp);
		}
		
		if(!verifierChampsTexte(DateDebut)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de d�but"+mp);
		}
		
		if(!verifierChampsTexte(DateFin)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de fin"+mp);
		}
		
		if(!verifierChampsTexte(TaciteRecon)) {
			enErreur = true;
			messagesErreur.add(ch+"Tacite reconduction"+mp);
		}
		
		if(!verifierChampsTexte(DatePrea)) {
			enErreur = true;
			messagesErreur.add(ch+"Date de pr�avis"+mp);
		}
		
		if(!verifierChampsTexte(MotifContrat)) {
			enErreur = true;
			messagesErreur.add(ch+"Motif de fin de contrat"+mp);
		}
		
		if(!verifierChampsTexte(CategoContrat)) {
			enErreur = true;
			messagesErreur.add(ch+"Cat�gorie de contrat"+mp);
		}
		
		if(!verifierChampsTexte(RefContrat)) {
			enErreur = true;
			messagesErreur.add(ch+"R�f�rence Contrat"+mp);
		}
		
		if(!verifierChampsTexte(NDevis)) {
			enErreur = true;
			messagesErreur.add(ch+"N� devis d'origine"+mp);
		}
		
		if(!verifierChampsTexte(CompteAnaly)) {
			enErreur = true;
			messagesErreur.add(ch+"Compte analytique"+mp);
		}
		
		if(!verifierChampsTexte(Comment)) {
			enErreur = true;
			messagesErreur.add(ch+"Commentaire"+mp);
		}	
		
		if(!verifierChampsTexte(Materiel)) {
			enErreur = true;
			messagesErreur.add(ch+"Mat�riel"+mp);
		}
		
		if(!verifierChampsTexte(Produits)) {
			enErreur = true;
			messagesErreur.add(ch+"Produits"+mp);
		}
		
		if(!verifierChampsTexte(FacturerPassa)) {
			enErreur = true;
			messagesErreur.add(ch+"Facturer sans passage"+mp);
		}
		
		if(!verifierChampsTexte(Contratprinci)) {
			enErreur = true;
			messagesErreur.add(ch+"Contrat principal"+mp);
		}
		
		if(!verifierChampsTexte(ModeRegl)) {
			enErreur = true;
			messagesErreur.add(ch+"Mode de r�glement"+mp);
		}
		
		if(!verifierChampsTexte(FacturerSeul)) {
			enErreur = true;
			messagesErreur.add(ch+"Facturer seul"+mp);
		}
		
		if(!verifierChampsTexte(FacturerAvec)) {
			enErreur = true;
			messagesErreur.add(ch+"Facturer avec"+mp);
		}
		
		if(!verifierChampsTexte(FacturerSiege)) {
			enErreur = true;
			messagesErreur.add(ch+"Facturer"+mp);
		}
		
		if(!verifierChampsTexte(Echeance)) {
			enErreur = true;
			messagesErreur.add(ch+"�ch�ance de paiement de la facture"+mp);
		}
		
		if(!verifierChampsTexte(ValeurX)) {
			enErreur = true;
			messagesErreur.add(ch+"Valeur du X"+mp);
		}
		
		if(!verifierChampsTexte(Reference)) {
			enErreur = true;
			messagesErreur.add(ch+"R�f�rence Facturation"+mp);
		}
		
		if(!verifierChampsTexte(DureePrea)) {
			enErreur = true;
			messagesErreur.add(ch+"dur�e du pr�avis"+mp);
		}
		
		if(!verifierChampsTexte(AdresseFact)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse de facturation diff�rente"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: si�ge"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff2)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: nom"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff3)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: nom 2"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff4)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: n�et voie"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff5)) {
			enErreur = true;
			messagesErreur.add(ch+"adresse diff�rente: cpt adresse"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff6)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: code postal"+mp);
		}
		
		if(!verifierChampsTexte(AdresseDiff7)) {
			enErreur = true;
			messagesErreur.add(ch+"Adresse diff�rente: ville"+mp);
		}
		
		if(!verifierChampsTexte(NMarche)) {
			enErreur = true;
			messagesErreur.add(ch+"Num�ro de march�"+mp);
		}
		
		if(!verifierChampsTexte(NEngagement)) {
			enErreur = true;
			messagesErreur.add(ch+"Num�ro d'engagement"+mp);
		}
		
	}

	@Override
	public String toString() {
		return "InfoContratCli [enErreur=" + enErreur + ", messagesErreur=" + messagesErreur + ", Civ="
				+ Arrays.toString(Civ) + ", Num=" + Arrays.toString(Num) + ", ch=" + ch + ", mp=" + mp + ", bo=" + bo
				+ ", CodeClient=" + CodeClient + ", CodeSite=" + CodeSite + ", NContrat=" + NContrat + ", DateDebut="
				+ DateDebut + ", DateFin=" + DateFin + ", TaciteRecon=" + TaciteRecon + ", DatePrea=" + DatePrea
				+ ", MotifContrat=" + MotifContrat + ", CategoContrat=" + CategoContrat + ", RefContrat=" + RefContrat
				+ ", NDevis=" + NDevis + ", CompteAnaly=" + CompteAnaly + ", Comment=" + Comment + ", Materiel="
				+ Materiel + ", Produits=" + Produits + ", FacturerPassa=" + FacturerPassa + ", Contratprinci="
				+ Contratprinci + ", ModeRegl=" + ModeRegl + ", FacturerSeul=" + FacturerSeul + ", FacturerAvec="
				+ FacturerAvec + ", FacturerSiege=" + FacturerSiege + ", Echeance=" + Echeance + ", ValeurX=" + ValeurX
				+ ", Reference=" + Reference + ", DureePrea=" + DureePrea + ", AdresseFact=" + AdresseFact
				+ ", AdresseDiff=" + AdresseDiff + ", AdresseDiff2=" + AdresseDiff2 + ", AdresseDiff3=" + AdresseDiff3
				+ ", AdresseDiff4=" + AdresseDiff4 + ", AdresseDiff5=" + AdresseDiff5 + ", AdresseDiff6=" + AdresseDiff6
				+ ", AdresseDiff7=" + AdresseDiff7 + ", NMarche=" + NMarche + ", NEngagement=" + NEngagement + "]";
	}
	
	
	
}
