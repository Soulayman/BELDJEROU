package com.example.oless

import android.Manifest
import android.content.ContentResolver
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.list_contact.*

class ContactActivity : AppCompatActivity(),/*new*/ View.OnClickListener {
    companion object {
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadContacts()
    }
    private fun loadContacts() {
        var contactsX= arrayListOf<Contact>()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_READ_CONTACTS)
            //callback onRequestPermissionsResult
        } else {
            contactsX = getContacts()
            /*new*/
            val adapter = ContactAdapter(contactsX,this)
            setContentView(R.layout.list_contact)
            contacts_recycler_view!!.layoutManager= LinearLayoutManager(this)
            contacts_recycler_view!!.adapter=adapter
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()
            } else {
                Toast.makeText(this,"Permission must be granted in order to display contacts information",Toast.LENGTH_LONG).show()

            }
        }
    }
    var contactsX= arrayListOf<Contact>()
    private fun getContacts(): ArrayList<Contact> {
        val resolver: ContentResolver = contentResolver;
        val cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null,
            null)
        if (cursor!!.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()
                if (phoneNumber > 0) {
                    val cursorPhone = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)
                    if(cursorPhone!!.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            val phoneNumValue = cursorPhone.getString(
                                cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            contactsX.add(Contact(name,phoneNumValue))
                        }
                    }
                    cursorPhone.close()
                }
            }
        } else {
            Toast.makeText(this,"pas de contact",Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return contactsX
    }
    /*new*/
    override fun onClick(view: View) {
        if(view.tag !=null){
            val index=view.tag as Int
            val contact=contactsX[index]
            Toast.makeText(this,"${contact.numero}",Toast.LENGTH_SHORT).show()
        }
    }
}