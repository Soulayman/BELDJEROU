package com.example.oless

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_reservation_form.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_hotel_description.setOnClickListener{
            println("youhou")
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            startActivity(intent)
        }
        activity_hotel_description.setOnClickListener{
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            intent.action=Intent.ACTION_VIEW
            intent.addCategory("Hotel")
            intent.putExtra("Underground Hôtel","l'Underground Hôtel est un batîment spécial , ici vous ne trouverez pas un hôtel classique lamda mais souterrain ")
            startActivity(intent)
        }
        activity_reservation_form.setOnClickListener{
            println("youhou")
            val intent = Intent(this, ReservationFormActivity::class.java)
            intent.action=Intent.ACTION_VIEW
            startActivity(intent)
        }

        listVille.setOnClickListener{
            val intent = Intent(this, VilleActivity::class.java)
            startActivity(intent)
        }

        listContact.setOnClickListener{
            val intent = Intent(this, ContactActivity::class.java)
            startActivity(intent)
        }



    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_home->{
                Toast.makeText(this,"Menu", Toast.LENGTH_SHORT).show() // Page Menu
                val intent = Intent(this, MainActivity::class.java)
                intent.action = Intent.ACTION_VIEW
                startActivity(intent) }

            R.id.action_reservation->{
                Toast.makeText(this,"Reservation", Toast.LENGTH_SHORT).show() // Page Menu
                val intent = Intent(this, ReservationFormActivity::class.java)
                intent.action = Intent.ACTION_VIEW
                startActivity(intent) }

            R.id.action_description->{
                Toast.makeText(this,"Description", Toast.LENGTH_SHORT).show() // Page Menu
                val intent = Intent(this, HotelDescriptionActivity::class.java)
                intent.action = Intent.ACTION_VIEW
                startActivity(intent) }

            R.id.action_ville->{
                Toast.makeText(this,"Description", Toast.LENGTH_SHORT).show() // Page Menu
                val intent = Intent(this, VilleActivity::class.java)
                intent.action = Intent.ACTION_VIEW
                startActivity(intent) }

        }
        return true
    }


}

