package com.example.oless

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_reservation_form.*
import java.time.LocalDate

class ReservationFormActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation_form)


        button_form.setOnClickListener{
            val reservation= Reservation(nom_view.text.toString(), LocalDate.parse(dateDebut_view.text),LocalDate.parse(dateFin_view.text))
            val intent = Intent(this, ReservationConfirmActivity::class.java)
            intent.putExtra("reservation",reservation)
            startActivity(intent)
        }
    }

}