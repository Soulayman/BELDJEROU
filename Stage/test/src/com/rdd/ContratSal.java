package com.rdd;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ContratSal {
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		File file = new File("D:\\BTS_SIO\\Stage\\RDD2021\\Excel\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("CONTRAT SALARIES");
		
		int rowNum = 1;
		
		List<InfoContratSal> retour = new ArrayList<>();
		
		for(int i=1;i<=sheet.getLastRowNum(); i++) {

			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoContratSal info = new InfoContratSal();
			info.setMatriSal(ligneCourante.getCell(0).getStringCellValue());
			info.setTypeContr(ligneCourante.getCell(1).getStringCellValue());
			info.setDateDebContr(ligneCourante.getCell(2).getStringCellValue());
			info.setDateFinContr(ligneCourante.getCell(3).getStringCellValue());
			info.setMotifDebEmplo(ligneCourante.getCell(4).getStringCellValue());
			info.setHoraireDeDebuEmplo(ligneCourante.getCell(5).getStringCellValue());
			info.setDureePeriodeEssai(ligneCourante.getCell(6).getStringCellValue());
			info.setTypeDePeriodeEssai(ligneCourante.getCell(7).getStringCellValue());
			info.setDateFinPeriode(ligneCourante.getCell(8).getStringCellValue());
			info.setClassificaMetier(ligneCourante.getCell(9).getStringCellValue());
			info.setClassificaMetierSAP(ligneCourante.getCell(10).getStringCellValue());
			info.setNaturEmplo(ligneCourante.getCell(11).getStringCellValue());
			info.setTauxHorai(ligneCourante.getCell(12).getStringCellValue());
			info.setSMIC(ligneCourante.getCell(13).getStringCellValue());
			info.setNePasTransm(ligneCourante.getCell(14).getStringCellValue());
			info.setNbHeureHebdo(ligneCourante.getCell(15).getStringCellValue());
			info.setNbHeureMensu(ligneCourante.getCell(16).getStringCellValue());
			info.setNbJourTrav(ligneCourante.getCell(17).getStringCellValue());
			info.setLundi(ligneCourante.getCell(18).getStringCellValue());
			info.setMardi(ligneCourante.getCell(19).getStringCellValue());
			info.setMercredi(ligneCourante.getCell(20).getStringCellValue());
			info.setJeudi(ligneCourante.getCell(21).getStringCellValue());
			info.setVendredi(ligneCourante.getCell(22).getStringCellValue());
			info.setSamedi(ligneCourante.getCell(23).getStringCellValue());
			info.setDimanche(ligneCourante.getCell(24).getStringCellValue());
			info.setContratSign(ligneCourante.getCell(25).getStringCellValue());
			
			retour.add(info);
			
			rowNum++;
		}
		
		for (InfoContratSal info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getMatriSal() + " ; " +info.getTypeContr() + " ; " + info.getDateDebContr() + " ; " + info.getDateFinContr()+ " ; " +info.getMotifDebEmplo()+ " ; " +info.getHoraireDeDebuEmplo()+ " ; " +info.getDureePeriodeEssai()
			+ " ; " +info.getTypeDePeriodeEssai()+ " ; " +info.getDateFinPeriode()+ " ; " + info.getClassificaMetier()+ " ; " +info.getClassificaMetierSAP()
			+ " ; " +info.getNaturEmplo()+ " ; " +info.getTauxHorai()+ " ; " +info.getSMIC()+ " ; " +info.getNePasTransm()+ " ; " +info.getNbHeureHebdo()+ " ; " +info.getNbHeureMensu()+ " ; " +info.getNbJourTrav()+ " ; " +info.getLundi()+ " ; " +info.getMardi()
			+ " ; " +info.getMercredi()+ " ; " +info.getJeudi()+ " ; " +info.getVendredi()+ " ; " +info.getSamedi()
			+ " ; " +info.getDimanche()+ " ; " +info.getContratSign()+ (info.isEnErreur() ? info.getMessagesErreur(): "OK"));
		}
		
		
	}
	
}
