//Mission 3 SI4
//BELDJEROU Soulayman
//23/11/2019
//Question1

/*Algo 
	Variables: a entier , tva réel , prixttc réel , quantite réel , resultat réel, resultatt réel , nom chaine de caractère
	Debut:
	
		a<-1
		tva<-0.2
	Tant que (a!=0 et a<2) faire
		afficher "saisir nom du bien"
		saisir nom
		afficher "saisir prixht"
		saisir prixht
		afficher "saisir quantite"
		saisir quantite
		
		prixttc <- prixht +(prixht*tva)/100
		resultat <- prixttc*quantite
		Si quantite < 100 alors
			prixttc <- prixht + (prixht*prixttc)/100
			resultat <- prixttc*quantite 
		Sinon
			prixttc <- prixht+(prixht*prixttc)/100
			resultatt <- prixttc*quantite 
			resultat <- resultatt-((resultatt*10)/100)
			
			afficher "nom + resultat"
		fin si
		
		afficher "saisir a"
		saisir a
	fin TQ
	
		Si (a!=0) alors
			afficher " vous avez taper le mauvais nombre"
		Sinon 
			afficher "a une autres fois"
		Fin si
	fin*/
	
		

import java.util.Scanner;
public class Question1{
	public static void main(String[]args){
		double prixttc;
		double prixht;
		double quantite;
		double tva=0.2;
		double resultat;
		double resultatt;
		String nom;
		int a=1; //initialisation d'une variable qui determinera l'entier pour la boucle
		
		Scanner a4 = new Scanner(System.in);
		
		while(a==1){ //ouverture de la boucle tant que 
			System.out.println("saisir nom du biens:");
			nom=a4.nextLine();
			System.out.println("saisir le prix hors taxe du bien:");
			prixht=a4.nextDouble();
			System.out.println("saisir la quantite de bien:");
			quantite=a4.nextDouble();
			
			prixttc=prixht+(prixht*tva)/100;
			resultat=prixttc*quantite;
			
			System.out.println(" resultat "+nom +" est de: "+ resultat);
			
			if(quantite<100) {
				prixttc=prixht+(prixht*prixttc)/100;
				resultat=prixttc*quantite;
			}else{
				prixttc=prixht+(prixht*prixttc)/100;
				resultatt=prixttc*quantite;
				resultat=resultatt-((resultatt*10)/100);
				
				System.out.println("le prix final de "+ nom + "est de" + resultat);
			}
			System.out.println("si vous souhaitez recommencer tapez 1 pour quitter taper 0");
			a=a4.nextInt();
		}//fermeture de la boucle Tant que
		
		if(a!=0){
			System.out.println("vous avez taper le mauvais nombre");
		}else{
			System.out.println("a une autre fois !");
		}
	}
}