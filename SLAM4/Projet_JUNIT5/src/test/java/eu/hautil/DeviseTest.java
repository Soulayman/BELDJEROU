package eu.hautil;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeviseTest {


    Devise m12CHF;
    Devise m14CHF;
    Devise m14USD;
    Devise m22USD;
    Devise m26CHF;
    int nb = 1;


    @Test
    void add() throws MonnaieDifferenteException{
        Devise result = m12CHF.add(m14CHF);
        Assertions.assertEquals(m26CHF, result, "L'additoon de deux Devise ne fonctionne pas");
    }

    @Test
    void addWithException() throws MonnaieDifferenteException{
        Assertions.assertThrows(MonnaieDifferenteException.class,()->m12CHF.add(m14CHF));
    }

    @BeforeAll
    void Vietnam(){
        System.out.println(nb + "e passage avant");
    }

    @AfterAll
    void Vietnam2(){
        System.out.println(nb++ +"e passage après");
    }


    //méthode d'initialisation
    @BeforeAll
    void init(){
       m12CHF= new Devise(12, "CHF");
       m14CHF= new Devise(14, "CHF");
       m14USD = new Devise(14, "USD");
       m22USD = new Devise(22, "USD");
       m26CHF = new Devise(26, "CHF");

    }

    @Test
    void testEquals() {



    // tester que m12CHF est bien égale à m12CHF
        Assertions.assertEquals(m12CHF,m12CHF,"Deux objets qui devrait être égaux mais non !");
        Assertions.assertTrue(m14CHF.equals(m14CHF),"égalité non prouvé");
    // tester que m12CHF n'est pas égale à m14CHF
        Assertions.assertNotEquals(m12CHF, m14CHF, "Égalité entre deux objets inégaux");
        Assertions.assertFalse(m12CHF.equals(m14CHF));
    // tester que m14CHF n'est pas égale à m14USD
        Assertions.assertNotEquals(m14USD,m14CHF,"Égalité entre deux objets inégaux");

    }


}