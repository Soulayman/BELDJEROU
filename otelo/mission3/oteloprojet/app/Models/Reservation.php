<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Reservation;

class Reservation extends Model
{
    protected $table = "reservation"; 
    protected $fillable = [
       'dateD' ,'dateF','idPeriode','updated_at','created_at'
   ];
}

