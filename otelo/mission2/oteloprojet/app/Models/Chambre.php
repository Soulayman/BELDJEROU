<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Chambre;

Class Chambre extends Model
{
     protected $table = "chambre"; 
     protected $fillable = [
        'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase' 
    ];
}
