package com.rdd;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Site {
	public static void main(String[] args) throws Exception {
		File file = new File("D:\\BTS_SIO\\Stage\\RDD2021\\Excel\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet= wb.getSheet("SITES");
		
		int rowNum = 1;
		
		List<InfoSite> retour = new ArrayList<>();
		
		for (int i = 1; i<=sheet.getLastRowNum(); i++)
		{
			XSSFRow ligneCourante = sheet.getRow(rowNum);
			InfoSite info = new InfoSite();
			
			info.setCodeCli(ligneCourante.getCell(0).getStringCellValue());	
			info.setCodeSite(ligneCourante.getCell(1).getStringCellValue());
			info.setLibelSite(ligneCourante.getCell(2).getStringCellValue());
			info.setMatriResp(ligneCourante.getCell(3).getStringCellValue());
			info.setMatriInsp(ligneCourante.getCell(4).getStringCellValue());
			info.setMatriComm(ligneCourante.getCell(5).getStringCellValue());
			info.setMatriChef(ligneCourante.getCell(6).getStringCellValue());
			info.setCategoSite(ligneCourante.getCell(7).getStringCellValue());
			info.setSectGeo(ligneCourante.getCell(8).getStringCellValue());
			info.setFacturable(ligneCourante.getCell(9).getStringCellValue());
			info.setInterdiPlan(ligneCourante.getCell(10).getStringCellValue());
			info.setComment(ligneCourante.getCell(11).getStringCellValue());
			info.setPole(ligneCourante.getCell(12).getStringCellValue());
			info.setActivite(ligneCourante.getCell(13).getStringCellValue());
			info.setSection(ligneCourante.getCell(14).getStringCellValue());
			info.setRegroup1(ligneCourante.getCell(15).getStringCellValue());
			info.setRegroup2(ligneCourante.getCell(16).getStringCellValue());
			info.setRegroup3(ligneCourante.getCell(17).getStringCellValue());
			info.setDirectReg(ligneCourante.getCell(18).getStringCellValue(	));
			info.setDirectOpe(ligneCourante.getCell(19).getStringCellValue());
			info.setTel(ligneCourante.getCell(20).getStringCellValue());
			info.setAccesClef(ligneCourante.getCell(21).getStringCellValue());
			info.setItiniTransp(ligneCourante.getCell(22).getStringCellValue());
			info.setPays(ligneCourante.getCell(23).getStringCellValue());
			info.setAdresse(ligneCourante.getCell(24).getStringCellValue());
			info.setComplemAdre(ligneCourante.getCell(25).getStringCellValue());
			info.setCodePost(ligneCourante.getCell(26).getStringCellValue());
			info.setVille(ligneCourante.getCell(27).getStringCellValue());
			info.setTelFix(ligneCourante.getCell(28).getStringCellValue());
			info.setTelMob(ligneCourante.getCell(29).getStringCellValue());
			info.setTelFax(ligneCourante.getCell(30).getStringCellValue());
			info.setEmail(ligneCourante.getCell(31).getStringCellValue());
			info.setAcceptMail(ligneCourante.getCell(32).getStringCellValue());
			info.setCodeAlarm(ligneCourante.getCell(33).getStringCellValue());
			info.setSurfaceTota(ligneCourante.getCell(34).getStringCellValue());
			info.setSurfaceVitre(ligneCourante.getCell(35).getStringCellValue());
			info.setPointEau(ligneCourante.getCell(36).getStringCellValue());
			info.setElectr(ligneCourante.getCell(37).getStringCellValue());
			info.setLocalMen(ligneCourante.getCell(38).getStringCellValue());
			info.setCompteCompta(ligneCourante.getCell(39).getStringCellValue());
			info.setCompteAnal(ligneCourante.getCell(40).getStringCellValue());
			info.setModeReg(ligneCourante.getCell(41).getStringCellValue());
			info.setEcheFact(ligneCourante.getCell(42).getStringCellValue());
			info.setValeurX(ligneCourante.getCell(43).getStringCellValue());
			info.setGroupeFact(ligneCourante.getCell(44).getStringCellValue());
			info.setActif(ligneCourante.getCell(45).getStringCellValue());
			info.setDateArret(ligneCourante.getCell(46).getStringCellValue());
			info.setPublicfrag(ligneCourante.getCell(47).getStringCellValue());
			info.setGIR(ligneCourante.getCell(48).getStringCellValue());
			info.setNRemplace(ligneCourante.getCell(49).getStringCellValue());
			info.setMesuProtec(ligneCourante.getCell(50).getStringCellValue());
			info.setNSecu(ligneCourante.getCell(51).getStringCellValue());
			info.setMutuelle(ligneCourante.getCell(52).getStringCellValue());
			info.setCaisRetrai(ligneCourante.getCell(53).getStringCellValue());
			info.setTypeLog(ligneCourante.getCell(54).getStringCellValue());
			info.setAccess(ligneCourante.getCell(55).getStringCellValue());
			info.setEtatGener(ligneCourante.getCell(56).getStringCellValue());
			info.setHabVie(ligneCourante.getCell(57).getStringCellValue());
			info.setObjeAtt(ligneCourante.getCell(58).getStringCellValue());
			info.setNature(ligneCourante.getCell(59).getStringCellValue());
			info.setConsigneCli(ligneCourante.getCell(60).getStringCellValue());
			info.setConsigneInt(ligneCourante.getCell(61).getStringCellValue());
			info.setMateriel(ligneCourante.getCell(62).getStringCellValue());
			
			retour.add(info);
			rowNum++;
		}
		
		for (InfoSite info : retour)
		{
			info.verifierCoherence();
			System.out.println(info.getCodeCli()+ " ; " +info.getCodeSite()+ " ; " +info.getLibelSite()+ " ; " +info.getMatriResp()
			+ " ; " +info.getMatriInsp()+ " ; " +info.getMatriComm()+ " ; " +info.getMatriChef()+ " ; " +info.getCategoSite()
			+ " ; " +info.getSectGeo()+ " ; " +info.getFacturable()+ " ; " +info.getInterdiPlan()+ " ; " +info.getComment()
			+ " ; " +info.getPole()+ " ; " +info.getActivite()+ " ; " +info.getSection()+ " ; " +info.getRegroup1()
			+ " ; " +info.getRegroup2()+ " ; " +info.getRegroup3()+ " ; " +info.getDirectReg()+ " ; " +info.getDirectOpe()
			+ " ; " +info.getTel()+ " ; " +info.getAccesClef()+ " ; " +info.getItiniTransp()+ " ; " +info.getPays()
			+ " ; " +info.getAdresse   ()+ " ; " +info.getComplemAdre()+ " ; " +info.getCodePost()+ " ; " +info.getVille()
			+ " ; " +info.getTelFix()+ " ; " +info.getTelMob()+ " ; " +info.getTelFax()+ " ; " +info.getEmail()
			+ " ; " +info.getAcceptMail()+ " ; " +info.getCodeAlarm()+ " ; " +info.getSurfaceTota()
			+ " ; " +info.getSurfaceVitre()+ " ; " +info.getPointEau()+ " ; " +info.getElectr()+ " ; " +info.getLocalMen()
			+ " ; " +info.getCompteCompta()+ " ; " +info.getCompteAnal()+ " ; " +info.getModeReg()+ " ; " +info.getEcheFact()
			+ " ; " +info.getValeurX()+ " ; " +info.getGroupeFact()+ " ; " +info.getActif()+ " ; " +info.getDateArret()
			+ " ; " +info.getPublicfrag()+ " ; " +info.getGIR()+ " ; " +info.getNRemplace()+ " ; " +info.getMesuProtec()
			+ " ; " +info.getNSecu()+ " ; " +info.getMutuelle()+ " ; " +info.getCaisRetrai()+ " ; " +info.getTypeLog()
			+ " ; " +info.getAccess()+ " ; " +info.getEtatGener()+ " ; " +info.getHabVie()+ " ; " +info.getObjeAtt()
			+ " ; " +info.getNature()+ " ; " +info.getConsigneCli()+ " ; " +info.getConsigneInt()+ " ; " +info.getMateriel()+  (info.isEnErreur() ? info.getMessagesErreur(): "OK"));		
		}
	}
}
