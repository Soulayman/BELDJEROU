
# Aspirateur Intelligent 02/10/2020

## Roomba souhaite faire de ses aspirateurs des appareils intélligents , et faire en sorte que l'appareil agisse de lui même en sachant quoi faire dans la maison , et définir ses tâches sans déranger l'utilisateur

## extrait de l'article : "Une nouvelle plate-forme d’intelligence artificielle doit permettre à ces machines d’apprendre où et quand nettoyer pour éviter d’importuner leurs utilisateurs."

## Source : https://www.01net.com/actualites/roomba-veut-rendre-ses-robots-aspirateurs-vraiment-intelligents-1968084.html


# Amazon Echo 09/10/2020

## Amazon lance une nouvelle game de produits sous le nom de "Echo" , le projet d'Amazon consiste a fabriquer les appareils connecté a l'aide de produits recyclé , ce qui est un avantage pour l'écologie , notamment le prix des appareils de chez Amazon qui ne seront pas cher

## extrait de l'article : "La conférence Amazon Devices se tenait hier soir, et, comme chaque année, au mois de septembre. L'occasion pour le géant de dévoiler de nouveaux produits. Les enceintes Echo (Echo, Echo Dot et Echo Dot avec horloge) ont été repensées avec un design sphérique et une meilleure qualité sonore. Le modèle Echo Plus disparaît. Les interactions avec Alexa ont été améliorées."

## Source : https://www.01net.com/actualites/amazon-lance-une-nouvelle-gamme-de-produits-echo-1981495.html

# Smart home 06/11/2020

## Smart home gagne du terrain ces temps ci , et affecte de plus en plus de logement , avec des assistants moins cher tel que Alexa , le Smart home continue d'évoluer a l'instant même

## extrait de l'article : "Le développement de la « smart home » (ou « maison intelligente ») est un phénomène assez récent, né de l’évolution de la domotique depuis le siècle dernier. Dans les années 70, la domotique permet de centraliser la gestion de nos équipements domestiques à partir d'une commande centrale. Mais le système est filaire, coûteux et doit être installé par un professionnel. Il faut attendre la fin des années 90 pour que cette technologie commence à se diffuser, et pour que les équipements puissent être réellement contrôlés à distance du domicile."

## source : https://www.01net.com/actualites/smart-home-quand-la-maison-se-veut-connectee-et-intelligente-1994572.html

# L’intelligence artificielle, encore plus indispensable pour les entreprises en temps de Covid 20/11/2020 

## avec notre période actuel , certaines entreprises souhaitent se doter d'Intelligence Artificiel , le sujet a peut être aucun rapport avec les IA de domotiques , mais ce sujet reste quand même bon a savoir , et assez intriguant

## extrait de l'article : "Carte blanche. En août dernier, Bercy a lancé une mission gouvernementale pour la transformation numérique des grands groupes, pilotée par Nicolas Guérin (Natixis) et Juliette de Maupeou (Capgemini). Cinq chantiers ont été identifiés : la transformation des compétences et des formations, la souveraineté des données, la souveraineté du e-paiement, les relations start-up et grands groupes et, enfin, une stratégie commune sur l’intelligence artificielle (IA). Des groupes de travail, avec des « do tank » thématiques, sont en train de se constituer avec la contribution de nombreux groupes industriels représentés par leur CDO (chief digital officer, ou directeur de la transformation digitale) ou des fonctions équivalentes."

## source : https://www.lemonde.fr/sciences/article/2020/11/11/l-intelligence-artificielle-encore-plus-indispensable-pour-les-entreprises-en-temps-de-covid_6059343_1650684.html

# Les préoccupations sur l'éthique des algorithmes doivent être prises très au sérieux 17/12/2020

## Google licencie une chercheuse de renom en intelligence artificielle important aux yeux du mondes , certains son contre ce licenciement tels que Lê Nguyên Hoang , spécialiste de l'éthique des algorithmes

## extrait de l'article : Le licenciement par Google de Timnit Gebru, chercheuse de renom en intelligence artificielle, jette une ombre sur les priorités de l’entreprise et suscite de légitimes protestations, souligne, dans une tribune au « Monde », Lê Nguyên Hoang, spécialiste de l’éthique des algorithmes."

## source : https://www.lemonde.fr/idees/article/2020/12/17/les-preoccupations-sur-l-ethique-des-algorithmes-doivent-etre-prises-tres-au-serieux_6063675_3232.html

# Intelligence artificielle cherche cerveaux désespérément 19/03/2021

## d'après l'article , on peux comprendre qu'il y a une pénurie tech , les entreprises national française sont retardé sur la modernisation 

## extrait de l'article : La pénurie de profils « tech » hautement qualifiés freine l’adoption de l’intelligence artificielle, retardant la modernisation des entreprises françaises, selon une étude du cabinet de recrutement PageGroup.

## source : https://www.lemonde.fr/emploi/article/2021/03/15/intelligence-artificielle-cherche-cerveaux-desesperement_6073187_1698637.html

#  La ville connectée, entre intelligence artificielle et libertés publiques 22/03/2021

## La ville connectée reste une option avantageuse pour la société d'aujourd'hui ,car celle-ci permettra le confort de toutes les personnes

## extrait de l'article : Jusqu’à l’avènement de la 5G, la domotique et l’automatisation des équipements techniques du bâtiment se cantonnaient le plus souvent aux installations de chauffage-ventilation et aux commandes de volets roulant ; mais les possibilités offertes par le déploiement de réseaux ultrapuissants, l’essor de l’Internet des objets et de l’IA ouvrent aujourd’hui d’innombrables perspectives. Les initiatives qui fleurissent grâce aux réseaux des Smart City autrichiennes présentent des solutions remarquables, non seulement par leurs technicités mais également dans leurs approches sociales et politiques.

## source : https://www.lemoniteur.fr/article/la-ville-connectee-entre-intelligence-artificielle-et-libertes-publiques.2133344