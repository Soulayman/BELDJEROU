/*Algo
	Variables:tva réel , prixttc réel , quantite réel , resultat réel, resultatt réel , nom chaine de caractère , b entier
	Debut:
		tva<-0.2
		afficher "saisir b"
		saisir b
		
		pour b de b à 0 pas -1 faire
		
		afficher "saisir nom du bien"
			saisir nom
			afficher "saisir prixht"
			saisir prixht
			afficher "saisir quantite"
			saisir quantite
					
			prixttc <- prixht +(prixht*tva)/100
			resultat <- prixttc*quantite
			Si quantite < 100 alors
				prixttc <- prixht + (prixht*prixttc)/100
				resultat <- prixttc*quantite 
			Sinon
				prixttc <- prixht+(prixht*prixttc)/100
				resultatt <- prixttc*quantite 
				resultat <- resultatt-((resultatt*10)/100)
						
				afficher "nom + resultat"
			fin si
		fin pour
	fin*/
import java.util.Scanner;
public class Question2{
	public static void main (String[] args){
		
		double prixttc;
		double prixht;
		double quantite;
		double tva=20;
		double resultat;
		double resultatt;
		String nom;
		int b; //initialisation de la nouvelle variable donc b
		
		Scanner a4 = new Scanner(System.in);
		
		System.out.println("combien de fois souhaitez vous calculer le prixttc de votre produit ?");//on affiche a l'utilisateur combien de fois veut-il faire tourner la boucle
		b=a4.nextInt();//ouverture du clavier a l'utilisateur 
		
		for(b=b; b>0; b--){
			System.out.println("saisir nom du biens:");
			nom=a4.next();
			a4.nextLine();
			System.out.println("saisir le prix hors taxe du bien:");
			prixht=a4.nextDouble();
			System.out.println("saisir la quantite de bien:");
			quantite=a4.nextDouble();
			
			prixttc=prixht+(prixht*tva)/100;
			resultat=prixttc*quantite;
			
			System.out.println(" resultat "+nom +" est de: "+ resultat);
			
			if(quantite<100){
				prixttc=prixht+(prixht*prixttc)/100;
				resultat=prixttc*quantite;
			}else{
				prixttc=prixht+(prixht*prixttc)/100;
				resultatt=prixttc*quantite;
				resultat=resultatt-((resultatt*10)/100);
				System.out.println(" le prix final de "+ nom + " est de " + resultat);
			}
		}
		
		System.out.println("merci et au revoir !!");
	}
}
			
			