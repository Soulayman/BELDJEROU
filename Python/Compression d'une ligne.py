def comp2(donnees):
    compression=[0 for i in range(12)]
    premier=donnees[0]
    j=0
    courant=premier
    for i in range(12):
        if donnees[i]==courant:
            compression[j]=compression[j]+1
        else:
            j=j+1
            courant=donnees[i]
            compression[j]=1
    T=compression
    compression=[]
    j=0
    while T[j]!=0:
        compression.append(T[j])
        j=j+1
    return compression 
    
