<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reservation;

Class Reservation extends Model
{
     protected $table = "test"; 
     protected $fillable = [
        'dateD','dateF','idPeriode'
    ];
}